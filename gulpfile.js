var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat');

    gulp.task('default', ['sass', 'concat']);

    gulp.task('sass', function() {
        gulp.src('resources/assets/sass/app.scss')
            .pipe(autoprefixer())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(gulp.dest('public/css/'));
    });

    gulp.task('sass2', function() {
        gulp.src('resources/assets/sass/landing.scss')
            .pipe(autoprefixer())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(gulp.dest('public/css/'));
    });

    gulp.task('watch', function() {
        gulp.watch('resources/assets/sass/*.scss', ['sass','sass2']);
        gulp.watch('resources/assets/js/*.js', ['concat','concat2']);
    });

    gulp.task('concat2', function() {
        gulp.src([
          "resources/assets/js/jquery/jquery.js",
          "resources/assets/js/bootstrap/bootstrap.min.js",

          "resources/assets/js/iCheck/icheck.min.js",
          "resources/assets/js/inputmask/jquery.inputmask.bundle.js",

          "resources/assets/js/materialize/materialize.min.js",
          "resources/assets/js/vendor/waypoints.min.js",
          "resources/assets/js/vendor/counter.up.min.js",
          "resources/assets/js/landing_app.js", // our scripts
    ])
            .pipe(concat('landing.js'))

            .pipe(gulp.dest('public/js'));
    });

    gulp.task('concat', function() {
        gulp.src([
          "resources/assets/js/jquery/jquery.js",
          "resources/assets/js/jquery/jquery-ui.js",
          "resources/assets/js/bootstrap/bootstrap.min.js",
          "resources/assets/js/moment.js",
          // angular staff
          "resources/assets/js/angular/angular.min.js",
          "resources/assets/js/angular/angular-aria.min.js",
          "resources/assets/js/angular/angular-animate.min.js",
          "resources/assets/js/angular/angular-material.min.js",
          "resources/assets/js/angular/angular-sanitize.js",
          "resources/assets/js/angular/angular-locale-ru.js",
          "resources/assets/js/angular/angular-material-calendar.js",
          "resources/assets/js/mfb-directive.js", // floating action button
          "resources/assets/js/angular/application.js",
          "resources/assets/js/iCheck/icheck.min.js",
          "resources/assets/js/inputmask/jquery.inputmask.bundle.js",
          // Materialize staff

          "resources/assets/js/materialize/materialize.min.js",
          "resources/assets/js/materialize/forms.js",
          "resources/assets/js/materialize/tabs.js",
          "resources/assets/js/vendor/scrollBar.js",
          "resources/assets/js/vendor/waypoints.min.js",
          "resources/assets/js/vendor/counter.up.min.js",

          "resources/assets/js/app.js", // our scripts

          "resources/assets/js/confetti_canvas.js", // confetti canvas animation

          "resources/assets/js/jquery/jquery.sparkline.min.js", // Sparkline
          "resources/assets/js/jquery/jquery-jvectormap-1.2.2.min.js", // jvectormap WorldMap
          "resources/assets/js/jquery/jquery-jvectormap-world-mill-en.js",
          "resources/assets/js/jquery/jquery.knob.js", // jQuery Knob Chart
          "resources/assets/js/jquery/jquery.dataTables.js",
          "resources/assets/js/bootstrap/dataTables.bootstrap.js",
          "resources/assets/js/daterangepicker.js", // daterangepicker
          "resources/assets/js/bootstrap/bootstrap-datepicker.js", // datepicker
          "resources/assets/js/bootstrap/locales/bootstrap-datepicker.ru.js", // datepicker Russian
          "resources/assets/js/bootstrap/bootstrap3-wysihtml5.all.min.js", // Bootstrap WYSIHTML5
          "resources/assets/js/jquery/jquery.slimscroll.min.js", // Slimscroll
          "resources/assets/js/fastclick.min.js", // FastClick
          "resources/assets/js/chartjs/Chart.js",

          "resources/assets/js/custom-file-input.js",
          "resources/assets/js/select2.full.min.js",

          "resources/assets/js/app.min.js",
          "resources/assets/js/dashboard.js",
    ])
            .pipe(concat('all.js'))

            .pipe(gulp.dest('public/js'));
    });
