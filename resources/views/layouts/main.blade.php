<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <title>Feedback</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49907441-11', 'auto');
    ga('send', 'pageview');

</script>
    <h1 class="text-center">Landing page</h1>

    <div class="text-center">
        <ul class="list-unstyled">
            <li>
                <a href="/auth/registration">
                    Регистрация
                </a>
            </li>
            <li>
                <a href="/auth/login">
                    Вход
                </a>
            </li>
        </ul>
    </div>

</body>
</html>
