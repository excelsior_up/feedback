<!DOCTYPE html>
<html lang="en" >
<head >
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1" >

    <link rel="shortcut icon" href="/img/nozh/logo.png" >

    <meta name="description"
          content="Увеличьте доверие у Ваших клиентов и будьте всегда информированы о процессах работы в Вашем салоне." >
    <meta name="keywords" content="CRM,feedback,excelsior,салон красоты" >
    <meta name="author" content="Excelsior web agency" >

    <title >NOZH</title >

    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic|Work+Sans:400,300,500,600&subset=latin,cyrillic'
          rel='stylesheet' type='text/css' >
    <link href="/css/landing/swiper.min.css" rel="stylesheet" >
    <link href="/css/landing/toolkit-startup.css" rel="stylesheet" >
    <link href="/css/landing/application-startup.css" rel="stylesheet" >
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" >

    <style >
        .swiper-slide {
            background: transparent;
        }

        .swiper-pagination {
            width: 100%;
        }

        @media (max-width: 768px) and (-webkit-min-device-pixel-ratio: 2) {
            body {
                width: 1px;
                min-width: 100%;
                *width: 100%;
            }

            #stage {
                height: 1px;
                overflow: auto;
                min-height: 100vh;
                -webkit-overflow-scrolling: touch;
            }
        }
    </style >
</head >


<body >
@if(app()->environment() == 'production')

    <script >
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date()
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0]
        a.async = 1
        a.src = g
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga')

      ga('create', 'UA-49907441-11', 'auto')
      ga('send', 'pageview')

    </script >

@endif

<div class="aqt aqu hidden" id="sidebar" >
    <ul class="nav anr nav-stacked" >
        <li class="tq" >Страницы</li >

        <li class="nav-divider" ></li >
        <li class="tq" >Авторизация</li >
        <li >
            <a href="/auth/login" >Вход</a >
        </li >
        <li >
            <a href="/auth/register" >Регистрация</a >
        </li >
    </ul >
</div >

<div class="aqs" id="stage" >

    <div class="k al apb aqx" id="intro" >
        <div class="blackBg" ></div >
        <nav class="dt apr px tu app-navbar and" >
            <div class="e" >
                <div class="pv" >
                    <button type="button" class="pz collapsed p-x-0" data-target="#stage" data-toggle="stage"
                            data-distance="-250" >
                        <span class="ee" >Toggle navigation</span >
                        <span class="qa" ></span >
                        <span class="qa" ></span >
                        <span class="qa" ></span >
                    </button >
                    <a class="l only-sm" href="" >
                        <strong style="background: #fff; padding: 12px; border-radius: 4px; color: #28669F;" >
                            NOZH
                        </strong >
                    </a >
                </div >
                <div class="pw collapse eu" >
                    <ul class="nav navbar-nav pl" >
                        <li ><a href="#" class="l" >
                                <img src="/img/nozh/logo.png" alt="CRM Feedback" style="width:150px;" >
                            </a ></li >
                    </ul >
                    <ul class="nav navbar-nav pk" >

                        <li >
                            <a href="/auth/login" >Вход</a >
                        </li >
                        <li >
                            <a href="/auth/register" class="registration-link" >Регистрация</a >
                        </li >
                    </ul >
                </div >
            </div >
        </nav >


        <img class="y" src="/img/landing/startup-0.svg" >

        <div class="aph anm" >
            <div class="e" >
                <div class="c" >
                    <div class="ha gl" >
                        <h1 class="apd aqy" >NOZH – клуб людей, ухаживающих за собой</h1 >

                        <p class="ak amf am" >Станьте нашим партнером СЕГОДНЯ и получайте новых клиентов Каждый
                            день.</p >
                        <a href="#prices" >
                            <button class=" specBtn dl ot ao" >Стать партнером</button >
                        </a >
                    </div >
                </div >
            </div >
        </div >
    </div >

    <div class="k block-secondary z" >
        <div class="e" >
            <div class="c ap" >

                <div class="gg vb" >
                    <img class="aqz" src="/img/nozh/app_1.png" style="width: 100%;" >
                </div >

                <div class="gk kk" >
                    <h6 class="am eu" >Возможности сервиса</h6 >

                    <h3 class="alp" >Увеличение числа клиентов</h3 >

                    <p class="ak amf" >за счет создания общей базы и поиску по интересующим фильтрам</p >

                    <div class="c vc" >

                        <div class="gk amf" >
                            <h5 class="alu" >Возможность превратить клиентов в постоянных </h5 >
                            <p >Аналитика и маркетинговый анализ по потребляемым услугам</p >
                        </div >

                        <div class="gk" >
                            <h5 class="alu" >Максимальная загрузка графика </h5 >

                            <p >Уведомления потенциальным клиентам о существующих «окнах» </p >
                        </div >
                    </div >

                    <div class="c vc" >
                        <div class="gk" >
                            <h5 class="alu" >Объективная оценка работы мастеров</h5 >
                            <p >Клиенты, получившие услугу у мастера, будут ставить оценки мастеру, оставлять отзывы, на
                                основании которых будет формироваться рейтинг</p >
                        </div >
                        <div class="gk amf" >
                            <h5 class="alu" >Ведение клиентской базы</h5 >
                            <p >Профиль целевой аудитории, портрет потенциального клиента</p >
                        </div >
                    </div >

                </div >

            </div >

        </div >
    </div >


    <div class="section_slider" >
        <div class="container" id="swipeCont" style="overflow: hidden;">
            <div class="sol" >
                <div class="vertical-center" >
                    <h6 class="am eu" >Инновация</h6 >

                    <h3 class="alp" >Простой интерфейс и богатый функционал</h3 >

                    <p class="ak amf am" >Теперь не нужно вести запись на бумаге, носить с собой блокнот и созваниваться
                        с клиентом. Он выбирает подходящее время для своего визита, а Вы получаете уведомление и
                        автоматическую запись. Все клиенты и записи в одном Мобильном приложении. </p >
                    <button class="specBtn dl oq ao tl" >
                        Попробуйте сейчас
                    </button >
                </div >
                <div class="empty-div vertical-center" ></div >
            </div >
            <div class="on" id="swiperOn">
                <!-- Swiper -->
                <div style="width: 300px; margin: 0 auto;" id="swiperCover" >
                    <div class="swiper-container swiper-app" >
                        <div class="swiper-wrapper" >
                            <div class="swiper-slide" ><img src="/img/nozh/slider0.png" class="img-responsive"
                                                            alt="admin" >
                            </div >
                            <div class="swiper-slide" ><img src="/img/nozh/slider1.png" class="img-responsive"
                                                            alt="admin" >
                            </div >
                            <div class="swiper-slide" ><img src="/img/nozh/slider3.png" class="img-responsive"
                                                            alt="admin" >
                            </div >
                            <div class="swiper-slide" ><img src="/img/nozh/slider19.png" class="img-responsive"
                                                            alt="admin" >
                            </div >
                        </div >
                        <!-- Add Pagination -->
                        <div class="swiper-pagination swiper-pagination-app" ></div >

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev swiper-button-prev-app" ></div >
                        <div class="swiper-button-next swiper-button-next-app" ></div >
                    </div >
                </div >
            </div >
        </div >
    </div >

    <div class="k block-secondary ac ams"
         style="    background: url(/img/nozh/city1.jpg); background-size: 100%; background-color: rgba(0,0,0,0.5); background-blend-mode: multiply; background-repeat: no-repeat; background-position: center; /* background-color: white; */ background-attachment: fixed;" >
        <div class="e" >
            <div class="c ap" >
                <div class="gg jr" >
                    <h6 class="am eu" >История</h6 >

                    <h3 style="color:white" } class="alp amf" >“В 2014 году в Сингапуре была основана компания «Online
                        Book Technology». На
                        сегодняшний день более 800 салонов красоты Сингапура и более 500 салонов красоты Австралии
                        пользуются приложением по бронированию Салонов красоты. В 2017 году было принято решение по
                        выходу на рынок Казахстана с брендом «NOZH».”</h3 >

                </div >

            </div >
        </div >
    </div >

    <div class="k block-secondary ae" >
        <div class="e eq" >

            <div class="c ah" >
                <div class="gz ix gs kj gm ni" >
                    <h3 class="alp ag" >Функционал NOZH</h3 >
                </div >
            </div >

            <div class="c af" >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-11.svg" >


                    <p ><strong >Запись клиетов 24/7</strong ></p >
                </div >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-10.svg" >
                    <p ><strong >Уведомления клиенту и мастеру о предстоящей записи </strong ></p >
                </div >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-9.svg" >

                    <p ><strong >Ведение клиентской базы </strong ></p >
                </div >
            </div >

            <div class="c af" >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-14.svg" >

                    <p ><strong > Создание акции в личном кабинете </strong ></p >
                </div >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-13.svg" >

                    <p ><strong > Вход с любого устройства </strong ></p >
                </div >
                <div class="gc anh ah" >
                    <img class="ag" src="/img/landing/startup-12.svg" >

                    <p ><strong > Богатая статистика по клиентам и оказанным услугам </strong ></p >
                </div >
            </div >
        </div >
    </div >

    <div class="k ai" id="prices" >
        <div class="e eq" >

            <div class="c ah" >
                <div class="gs kj gm ni" >
                    <h6 class="am eu" >Дополнительная мотивация</h6 >

                </div >
            </div >

            <div class="c" >
                <div class="gc ana ah" >

                    <div class="ana" >
                        <h6 class="am eu ag" style="color:black" > AWARD по итогам 2018 года</h6 >
                    </div >

                    <ul class="fh sw eo ami" >
                        <li class="anb" >В каждой области</li >
                        <li class="anb" >Первые будут в ТОП-10 в течение 3 месяцев</li >
                        <li class="anb" >Вознаграждения от <strong >500 000 тенге</strong ></li >
                    </ul >

                </div >

                <div class="gc ana ah" >
                    <div class="ana" >
                        <h6 class="am eu ag" style="color:black" >5 номинаций каждый месяц</h6 >
                    </div >

                    <ul class="fh sw eo ami" >
                        <li class="anb" > Cамый активный мастер</li >
                        <li class="anb" > Cамый посещаемый салон красоты</li >
                        <li class="anb" > Cамый профессиональный мастер <br />(по количеству отзывов)</li >
                        <li class="anb" > Cамый лояльный мастер <br />(по оценкам клиентов)</li >
                        <li class="anb" > Cамый популярный салон красоты (лайки и отзывы в соц.сетях)</li >
                    </ul >

                    <a href="/auth/register" >
                        <button class="dl ao ot btn-block" >
                            Вступить в клуб
                        </button >
                    </a >
                </div >

                <div class="gc ana ah" >
                    <div class="ana" >
                        <h6 class="am eu ag" style="color:black" >3 публикации в соц. сетях</h6 >
                    </div >

                    <ul class="fh sw eo ami" >
                        <li class="anb" > О мастере/салоне</li >
                        <li class="anb" > Лайфхак от мастера (клиенты будут голосовать за самый полезный лайфхак)</li >
                        <li class="anb" > Интересная история (голосование)</li >

                    </ul >
                </div >
            </div >

        </div >
    </div >

    <div class="k al aj" >
        <div class="e" >
            <div class="c" >
                <div class="gg amf" >
                    <ul class="fh sv" >
                        <li class="ag" ><h6 class="eu" >О нас</h6 ></li >
                        <li class="am" >
                            “Мы создаем Клуб людей, которые хотят быть здоровыми, красивыми, ухоженными. У нас каждый
                            мастер найдет своего клиента, а каждый клиент найдет то, что искал”
                        </li >
                        <li style="margin-bottom:20px" ></li >
                        <li class="ag" ><h6 class="eu" >Контакты</h6 ></li >
                        <li >
                            <a href="tel:+77478324711" >
                                <i class="glyphicon glyphicon-phone" ></i >
                                +77088888877
                            </a >
                        </li >
                        <li >
                            <a href="mailto:partner@nozh.online" >
                                <i class="glyphicon glyphicon-envelope" ></i >
                                partner@nozh.kz
                            </a >
                        </li >
                    </ul >
                </div >
                <div class="fu kk amf" >
                </div >

            </div >
        </div >
    </div >

</div >

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js" ></script >
<script src="/js/landing/swiper.jquery.js" ></script >
<script src="/js/landing/toolkit.js" ></script >
<script src="/js/landing/application.js" ></script >

@if(app()->environment() == 'production')
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript' >
      (function () {
        var widget_id = 'nujvaymsS3'
        var s = document.createElement('script')
        s.type = 'text/javascript'
        s.async = true
        s.src = '//code.jivosite.com/script/widget/' + widget_id
        var ss = document.getElementsByTagName('script')[0]
        ss.parentNode.insertBefore(s, ss)
      })()</script >
    <!-- {/literal} END JIVOSITE CODE -->
@endif
</body >
</html >
