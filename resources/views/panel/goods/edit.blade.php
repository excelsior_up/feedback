@extends('panel.layout')

@section('page_title')
    Мой склад
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Расходные материалы',
            'dash_caption_small' => 'Добавление'
        ]
    )

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        {{-- <div class="pull-left">
                            <div class="box-title">
                                Редактировать мастера
                            </div>
                        </div> --}}
                    </div>

                    <hr>

                    <div class="box-body">
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::model($good, ['class' => 'form-horizontal', 'files' => true]) !!}
                                @include('panel.goods._form', ['button_caption' => 'Сохранить', 'hide_salon' => true])
                            {!! Form::close()!!}
                        </div>
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::open(['method' => 'post', 'url' => '/panel/goods/delete/'. $good->id])!!}
                                {!! Form::submit('Удалить?',['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
