@include('flash::message')

<div class="form-group custom-file-upload" flex>
    <img src="{{ isset($good) ? ($good->photo ? $good->photo : '/img/user.png') : '/img/user.png' }}" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
    <input type="file" id="image_changer_input" name="photo" accept="image/*"  />
</div>

<md-input-container flex>
    <label for="brand">Брэнд</label>
    {!! Form::text('brand', null, ['']) !!}
</md-input-container>

<md-input-container flex>
    <label for="name">Название</label>
    {!! Form::text('name', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    {{-- <label for="category">Категория</label> --}}
    <select class="form-control" name="category">
        <?php $categories=[
            'Лаки для ногтей',
            'Лаки для волос',
            'Краски для волос',
            'Шампуни',
            'Освежители',
            'Наполнители'
        ];?>

        @if(isset($categories))
            @foreach($categories as $category)
                <option value="{{$category}}"
                @if(isset($good))
                    @if($category == $good->category)
                        selected="selected"
                    @endif
                @endif
                >
                    {{$category}}
                </option>
            @endforeach
        @endif

    </select>
</md-input-container>

<md-input-container flex>
    <label for="options">Характеристики</label>
    {!! Form::text('options', null, ['']) !!}
</md-input-container>

<md-input-container flex>
    <label for="amount">Количество</label>
    {!! Form::text('amount', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="price">Цена за шт.</label>
    {!! Form::text('price', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="capacity">Объем (если предусмотрен)</label>
    {!! Form::text('capacity', null, ['']) !!}
</md-input-container>

<div class="box-footer">
    <md-button class="md-raised md-primary-btn pull-right">
        @if(isset($button_caption))
            {{$button_caption}}
        @else
            Добавить
        @endif
    </md-button>
</div>
