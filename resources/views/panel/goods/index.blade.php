@extends('panel.layout')

@section('page_title')
    Мой склад
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Мой склад',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
                <h3 class="box-title">
                    Список расходных материалов
                </h3>
            </div>

            <div class="box-body">
                <table id="clients-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Изображение</th>
                        <th>Подробнее</th>
                        <th>Брэнд</th>
                        <th>Название</th>
                        <th>Категория</th>
                        <th>Характеристики</th>
                        <th>Количество</th>
                        <th>Цена за шт.</th>
                        <th>Объем <br>
                            (если предусмотрен)
                        </th>
                        {{--<th>Салон</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($goods as $good)

                            <tr>
                                <td class="text-center">
                                    {{-- Avatar helper --}}
                                    @include('helpers.avatar_small',['avatar' => $good->photo ? $good->photo : null, 'caption' => $good->name])
                                </td>
                                <td>
                                    {{-- Ссылка на профиль --}}
                                    <a style="font-size:30px;" href="/panel/goods/info/{{$good->id}}">
                                        &nbsp;
                                        <i class="fa fa-list-alt"></i>
                                        &nbsp;
                                    </a>
                                </td>
                                <td>{{ $good->brand}}</td>
                                <td>{{ $good->name}}</td>
                                <td>{{ $good->category}}</td>
                                <td>{{ $good->options}}</td>
                                <td>{{ $good->amount}}</td>
                                <td>{{ $good->price}}</td>
                                <td>{{ $good->capacity}}</td>

                                {{--<td>{{ $master->salon->name }}</td>--}}
                            </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </section>

@stop
