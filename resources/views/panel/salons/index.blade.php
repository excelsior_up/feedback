@extends('panel.layout')

@section('page_title')
    Салоны
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Салоны',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <div class="row">
            <div class="container-fluid">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <div class="box-title">
                                Все салоны
                            </div>
                        </div>

                        {{--<div class="pull-right">
                            <md-button href="/panel/salons/add" class="md-raised md-primary-btn">Добавить салон</md-button>
                        </div>--}}

                    </div>

                    <div>
                        @include('flash::message')
                    </div>

                    <div class="box-body">
                        @forelse(array_chunk($salons->all(), 4) as $salon_rows)
                            <div class="row">
                                @foreach($salon_rows as $salon)
                                    <div class="col-md-4">
                                        <md-card>
                                            <div class="panel-heading">
                                                <b>{{ $salon->name }}</b>
                                            </div>
                                            <md-card-content class="panel-body">
                                                <div class="salonimg-wrap">
                                                    <img src="{{ $salon->image }}" alt="{{ $salon->name }}" class="img img-responsive"/>
                                                </div>
                                                <hr/>
                                                <p>
                                                    {{ $salon->description }}
                                                </p>
                                            </md-card-content>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-6 text-left text-center-sm">
                                                        <a href="/panel/salons/edit/{{ $salon->id }}" class="nowrap"><i class="fa fa-pencil fa-fw"></i> Редактировать</a>
                                                    </div>
                                                    {{--<div class="col-md-6 text-right text-center-sm">
                                                        <a href="/panel/salons/delete/{{ $salon->id }}"class="text-sm" style="color:red"><i class="fa fa-times fa-fw"></i> Удалить</a>
                                                    </div>--}}
                                                </div>
                                            </div>
                                        </md-card>
                                    </div>
                                @endforeach
                            </div>
                        @empty
                            <h4 class="text-center">
                                У вас пока нет салонов. Чтобы добавить, нажмите кнопку "Добавить салон".
                            </h4>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
