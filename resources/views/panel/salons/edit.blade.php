@extends('panel.layout')

@section('page_title')
    Управление салоном
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Салоны',
            'dash_caption_small' => 'Редактировать салон'
        ]
    )

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <div class="box-title">
                                Редактирование данных салона салон
                            </div>
                        </div>
                    </div>

                    <hr>
                    
                    <div class="box-body">
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::model($salon, ['class' => 'form-horizontal', 'files' => true]) !!}

                                @include('panel.salons._form', ['button_text' => 'Сохранить'])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
