<div >
    @include('flash::message')
</div >


<md-input-container flex >
    {!! Form::label('name', 'Название салона: ') !!}
    {!! Form::text('name', null, ['required']) !!}
</md-input-container >

<md-input-container flex >
    {!! Form::label('category', 'Категория ( Например, барбершоп ) ') !!}
    {!! Form::text('category', null, ['placeholder' => 'Например, барбершоп', 'required']) !!}

</md-input-container >


<div class="   input-field">
    {!! Form::label('city', 'Город: ') !!}
    {!! Form::select('city', ["Абай", "Акколь", "Аксай", "Аксу", "Актау", "Актобе", "Алга", "Алматы", "Арал", "Аркалык", "Арыс", "Астана", "Атбасар", "Атырау", "Аягоз", "Байконыр", "Балхаш", "Булаево", "Державинск", "Ерейментау", "Есик", "Есиль", "Жанаозен", "Жанатас", "Жаркент", "Жезказган", "Жем", "Жетысай", "Житикара", "Зайсан", "Зыряновск", "Казалинск", "Кандыагаш", "Капшагай", "Караганды", "Каражал", "Каратау", "Каркаралинск", "Каскелен", "Кентау", "Кокшетау", "Костанай", "Кулсары", "Курчатов", "Кызылорда", "Ленгер", "Лисаковск", "Макинск", "Мамлютка", "Павлодар", "Петропавловск", "Приозёрск", "Риддер", "Рудный", "Сарань", "Сарканд", "Сарыагаш", "Сатпаев", "Семей", "Сергеевка", "Серебрянск", "Степногорск", "Степняк", "Тайынша", "Талгар", "Талдыкорган", "Тараз", "Текели", "Темир", "Темиртау", "Туркестан", "Уральск", "Усть-Каменогорск", "Ушарал", "Уштобе", "Форт-Шевченко", "Хромтау", "Шардара", "Шалкар", "Шар", "Шахтинск", "Шемонаиха", "Шу", "Шымкент", "Щучинск", "Экибастуз", "Эмба"], null, [ 'required']) !!}

</div>


<md-input-container flex >
    {!! Form::label('address', 'Адрес салона: ') !!}
    {!! Form::text('address', null, ['required']) !!}
</md-input-container >

<md-input-container flex >
    {!! Form::label('address', 'Веб-сайт салона: ') !!}
    {!! Form::input('address', 'http://') !!}
</md-input-container >


<md-input-container flex >
    {!! Form::label('Instagram', 'Instagram: (Укажите логин, например, @beautysalon ') !!}
    {!! Form::text('url') !!}
</md-input-container >

<h3 >Расписание</h3 >

<table >
    <thead >
    <tr >
        <td >{!! Form::checkbox('Понедельник', 'Понедельник', false, ['id' => 'Понедельник']) !!}
            {!! Form::label('Понедельник', 'Понедельник', ['class' => 'control-label']) !!}
        </td >
        <td >{!! Form::checkbox('Вторник', 'Вторник', false, ['id' => 'Вторник']) !!}
            {!! Form::label('Вторник', 'Вторник', ['class' => 'control-label']) !!}</td >
        <td >{!! Form::checkbox('Среда', 'Среда', false, ['id' => 'Среда']) !!}
            {!! Form::label('Среда', 'Среда', ['class' => 'control-label']) !!}</td >
        <td >{!! Form::checkbox('Четверг', 'Четверг', false, ['id' => 'Четверг']) !!}
            {!! Form::label('Четверг', 'Четверг', ['class' => 'control-label']) !!}</td >
        <td >{!! Form::checkbox('Пятница', 'Пятница', false, ['id' => 'Пятница']) !!}
            {!! Form::label('Пятница', 'Пятница', ['class' => 'control-label']) !!}</td >
        <td >{!! Form::checkbox('Суббота', 'Суббота', false, ['id' => 'Суббота']) !!}
            {!! Form::label('Суббота', 'Суббота', ['class' => 'control-label']) !!}</td >
        <td >{!! Form::checkbox('Воскресенье', 'Воскресенье', false, ['id' => 'Воскресенье']) !!}
            {!! Form::label('Воскресенье', 'Воскресенье', ['class' => 'control-label']) !!}</td >
    </tr >
    </thead >
    <tbody >
    <tr >
        <td >
            <md-input-container flex >
                {!! Form::label('fromMon', 'C: ') !!}
                {!! Form::text('timeFromMon', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromTue', 'C: ') !!}
                {!! Form::text('timeFromTue', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromWen', 'C: ') !!}
                {!! Form::text('timeFromWen', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromThu', 'C: ') !!}
                {!! Form::text('timeFromThu', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromFri', 'C:') !!}
                {!! Form::text('timeFromFri', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromSat', 'C:') !!}
                {!! Form::text('timeFromSat', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('fromSun', 'C:') !!}
                {!! Form::text('timeFromSun', '08:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
    </tr >
    <tr >
        <td >
            <md-input-container flex >
                {!! Form::label('toMon', 'До: ') !!}
                {!! Form::text('timeFromMon', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toTue', 'До: ') !!}
                {!! Form::text('timeToTue', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toWen', 'До: ') !!}
                {!! Form::text('timeToWen', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toThu', 'До: ') !!}
                {!! Form::text('timeToThu', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toFri', 'До:') !!}
                {!! Form::text('timeToFri', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toSat', 'До:') !!}
                {!! Form::text('timeToSat', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
        <td >
            <md-input-container flex >
                {!! Form::label('toSun', 'До:') !!}
                {!! Form::text('timeToSun', '21:00', ['class' => 'form-control']) !!}
            </md-input-container >
        </td >
    </tr >
    </tbody >
</table >

<div class="form-group custom-file-upload" flex >
    <img src="{{ isset($salon) ? ($salon->image ? $salon->image : '/img/upload-area.jpg') : '/img/upload-area.jpg' }}"
         id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;" >
    <input type="file" id="image_changer_input" name="image" accept="image/*" />
</div >

<hr >

<div class="form-group" >
    <div class="col-sm-offset-2  col-sm-10 text-right" >
        <md-button class="md-raised md-primary-btn" >
            {{ $button_text }}
        </md-button >
    </div >
</div >
