@extends('panel.acquaintance')

@section('page_title')
    Услуги салона
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Салоны',
            'dash_caption_small' => 'Добавить салон'
        ]
    )
    <canvas id="confeti" class="active" ></canvas >

    <section class="content" >

        <div class="row" >
            <div class="col-md-10 noFloat centerIt" >
                <div class="box" >
                    <div class="box-header" >

                        <h2 class="text-center" >
                            Выберите услуги салона
                        </h2 >

                    </div >

                    <div >
                        @include('flash::message')
                    </div >

                    <hr >

                    <div class="box-body" >
                        <div class="col-md-12" >
                            {!! Form::open(['url' => ['panel/salon/add-services'], 'class' => 'form-horizontal', 'files' => true]) !!}

                            {!! Form::hidden('salon_id', $salon_id) !!}

                            <div >

                                <h3 >Категории</h3 >

                                <md-card >
                                    <ul class="tabs" >

                                        @foreach($categories as $category)
                                            <li class="tab col-xs-3 {{ $category->id == 1 ? ' active' : '' }}" >
                                                <a href="#tab-{{ $category->id }}"
                                                   aria-controls="tab-{{ $category->id }}" role="tab"
                                                   data-toggle="tab" >
                                                    {{ $category->name }}
                                                </a >
                                            </li >
                                        @endforeach

                                    </ul >
                                </md-card >
                                <!-- Tab panes -->
                                <div class="tab-content" >
                                    @foreach($categories as $category)
                                        <div role="tabpanel" class="tab-pane {{ $category->id == 1 ? 'active' : '' }}"
                                             id="tab-{{ $category->id }}" >
                                            @foreach($category->subcategories as $subcategory)

                                                <div class="panel-group" id="accordion" role="tablist"
                                                     aria-multiselectable="true" >
                                                    <div class="panel panel-default" >
                                                        <div class="panel-heading" role="tab"
                                                             id="heading{{ $subcategory->id }}" >
                                                            <h4 class="panel-title" >
                                                                <a role="button" data-toggle="collapse"
                                                                   data-parent="#accordion"
                                                                   href="#collapse{{ $subcategory->id }}"
                                                                   aria-expanded="true"
                                                                   aria-controls="collapse{{ $subcategory->id }}" >
                                                                    <b >
                                                                        {{ $subcategory->name }}
                                                                    </b >
                                                                </a >
                                                            </h4 >
                                                        </div >
                                                        <div id="collapse{{ $subcategory->id }}"
                                                             class="panel-collapse collapse in" role="tabpanel"
                                                             aria-labelledby="heading{{ $subcategory->id }}" >
                                                            <div class="panel-body" >

                                                                @foreach($subcategory->services as $service)
                                                                    <div class="col-md-4" >
                                                                        {!! Form::checkbox('service_ids[]', $service->id, false, ['id' => 'service-' . $service->id]) !!}
                                                                        {!! Form::label('service-' . $service->id, $service->name, ['class' => 'control-label']) !!}
                                                                    </div >
                                                                @endforeach
                                                            </div >
                                                        </div >
                                                    </div >
                                                </div >


                                            @endforeach
                                        </div >
                                    @endforeach
                                </div >
                            </div >


                            <div class="row" >
                                <div class="col-md-6 col-md-offset-3" >

                                    <h3 >Если Вы не нашли в нашем списке какую-либо услугу, сообщите нам и она появится
                                        в скором времени! </h3 >
                                    <md-input-container flex >
                                        {!! Form::label('newServices', 'Введите услуги через запятую') !!}
                                        {!! Form::text('newServices', null, ['class' => 'form-control']) !!}
                                        <md-button flex id="btnAdd" onclick="
                                                        var btn = document.querySelector('#btnAdd')
                                                       var list = document.querySelector('#newServicesList')
                                                       var  value = document.querySelector('#newServices').value;
                                                        if(value.length > 0){
                                                          var hdnList = document.querySelector('#hiddenList');
                                                           hdnList.setAttribute('value', list.innerText + ',' + value)
                                                           var li = document.createElement('li');
                                                           li.innerText = value;
                                                           list.appendChild(li);
                                                        }
                                                        " type="button"
                                                   class="btn btn-lg btn-clock block-btn md-raised md-primary-btn" >

                                            Добавить услугу
                                        </md-button >
                                        <input type="hidden" id="hiddenList" name="hiddenNewServices" >
                                        <ul id="newServicesList" >

                                        </ul >
                                        <script >

                                          btn.addEventListener('click', function (e) {

                                            e.preventDefault()

                                          })
                                        </script >
                                    </md-input-container >
                                </div >
                            </div >

                            <div class="form-group text-center" >
                                <div class="col-md-offset-1 col-sm-10" >
                                    <md-button flex type="submit"
                                               class="btn btn-lg btn-clock block-btn md-raised md-primary-btn" >
                                        Все услуги выбраны
                                    </md-button >
                                </div >
                            </div >
                            {!! Form::close() !!}
                        </div >
                    </div >
                </div >
            </div >
        </div >

    </section >

@stop
