@extends('panel.acquaintance')

@section('page_title')
    Добавить салон
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Салоны',
            'dash_caption_small' => 'Добавить салон'
        ]
    )
    <canvas id="confeti" class="active"></canvas>

    <section class="content">

        <div class="row">
            <div class="col-md-10 centerIt noFloat">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title col-xs-12 text-center">
                                  <h3> Добавление салона  </h3>
                            </div>
                        <div>
                    <hr>

                    <div class="box-body" ng-controller="serviceController">
                        <div class="col-md-12">
                            {!! Form::open(['class' => 'form-horizontal', 'files' => true]) !!}

                            @include('panel.salons._form', ['button_text' => 'Дальше'])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
