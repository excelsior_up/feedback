@extends('panel.acquaintance')

@section('page_title')
    Последний шаг
@stop

@section('content')

    @include('panel.partials.dashboard_navigator-aq',
        [
            'dash_caption' => 'Последний шаг',
            'dash_caption_small' => ''
        ]
    )
    <canvas id="confeti" class="active"></canvas>

    <div class="congrats-wrap" layout="row" layout-sm="column" layout-align="center center">
        <md-card class="col-md-8 noFloat centerIt ">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box"  layout="column" layout-align="center center">

            <div class="box-header">
                <h1 class="raspberryColor" style="margin-bottom: auto">
                    Остался последний шаг!
                </h1>
            </div>

            <div class="box-body" layout="row" layout-sm="column" layout-wrap="nowrap" layout-align="center center">

                <h2 class="text-center" flex="100" style="letter-spacing: 6px">
                    Уважаемая(ый) <span class="raspberry-color">{{ $user->name }} {{ $user->surname }}!</span>
                </h2>
                <hr class="divider">
                <p class="subhead" flex=70 style="line-height: 2;font-size: 1.9rem;">
                    Вы успешно справились с первичной настройкой. Наш менеджер скоро свяжется с Вами и детально проконсультирует по работе в системе и расскажет обо всех ее возможностях. А пока Вы можете ознакомиться с инструкциями, которые мы приготовили для Вас
                </p>

                <ul>
                    <li><a href="#">Краткий список возможностей NOZH</a></li>
                    <li><a href="#">Настройки сервиса по шагам</a></li>
                    <li><a href="#">Инструкция по работе для администратора </a></li>
                </ul>

            </div>

        </div>

    </md-card>
    </div>
@stop
