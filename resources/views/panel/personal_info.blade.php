@extends('panel.layout')

@section('page_title')
    Личный профиль
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Мой профиль',
            'dash_caption_small' => 'Персональная информация'
        ]
    )

    <section class="content">

        <div class="row">

            {{-- Виджет информации --}}
            <div class="col-md-6">

                <div class="box box-widget widget-user">

                    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username">
                            @if($user)
                                {{ $user->surname }}
                                {{ $user->name }}
                            @endif
                        </h3>
                        <h5 class="widget-user-desc">

                        </h5>
                    </div>
                    <div class="widget-user-image">
                        {{-- Avatar helper --}}
                        @include('helpers.avatar_small',['avatar' => $user->avatar ? $user->avatar : null, 'caption' => $user->name])
                    </div>
                    <div class="box-footer">
                        <div class="row">

                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        @if($user->salons->count())
                                            {{ $user->salons->first()->clients->count() }}
                                        @endif
                                    </h5>
                                    <span class="description-text">Клиенты</span>
                                </div>
                            </div>

                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        @if($user->salons->count())
                                            {{ $user->salons->first()->masters->count() }}
                                        @endif
                                    </h5>
                                    <span class="description-text">Мастеры</span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        @if($user->salons->count())
                                            {{ $user->salons->first()->services->count() }}
                                        @endif
                                    </h5>
                                    <span class="description-text">Услуги</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                {{-- Виджет изменения настроек профиля --}}
                @include('panel.profile_partials._settings', ['user' => $user])

            </div>


            <div class="col-md-6">

                {{-- Виджет изменения профиля --}}
                @include('panel.profile_partials._edit_profile', ['user' => $user])

                {{-- Виджет смены пароля --}}
                @include('panel.profile_partials._change_password', ['user' => $user])

            </div>

        </div>
        <div class="row">
            <div class="col-md-6">

            </div>
        </div>

    </section>
@stop
