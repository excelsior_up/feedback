<footer class="noMargin main-footer clearfix">
    <div class="col-md-4">
        <h5>Контакты</h5>
        <ul class="list-unstyled">
            <li>
                <span class="myIcons">
                        <img src="/img/icons/21.png">
                </span>
                Tel.:
                <span>
                    <a href="callto:+77478328711">+7 708 888 88 77</a>
                </span>
            </li>
            <li>
                 <span class="myIcons">
                        <img src="/img/icons/19.png">
                 </span>
                E-mail
                <span>
                    <a href="mailto:partner@nozh.online">partner@nozh.kz</a>
                </span>

            </li>
        </ul>
    </div>
    <div class="col-md-8">
        <h5>Правовая информация</h5>
        <ul class="list-unstyled">
            <li>
                <span class="myIcons">
                    <img src="/img/icons/22.png">
                </span>
                <a href="">Политика конфиденциальности</a></li>
            <li>
                <span class="myIcons">
                    <img src="/img/icons/23.png">
                </span>
                <a href="">Соглашение об использовании сервиса nozh.online</a></li>
        </ul>
    </div>

</footer>
