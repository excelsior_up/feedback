<header class="main-header">
    <!-- Logo -->
    <a href="/panel" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <img id="logoBig" src="{{ asset('./img/logo.png') }}" alt="CRM Feedback" width="40px">
        <img id="logoSmall"  src="{{ asset('./img/logo.png') }}" alt="CRM Feedback" width="40px">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">



        <div id="relative">

            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Переключение навигации</span>
            </a>

            <div><img src="/img/nozh/logo.png" style="width: 50px;"></div>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    {{-- Меню сообщений в шапке --}}
                    {{--@include('panel.partials.partials_header.messages')

                    --}}{{-- Уведомления --}}{{--
                    @include('panel.partials.partials_header.notifications')

                    --}}{{-- Задания --}}{{--
                    @include('panel.partials.partials_header.tasks')--}
                    {{-- Меню аккаунта пользователя --}}
                    @include('panel.partials.partials_header.account')

                            <!-- Control Sidebar Toggle Button -->
                    {{--<li>
                        <a href="#" data-toggle="control-sidebar">
                            <i class="fa fa-gears"></i>
                            Настройки
                        </a>
                    </li>--}}
                </ul>
            </div>

        </div>

    </nav>
</header>
