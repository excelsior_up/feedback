<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="{{ $user->avatar ? $user->avatar : '/img/user.png' }}" class="user-image" alt="User Image">
        <span class="hidden-xs username">
            @if($user)
                {{$user->name}}
                {{$user->surname}}
            @endif
        </span>
    </a>
    <ul class="dropdown-menu">

        {{-- Информация о пользователе и салоне --}}
        <li class="user-header">

            {{-- Изображение пользователя --}}
            <img src="{{ $user->avatar ? $user->avatar : '/img/user.png' }}" class="img-circle" alt="User Image">

            {{-- Изображение (лого) салона
            <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}

            {{-- Информация о пользователе и салоне --}}
            <p>
                @if($user)
                    {{ $user->name }} {{ $user->surname }}
                    <small>
                        В Feedback с
                        {{ date('d.m.Y', strtotime($user->created_at)) }}
                    </small>
                @endif
            </p>

        </li>

        {{-- Меню --}}
        <li class="user-body">
            <div class="col-xs-4 text-center">
                <a href="/panel/salons">Салоны</a>
            </div>
            <div class="col-xs-4 text-center">
                <a href="/panel/masters">Мастера</a>
            </div>
            <div class="col-xs-4 text-center">
                <a href="/panel/clients">Клиенты</a>
            </div>
        </li>

        {{-- Подвал меню --}}
        <li class="user-footer">
            <div class="pull-left">
                <md-button href="/panel/profile" class="md-raised md-primary-btn">
                     Профиль
                </md-button>
            </div>
            {{-- Выйти из аккаунта --}}
            <div class="pull-right">
                <md-button href="/auth/logout" class="md-raised md-primary-btn">
                     Выйти
                </md-button>
            </div>
        </li>
    </ul>
</li>
