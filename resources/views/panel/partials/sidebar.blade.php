<section>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" >
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    {{-- Avatar helper --}}
                    @include('helpers.avatar_extra_small',['avatar' => $user->avatar ? $user->avatar : null, 'caption' => $user->name, 'size' => 45])
                </div>
                <div class="pull-left info">
                    <p class="username">
                        @if(Auth::director()->check())
                            {{ Auth::director()->get()->name }}
                            {{ Auth::director()->get()->surname }}
                        @endif
                    </p>
                    @if($user->salons->count())
                        <a href="/panel/salons/edit/{{ $user->salons->first()->id }}"><i class="fa fa-circle online"></i> {{ $user->salons->first()->name }}</a>
                    @endif
                </div>
            </div>
            <!-- search form -->
            {{--<form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>--}}
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->

                <ul class="sidebar-menu content mCustomScrollbar" id="sidebar">
                    <li>
                        <a class="hi" href="/panel">
                            <div class="myIcons">
                                <img src="/img/icons/1.png">
                            </div>

                            <span>Панель управления</span>
                        </a>
                    </li>
                    <li>
                        <a class="hi" href="/panel/table">
                            <div class="myIcons">
                                <img src="/img/icons/2.png">
                            </div>
                            <span>Журнал записей</span>
                        </a>
                    </li>
                    <li>
                        <a class="hi" href="/panel/profile">
                            <div class="myIcons">
                                <img src="/img/icons/3.png">
                            </div>
                            <span>Мой профиль</span>
                        </a>
                    </li>
                    <li class="treeview">
                        {{--<a href="#">
                            <i class="fa fa-paint-brush"></i>
                            <span>Салоны</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/panel/salons">
                                <i class="fa fa-reorder"></i> Все салоны</a>
                            </li>
                            <li><a href="/panel/salons/add">
                                <i class="fa fa-plus"></i> Добавить</a>
                            </li>
                        </ul>--}}
                        <a class="hi" href="{{ $user->salons->count() ? '/panel/salons/edit/' . $user->salons->first()->id : '/panel/salons/add' }}">
                            <div class="myIcons">
                                <img src="/img/icons/4.png">
                            </div>
                            <span>Салон</span>
                        </a>
                    </li>
                    <li>
                        <a class="hi" href="/panel/services">
                            <div class="myIcons">
                                <img src="/img/icons/5.png">
                            </div>
                            <span>Услуги</span>
                        </a>
                    </li>
                    <li>
                        <a class="hi" href="/panel/outcomes">
                            <div class="myIcons">
                                <img src="/img/icons/6.png">
                            </div>
                            <span>Затраты</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a class="hi" href="#">
                            <div class="myIcons">
                                <img src="/img/icons/7.png">
                            </div>
                            <span>Мой склад</span>
                            <i class="fa  addShadow fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu  sub-menu">
                            <li class="treeview">

                                {{-- Расходные материалы --}}
                                <li class="treeview">
                                    <a class="gi" href="#">
                                        <div class="myIcons">
                                            <img src="/img/icons/8.png">
                                        </div>
                                        <span>Расходный материал</span>
                                        <i class="fa  addShadow fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="treeview">

                                            <li><a href="/panel/goods">
                                                <i class="fa addShadow  fa-reorder"></i> Список</a>
                                            </li>
                                            <li><a href="/panel/goods/add">
                                                <i class="fa  addShadow fa-plus"></i> Добавить</a>
                                            </li>

                                        </li>
                                    </ul>
                                </li>

                                {{-- Товары на продажу --}}
                                <li class="treeview">
                                    <a class="gi" href="#">
                                        <div class="myIcons">
                                            <img src="/img/icons/9.png">
                                        </div>
                                        <span>Товары на продажу</span>
                                        <i class="fa  addShadow fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="treeview">

                                            <li><a href="/panel/goods">
                                                <i class="fa addShadow  fa-reorder"></i> Список</a>
                                            </li>
                                            <li><a href="/panel/goods/add">
                                                <i class="fa  addShadow fa-plus"></i> Добавить</a>
                                            </li>

                                        </li>
                                    </ul>
                                </li>

                            </li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a class="hi" href="#">
                            <div class="myIcons">
                                <img src="/img/icons/15.png">
                            </div>
                            <span>Клиенты</span>
                            <i class="fa addShadow  fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu  sub-menu">
                            <li><a href="/panel/clients">
                                <i class="fa addShadow  fa-reorder"></i> Список</a>
                            </li>
                            <li><a href="/panel/clients/add">
                                <i class="fa  addShadow fa-plus"></i> Добавить</a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a class="hi" href="#">
                            <div class="myIcons">
                                <img src="/img/icons/16.png">
                            </div>
                            <span>Мастера</span>
                            <i class="fa  addShadow fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu  sub-menu">
                            <li><a href="/panel/masters"><i class="fa  addShadow fa-reorder"></i> Список</a></li>
                            <li><a href="/panel/masters/add"><i class="fa  addShadow fa-plus"></i> Добавить</a></li>
                        </ul>
                    </li>
                    {{--<li class="treeview">
                        <a href="#">
                            <i class="fa fa-hand-pointer-o"></i>
                            <span>Администраторы</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/panel/admins"><i class="fa fa-reorder"></i> Список</a></li>
                            <li><a href="/panel/admins/add"><i class="fa fa-plus"></i> Добавить</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-hand-paper-o"></i>
                            <span>Менеджеры</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="/panel/managers"><i class="fa fa-reorder"></i> Список</a></li>
                            <li><a href="/panel/managers/add"><i class="fa fa-plus"></i> Добавить</a></li>
                        </ul>
                    </li>--}}
        {{--
                    <li class="header">Уведомления</li>
                    <li>
                        <a href="#">
                            <i class="fa addShadow  fa-circle-o text-red"></i>
                            <span>Important</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-circle-o text-yellow"></i>
                            <span>Warning</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-circle-o text-aqua"></i>
                            <span>Information</span>
                        </a>
                    </li>
          --}}
                    <li>
                        <a  class="hi" href="/auth/logout">
                            <div class="myIcons">
                                <img src="/img/icons/17.png">
                            </div>
                            <span>Выход</span>
                        </a>
                    </li>
        {{--         @if($user->salons->count() && ! $user->salons->first()->site)
                     <li class="header">&nbsp;</li>
                     <li>
                             <div class="alert alert-warning success-rasp" role="alert">
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                 <br>

                                 <h5>У Вашего салона нет веб-сайта?</h5>

                                 <p style="white-space: normal; line-height: 20px">Вам несказанно повезло, мы предлагает Вам создать сайт со 20% скидкой!</p>
                                 <br>
                                 <a href="http://excelsior.su" target="_blank" class="alert-link">Excelsior.su</a>
                             </div>
                     </li>
                 @endif --}}
                </ul>


     </section>
     <!-- /.sidebar -->
    </aside>
</section>
