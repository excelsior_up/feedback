<!-- Modal -->
<div class="modal fade" id="newService" tabindex="-1" role="dialog" aria-labelledby="newServiceLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="newServiceLabel">
                    Добавление новой оказанной услуги
                </h4>
            </div>

            {!! Form::open(['url' => 'panel/transactions/new']) !!}

                <div class="modal-body">

                    <div class="form-group">
                        {!! Form::label('service_id', 'Выберите сервис: ') !!}
                        <select class="form-control" name="service_id" id="service_id">
                            @forelse($user->salons->first()->services as $salon_service)
                                <option value="{{ $salon_service->id }}">
                                    {{ $salon_service->service->name }}
                                </option>
                            @empty
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group">
                        {!! Form::label('client_id', 'Выберите клиента: ') !!}
                        <select class="form-control" name="client_id" id="client_id">
                            @forelse($user->salons->first()->clients as $client)
                                <option value="{{ $client->id }}" aria-label="опция">
                                    {{ $client->name }}
                                </option>
                            @empty
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group">
                        {!! Form::label('master_id', 'Выберите мастера: ') !!}
                        <select class="form-control" name="master_id" id="master_id">
                            @forelse($user->salons->first()->masters as $master)
                                <option value="{{ $master->id }}">
                                    {{ $master->name }}
                                </option>
                            @empty
                            @endforelse
                        </select>
                    </div>

                    {{--<div class="form-group">
                        <select class="select2">
                          <option value="AL">Alabama</option>
                          <option value="WY">Wyoming</option>
                        </select>
                    </div>--}}

                    <md-input-container flex>
                        {!! Form::label('price', 'Введите сумму: ') !!}
                        {!! Form::text('price', null, ['class' => 'form-control', 'required']) !!}
                    </md-input-container>

                    <md-input-container flex>
                        {!! Form::label('comment', 'Введите комментарий (не обязательно): ') !!}
                        {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 3]) !!}
                    </md-input-container>

                    <div class="form-group">
                        {!! Form::label('', 'Оцените работу мастера: ') !!}
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                            @foreach(range(1, 5) as $rate)
                                <div class="btn-group" role="group">
                                    <input type="radio" name="rate" id="rate-{{ $rate }}" value="{{ $rate }}" {{ $rate == 5 ? 'checked' : '' }}>
                                    <label for="rate-{{ $rate }}">{{ $rate }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <md-button class="md-raised md-warn" data-dismiss="modal">Отмена</md-button>
                    <input class="angular-button" type="submit" value="Добавить"/>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--
Скрипт для изменения мастеров и клиентов когда много салонов
<script>
    function salonChanged(salon)
    {
        var services_content = '',
                clients_content = '',
                masters_content = '';

        @foreach($user->salons as $salon)
            if ( {{ $salon->id }} == $(salon).val() )
            {
                @forelse($salon->services as $salon_service)
                    services_content += '<option value="{{ $salon_service->id }}">{{ $salon_service->service->name }}</option>';
                @empty
                @endforelse

                @forelse($salon->clients as $client)
                    clients_content += '<option value="{{ $client->id }}">{{ $client->name }}</option>';
                @empty
                @endforelse

                @forelse($salon->masters as $master)
                    masters_content += '<option value="{{ $master->id }}">{{ $master->name }}</option>';
                @empty
                @endforelse
            }
        @endforeach

        $("#service_id").html(services_content);
        $("#client_id").html(clients_content);
        $("#master_id").html(masters_content);
    }
</script>--}}
