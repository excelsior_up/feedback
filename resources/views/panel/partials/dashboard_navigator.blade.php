<section class="content-header col-md-6 noFloat centerIt">
    <h1>
        @if(isset($dash_caption))
            <span class="blackStyle">{{$dash_caption}}</span>
        @else
            <span class="blackStyle">Панель управления</span>
        @endif
        <small class="displayBlock">
            @if(isset($dash_caption_small))
                {{ $dash_caption_small }}
            @else

            @endif
        </small>
    </h1>

</section>
