<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Изменение данных профиля</h3>
    </div>
        {!! Form::model( $user,
            [
                'method' => 'POST',
                'action' => 'DirectorController@postEdit',
                'files' => true
            ])
        !!}

        <div class="box-body">

            <div class="form-group custom-file-upload" flex>
                <img src="{{ isset($user) ? ($user->avatar ? $user->avatar : '/img/user.png') : '/img/user.png' }}" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
                <input type="file" id="image_changer_input" name="avatar" accept="image/*"  />
            </div>

            <md-input-container flex>
              <label for="surname">
                  Фамилия
              </label>
              {!! Form::text('surname', null, ['class' => 'form-control']) !!}
            </md-input-container>

            <md-input-container flex>
              <label for="name">
                  Имя
              </label>
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </md-input-container>

            <md-input-container flex>
              <label for="email">
                  Email
              </label>
              {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </md-input-container>

            <md-input-container flex>
                <label for="phone">
                    Телефон
                </label>
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </md-input-container>

            <md-button type="submit" class="md-raised md-primary-btn pull-right">
                Сохранить изменения
            </md-button>

        </div>

    {!! Form::close() !!}
</div>
