<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Настройки e-mail уведомлений</h3>
    </div>
        {!! Form::model( $settings,
            [
                'method' => 'POST',
                'action' => 'DirectorController@postSettings',
            ])
        !!}

        <div class="box-body">

            <div>
                <div class="form-block">
                    <input type="checkbox" name="add_client_mail" id="add_client_mail" class="form-control" @if($settings->add_client_mail == 1) checked="checked" @endif>
                    {!! Form::label('add_client_mail', 'Добавление клиента') !!}
                </div>

                <div class="form-block">
                    <input type="checkbox" name="add_master_mail" id="add_master_mail" class="form-control" @if($settings->add_master_mail == 1) checked="checked" @endif>
                    {!! Form::label('add_master_mail', 'Добавление мастера') !!}
                </div>

                <div class="form-block">
                    <input type="checkbox" name="add_outcome_mail" id="add_outcome_mail" class="form-control" @if($settings->add_outcome_mail == 1) checked="checked" @endif>
                    {!! Form::label('add_outcome_mail', 'Добавление данных о затратах') !!}
                </div>

                <div class="form-block">
                    <input type="checkbox" name="add_transaction_mail" id="add_transaction_mail" class="form-control" @if($settings->add_transaction_mail == 1) checked="checked" @endif>
                    {!! Form::label('add_transaction_mail', 'Добавление оказанной услуги') !!}
                </div>

                <div class="form-block">
                    <input type="checkbox" name="add_profile_password_mail" id="add_profile_password_mail" class="form-control" @if($settings->add_profile_password_mail == 1) checked="checked" @endif>
                    {!! Form::label('add_profile_password_mail', 'Изменение пароля в профиле') !!}
                </div>
            </div>


            <md-button type="submit" class="md-raised md-primary-btn pull-right">
                Сохранить изменения
            </md-button>

        </div>

    {!! Form::close() !!}
</div>
