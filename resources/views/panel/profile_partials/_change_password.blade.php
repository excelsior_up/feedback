<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Изменение пароля</h3>
    </div>
        {!! Form::open(
            [
                'method' => 'POST',
                'action' => [
                    'DirectorController@postPassword',
                    $user->id
                ]
            ])
        !!}

      <div class="box-body">

          {{-- Если есть ошибки --}}
          @if(Session::has('err_msg') and Session::has('err_dsc'))
            <div class="callout callout-danger">
                <h4>{{Session::get('err_msg')}}</h4>
                <p>{{Session::get('err_dsc')}}</p>
            </div>
          @endif

          {{-- Если успешно поменять пароль --}}
          @if(Session::has('suc_msg') and Session::has('suc_dsc'))
            <div class="callout callout-success">
                <h4>{{Session::get('suc_msg')}}</h4>
                <p>{{Session::get('suc_dsc')}}</p>
            </div>
          @endif

        <md-input-container flex>
          <label for="inputPassword1">
              Старый пароль
          </label>
          <input name="oldpassword" type="password" class="form-control" id="inputPassword1" >
        </md-input-container>

        <md-input-container flex>
          <label for="inputPassword2">
              Новый пароль
          </label>
          <input name="password" type="password" class="form-control" id="inputPassword2" >
        </md-input-container>

        <md-input-container flex>
          <label for="inputPassword3">
              Новый пароль (еще раз)
          </label>
          <input name="repassword" type="password" class="form-control" id="inputPassword3">
        </md-input-container>

        <md-button type="submit" class="md-raised md-primary-btn pull-right">Изменить пароль</md-button>

      </div>

    {!! Form::close() !!}
</div>
