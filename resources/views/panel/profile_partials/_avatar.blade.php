<div class="box box-info">

    <div class="box-header with-border">
        <h3 class="box-title">Изменение аватара пользователя</h3>
    </div>

    {!! Form::open(
        [
            'method' => 'POST',
            'action' => 'DirectorController@postAvatar',
            'files' => true
        ])
    !!}

        <div class="form-group custom-file-upload" flex>
            <img src="{{ isset($user) ? ($user->avatar ? $user->avatar : '/img/user.png') : '/img/user.png' }}" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
            <input type="file" id="image_changer_input" name="avatar" multiple  />
        </div>

        <div class="box-footer">
            <md-button class="md-raised md-primary-btn pull-right">
                @if(isset($button_caption))
                    {{$button_caption}}
                @else
                    Заменить
                @endif
            </md-button>
        </div>

    {!! Form::close() !!}

</div>
