<!-- quick email widget -->
<div class="box box-solid ">
    <div class="box-header">
        <span class="myIcons">
                <img src="/img/icons/19.png">
        </span>
        <h3 class="box-title">Написать Email</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
    </div>
    {!! Form::open(['url' => '/panel/send-email']) !!}
        <div class="box-body">
            <div class="form-group">
                {!! Form::label('email', 'Кому: ') !!}
                <select class="select2 form-control" name="email" id="email" style="height:35px">
                    @foreach($user->salons->first()->clients as $client)
                        <option value="{{ $client->email }}">
                            Клиент: {{ $client->name }} {{ $client->surname }}
                        </option>
                    @endforeach
                    @foreach($user->salons->first()->masters as $master)
                        <option value="{{ $master->email }}">
                            Мастер: {{ $master->name }} {{ $master->surname }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="subject" placeholder="Тема" required>
            </div>
            <div>
                <textarea class="textarea" placeholder="Письмо" name="text" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
            </div>
        </div>

        <div class="box-footer clearfix">
            <button class="pull-right btn btn-default" id="sendEmail">Отправить <i class="fa fa-arrow-circle-right"></i></button>
        </div>

    {!! Form::close() !!}

</div>