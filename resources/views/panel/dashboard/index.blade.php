@extends('panel.layout')

@section('content')


    {{-- Подключение навигатора --}}
    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Панель управления',
            'dash_caption_small' => '',
            'hide_bread' => true
        ]
    )

    <!-- Main content -->
    <section class="content">

        {{-- Информационные блоки-виджеты --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box clearfix">
                    <div class="box-header with-border">
                        <h3 class="box-title ">Отчет на сегодня</h3>
                </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div >
                            <span class="info-box-icon">
                                <span class="myIcons">
                                    <img src="/img/icons/24.png" style="width: 82px;">
                                </span>
                            </span>
                            <div class="info-box-content clearfix">
                                <span class="info-box-text addLightBorder">Прибыль</span>
                                <span class="info-box-number   oswaldStyle notVisible counter">
                                    {{-- {{ $dashboard['today']['amountOfMoney'] }}  --}}
                                    {{$todayProfit[2]}}
                                      {{-- <small>&#8376;</small> --}}
                                      <i class="tengepng"></i>
                                       <br>
                                </span>
                                <span style="color:green;" class="col-xs-6 upDown oswaldStyle">
                                    <i class="fa fa-sort-up"></i>
                                    <span class=" notVisible counter">{{$todayProfit[0]}}</span>
                                      {{-- <small>&#8376;</small>  --}}
                                      <i class="tengepng"></i>
                                    <br>
                                </span>

                                <span style="color:red;" class="col-xs-6 upDown oswaldStyle" >
                                    <i class="fa fa-sort-desc"></i>
                                    <span class=" notVisible counter">{{$todayProfit[1]}}</span>
                                    {{-- <small>&#8376;</small> --}}
                                    <i class="tengepng"></i>
                                </span>


                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">

                        <div >
                            <span class="info-box-icon">
                                <span class="myIcons">
                                    <img src="/img/icons/5.png" style="width: 82px;">
                                </span>
                            </span>
                            <div class="info-box-content clearfix">
                                <span class="info-box-text addLightBorder">Оказано услуг</span>
                                <span class="info-box-number  notVisible counter oswaldStyle">{{ $dashboard['today']['numberOfServices'] }}</span>
                            </div>
                        </div>
                    </div>

                    {{-- Исправление для устройств с небольшими экранами --}}
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div >
                            <span class="info-box-icon">
                                <span class="myIcons">
                                    <img src="/img/icons/15.png" style="width: 82px;">
                                </span>
                            </span>
                            <div class="info-box-content clearfix">
                                <span class="info-box-text addLightBorder">Посетителей</span>
                                <span class="info-box-number notVisible counter oswaldStyle">{{ $dashboard['today']['numberOfClients'] }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <span class="info-box-text addLightBorder">Погода</span>
                        <div id="gsInformerID-3r343bn3D2h8uB" class="gsInformer" style="width:214px;height:95px">
                            <div class="gsIContent">
                                <div id="cityLink">
                                    <a href="#" target="_blank">Погода в Алматы</a>
                                </div>
                                <div class="gsLinks">
                                </div>
                            </div>
                        </div>
                        <script async src="https://www.gismeteo.kz/informers/-/n0C0nt3jKAiqN1" type="text/javascript"></script>
                    </div>


                </div>
            </div>
        </div>

        <div>
            @include('flash::message')
        </div>

        {{-- Основное наполнение панели управления --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Отчет на неделю</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height:230px"></canvas>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header  notVisible counter oswaldStyle">{{ $weekProfit['income'] }}  <i class="tengepng"></i></h5>
                                    <span class="description-text">Доход</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header  notVisible counter oswaldStyle">{{ $weekProfit['outcome'] }}  <i class="tengepng"></i></h5>
                                    <span class="description-text">Расход</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header  notVisible counter oswaldStyle">{{ $weekProfit['profit'] }}  <i class="tengepng"></i></h5>
                                    <span class="description-text">Прибыль</span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-footer -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">

                {{-- Последние Услуги --}}
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Последние услуги</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Клиент</th>
                                        <th>Мастер</th>
                                        <th>Услуга</th>
                                        <th>Оценка</th>
                                        <th>Цена</th>
                                        <th>Комментарий</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions as $transaction)
                                        <tr>
                                            <td>
                                                <a href="/panel/clients/profile/{{ $transaction->client->id }}">
                                                    {{ $transaction->client->name }} {{ $transaction->client->surname }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/panel/masters/profile/{{ $transaction->master->id }}">
                                                    {{ $transaction->master->name }} {{ $transaction->master->surname }}
                                                </a>
                                            </td>
                                            <td>
                                                {{-- Исправление --}}
                                                {{App\SalonService::where('id',$transaction->service_id)->first()->service->name}}
                                                {{-- {{$transaction->service->name}} --}}
                                            </td>
                                            <td>
                                                {{ $transaction->rate }}
                                            </td>
                                            <td>
                                                {{ $transaction->price }} тг.
                                            </td>
                                            <td>
                                                {{ $transaction->comment }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if( ! $transactions->count())
                                <h5 class="text-center">
                                    <em>Пока нет посетителей. Это очень легко исправить.</em> <md-button data-toggle="modal"  class="md-raised md-primary" data-target="#newService">Добавить</md-button>
                                </h5>
                            @endif
                        </div><!-- /.table-responsive -->
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <md-button data-toggle="modal"  class="md-raised md-primary" data-target="#newService">Добавить новую услугу</md-button>
                    </div><!-- /.box-footer -->
                </div>

                {{-- Блок последних затрат --}}
                @include('panel.dashboard._latest_outcomes', ['latestOutcomes' => $latestOutcomes, 'hide_add' => true])

                <div class="row">

                    <div class="col-md-12">

                        {{-- calendar --}}
                        @include('panel.dashboard._calendar')

                    </div>

                    {{--<div class="col-md-6">

                        --}}{{-- Блок чата --}}{{--
                        @include('panel.dashboard._chat_box')

                    </div>

                    <div class="col-md-6">

                        --}}{{-- Блок списка задач --}}{{--
                        @include('panel.dashboard._todo_list')

                    </div><!-- /.col -->--}}
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        {{-- quick email --}}
                        @include('panel.dashboard._email')
                    </div>
                </div>

            </div><!-- /.col -->

            {{-- Правая колонка --}}
            <div class="col-md-4">

                {{-- Блок предстоящих дней рождения --}}
                @include('panel.dashboard._birthdays', ['clients' => $upcomingBirthdays])

                {{-- Блок доходов по мастерам --}}
                @include('panel.dashboard._masters_profit', ['mastersProfit' => $mastersProfit])

                {{-- Блок популярных услуг --}}
                @include('panel.dashboard._popular_services', ['transactions' => $popularServices])

                {{-- Блок последних посетителей --}}
                @include('panel.dashboard._latest_visitors')

            </div>

        </div>

    </section>

@stop
