<!-- USERS LIST -->
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Последние посетители</h3>
        <div class="box-tools pull-right">
            {{--<span class="label label-danger">8 Новых Клиентов</span>--}}
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <ul class="users-list clearfix">
            @forelse($latestVisitors as $visitor)
                <li>
                    {{-- Аватар (Avatar helper) --}}

                    @include('helpers.avatar_extra_small', ['avatar' => $visitor->client->avatar ? $visitor->client->avatar : null, 'caption' => $visitor->client->name])

                    <a class="users-list-name" href="/panel/clients/profile/{{ $visitor->client->id }}" title="{{ $visitor->client->name }} {{ $visitor->client->surname }}">
                        {{ $visitor->client->name }}
                    </a>
                    <span class="users-list-date">{{ date('d.m', strtotime($visitor->created_at)) }}</span>
                </li>
            @empty
                <h5 class="text-center" >
                   <em>Пока нет посетителей. <br> Это очень легко исправить. <br> </em><br>
                    <md-button onclick="window.location = '/panel/clients/add';" onclick="window.location = '/panel/masters/add';" class="col-xs-12 noMargin md-raised md-primary" data-toggle="modal"> Добавить</md-button>

                </h5>
            @endforelse
        </ul><!-- /.users-list -->
    </div><!-- /.box-body -->
    <div class="box-footer text-center">
        @if($latestVisitors->count())
            <a href="#" class="uppercase">Смотреть всех посетителей</a>
        @endif
    </div><!-- /.box-footer -->
</div><!--/.box -->
