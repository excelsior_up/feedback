<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Последние затраты</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">

            <table class="table table-bordered table-striped">

                    @if(! isset($hide_add))
                    <thead>
                    <tr>
                        <th>Предмет затраты</th>
                        <th>Категория</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Общая стоимость</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {!! Form::open(['method' => 'POST', 'action' => 'OutcomesController@postAdd']) !!}
                                <td>{!! Form::text('name', null)!!}</td>
                                <td>{!! Form::text('category', null)!!}</td>
                                <td>{!! Form::text('quantity', null)!!}</td>
                                <td>{!! Form::text('price', null)!!}</td>
                                <td>{!! Form::submit('Добавить', ['class' => 'btn btn-success'])!!}</td>
                            {!! Form::close() !!}
                        </tr>
                    @endif
                    @forelse($latestOutcomes as $outcome)
                        <thead>
                        <tr>
                            <th>Предмет затраты</th>
                            <th>Категория</th>
                            <th>Количество</th>
                            <th>Цена</th>
                            <th>Общая стоимость</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$outcome->name}}</td>
                                <td>{{$outcome->category}}</td>
                                <td>{{$outcome->quantity}}</td>
                                <td>{{$outcome->price}}</td>
                                <td>{{$outcome->sum}}</td>
                            </tr>

                    @empty
                        <h5 class="text-center">
                            <em>Вы не производили затрат. Это очень легко исправить.</em>
                        </h5>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="box-footer clearfix">
        {{-- <a data-toggle="modal" class="btn btn-sm btn-info btn-flat pull-left" data-target="#newService">
            Добавить новую услугу
        </a> --}}
        <a href="/panel/outcomes" class="">
            <md-button class="md-raised md-primary" >Перейти к затратам</md-button>
        </a>
    </div><!-- /.box-footer -->
</div>
