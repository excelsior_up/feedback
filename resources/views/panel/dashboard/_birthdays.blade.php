{{-- Основной бокс (контейнер) --}}
<div class="box box-danger">

    {{-- Заголовок --}}
    <div class="box-header with-border">

        {{-- Текст заголовка --}}
        <h3 class="box-title">
            <span class="myIcons">
                <img src="/img/icons/20.png">
            </span>
            Ближайшие Дни Рождения
        </h3>

        {{-- Кнопки закрытия и коллапса для бокса (контейнера) --}}
        {{-- <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> --}}

    </div>

    {{-- Основной контейнер --}}
    <div class="box-body no-padding">

        {{-- Список клиентов --}}
        <ul class="users-list clearfix">

            {{-- Проход по всем клиентам, с выбранными днями рождения --}}
            @forelse($clients as $client)
            <li>

                {{-- Аватар (Avatar helper) --}}
                @include('helpers.avatar_extra_small', ['avatar' => $client->avatar ? $client->avatar : null, 'caption' => $client->name])

                {{-- Ссылка на профиль клиента --}}
                <a class="users-list-name " href="/panel/clients/profile/{{$client->id}}">
                    {{$client->surname}}
                </a>

                {{-- Высчитывание метки даты рождения --}}
                @if(Carbon\Carbon::now()->format('m-d') == Carbon\Carbon::createFromFormat('Y-m-d', $client->birthday)->format('m-d'))
                    {{-- День рождения клиента сегодня --}}
                    <span class="label label-success">
                        сегодня
                    </span>
                @else
                    {{-- Метка месяца и дня рождения --}}
                     <span class="users-list-date">{{ Carbon\Carbon::createFromFormat('Y-m-d',$client->birthday)->format('d.m') }}</span>
                @endif

            </li>


            @empty
                <h5 class="text-center">
                    <em>Ближайших дней рождения не предвидится.</em>
                </h5>
            @endforelse

        </ul>

    </div>

    {{-- Подвал блока --}}
    <div class="box-footer text-center">

        {{-- Ссылка на таблицу пользователей --}}
        <a href="/panel/clients" class="">
            <md-button md-no-ink class="md-primary"> Показать всех клиентов</md-button>
        </a>
    </div>

</div>
