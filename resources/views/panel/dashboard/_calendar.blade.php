<!-- Calendar -->
<div class="box box-solid box" ng-controller="calendarCtrl">
    <div class="box-header with-border">
        <span class="myIcons">
                <img src="/img/icons/2.png">
        </span>
        <h3 class="box-title">Календарь</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            {{--<div class="btn-group">
                <butaston class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#">Add new event</a></li>
                    <li><a href="#">Clear events</a></li>
                    <li class="divider"></li>
                    <li><a href="#">View calendar</a></li>
                </ul>
            </div>--}}
            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <!--The calendar -->
        {{-- <div id="calendar" style="width: 100%"></div> --}}
        <calendar-md flex layout layout-fill
          calendar-direction="direction"
          on-prev-month="prevMonth"
          on-next-month="nextMonth"
          on-day-click="dayClick"
          title-format="'MMMM y'"
          ng-model='selectedDate'
          week-starts-on="firstDayOfWeek"
          tooltips="tooltips"
          <--Set the initial month here. "8" is September. Defaults to current-->
          data-start-month="8"
          <--Set the initial year here. Defaults to current.-->
          data-start-year="2015"
          tooltips="tooltips"
          day-format="dayFormat"
          day-label-format="'EEE'"
          day-label-tooltip-format="'EEEE'"
          day-tooltip-format="'fullDate'"
          day-content="setDayContent"></calendar-md>
    </div><!-- /.box-body -->
    <div class="box-footer text-black">
        <div class="row">
            <div class="col-sm-6">

            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div><!-- /.box -->
