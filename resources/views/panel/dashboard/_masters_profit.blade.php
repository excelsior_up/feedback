{{-- Основной контейнер --}}
<div class="box box-primary">

    {{-- Шапка контейнера --}}
    <div class="box-header with-border">

        {{-- Текст шапки контейнера --}}
        <h3 class="box-title">

            <span class="myIcons">
                <img src="/img/icons/24.png">
            </span>
            Доход по мастерам
        </h3>

        {{-- Кнопки закрытия и коллапса контейнера --}}
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa



                -times"></i>
            </button>
        </div>

    </div>

    {{-- Основное наполнение контейнера --}}
    <div class="box-body">
        <div class="row">

            <div class="col-md-12">

                @forelse($mastersProfit as $masterProfit)

                    <div class="progress-group">



                        {{-- Сумма прибыли за оказанные мастером услуги --}}
                        <span class="progress-number">
                            {{$masterProfit[0]}}  <i class="tengepng"></i>
                        </span>

                        <?php
                            // Логика изменения цветов и длины полосы
                            $colourArray = ['progress-bar-red', 'progress-bar-yellow', 'progress-bar-green'];
                            if ($mastersProfit[0][0] > 0)
                            {
                                $percentage  = $masterProfit[0] * 100  / $mastersProfit[0][0];
                            }
                            else
                            {
                                $percentage = 0;
                            }
                            if ($percentage <= 33){$selectedColour = $colourArray[0];}
                            if ($percentage >  33){$selectedColour = $colourArray[1];}
                            if ($percentage >  66){$selectedColour = $colourArray[2];}
                        ?>

                        <div class="row">

                            {{-- Контейнер аватара --}}
                            <div class="col-md-2">

                                {{-- Аватар мастера слева от полосы --}}
                                @include('helpers.avatar_extra_small',['avatar'=>$masterProfit[1]->avatar,'caption'=>$masterProfit[1]->name])

                                {{-- Ссылка на профиль мастера --}}
                                <span class="progress-text col-xs-12">
                                    <a class="" href="/panel/masters/profile/{{$masterProfit[1]->id}}">
                                        {{$masterProfit[1]->surname}}
                                        {{$masterProfit[1]->name}}
                                    </a>
                                </span>
                            </div>

                            {{-- Контейнер полосы --}}
                            <div class="col-md-10">
                                <div class="progress sm" style="margin-top: 8%;">

                                    {{-- Передаются цвет и ширина заполненности полосы --}}
                                    <div class="progress-bar {{$selectedColour}}" style="width: {{$percentage}}%"></div>

                                </div>
                            </div>
                        </div>


                    </div>
                @empty
                    <h5 class="text-center">
                        <em>Пока нет мастеров. Это очень легко исправить.</em>
                    </h5>
                    <md-button onclick="window.location = '/panel/masters/add';" onclick="window.location = '/panel/masters/add';" class="col-xs-12 noMargin md-raised md-primary" data-toggle="modal"> Добавить</md-button>
                @endforelse


            </div>

        </div>
    </div>

</div>
