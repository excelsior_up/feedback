{{-- Основной контейнер --}}
<div class="box box-warning">

    {{-- Шапка контейнера --}}
    <div class="box-header with-border">

        {{-- Текст шапки контейнера --}}
        <h3 class="box-title">Популярные услуги</h3>

        {{-- Кнопки закрытия и коллапса контейнера --}}
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>

    </div>

    {{-- Основное наполнение контейнера --}}
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">

                {{-- График (Секторы) --}}
                <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                </div>

                @if( ! count($popularServices))
                    <p class="text-center col-xs-12">
                        Пока не оказано ни одной услуги.<br>
                        Это легко исправить. <md-button class="md-raised md-primary" data-toggle="modal" data-target="#newService">Добавить</md-button>
                    </p>
                @endif
            </div>
        </div>
    </div>

    {{-- Подвал контейнера --}}
    <div class="box-footer no-padding">

        {{-- Легенда графика --}}
        <ul class="nav nav-pills nav-stacked">

            {{-- Код PHP для окрашивания меток в разные цвета --}}
            <?php
                // Названия классов цветов, в которые будут окрашены кружочки
                $labelColours = ['red', 'green', 'yellow', 'aqua', 'light-blue', 'grey'];
                // Счетчик набора цветов
                $counterColour = 0;
            ?>

            {{-- Проход по оказанным услугам, прошедшим фильтр популярности --}}
                @foreach($transactions as $current)
                    <li>
                        <a href="">

                            @if($counterColour <= 5)
                                <?php $selected = $labelColours[$counterColour]; ?>
                            @else
                                <?php $selected = $labelColours[5]; ?>
                            @endif
                            <i class="fa fa-circle-o text-{{$selected}}"></i>
                            <?php $counterColour = $counterColour + 1; ?>

                            {{-- Название услуги --}}
                            {{$current[1]}}

                            {{-- Количесвто оказанных услуг --}}
                            <span class="pull-right">
                                {{$current[0]}}
                            </span>

                        </a>
                    </li>
                @endforeach
        </ul>
    </div>

</div>
