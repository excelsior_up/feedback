<div class="form-group custom-file-upload" flex>
    <img src="{{ isset($client) ? ($client->avatar ? $client->avatar : '/img/user.png') : '/img/user.png' }}" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
    <!--<label for="file">File: </label>-->
    <input type="file" id="image_changer_input" name="avatar" multiple  />
</div>

<md-input-container flex>
    <label for="surname">Фамилия</label>
    {!! Form::text('surname', null, ['class' => 'form-control']) !!}
</md-input-container>

<md-input-container flex>
    <label for="name">Имя</label>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</md-input-container>

<md-input-container flex>
    <label for="email">Email</label>
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</md-input-container>

<md-input-container flex>
    <label for="phone">Телефон</label>
    {!! Form::text('phone', null, ['class' => 'form-control phone-mask']) !!}
</md-input-container>

<md-input-container flex>
    <label for="birthday">Дата рождения</label>
    {!! Form::text('birthday', null, ['class' => 'form-control date-mask']) !!}
</md-input-container>

{{--
@if(! isset($hide_salon))

    {!! Form::input('hidden', 'salon_id', '[[ testing ]]', ['ng-model' => 'testing']) !!}
    <md-input-container class="md-select-wrapper" flex>
        {!! Form::label('salon_id', 'Выберите салон: ') !!}
        <md-select name="salon_ids" id="salon_id" aria-label="bla-lba" ng-model="testing" {{ old('salon_id') ? 'ng-init=testing=' . old('salon_id') . '' : '' }}>
            @foreach($salons as $salon)
                <md-option value="{{ $salon->id }}" aria-label="{{ $salon->id }}">
                    {{ $salon->name }}
                </md-option>
            @endforeach
        </md-select>
    </md-input-container>

@endif
--}}

<div class="box-footer">
    <md-button class="md-raised md-primary-btn pull-right">
        @if(isset($button_caption))
            {{$button_caption}}
        @else
            Добавить
        @endif
    </md-button>
</div>
