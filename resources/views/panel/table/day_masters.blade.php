@extends('panel.layout')

@section('page_title')
    Журнал
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Журнал',
            'dash_caption_small' => ''
        ]
    )
    {{-- Initializing values for modal window . MUST BE REFACTORED LATER!--}}

    <?php $counter = 0; ?>
    @forelse($user->salons->first()->services as $salon_service)
        <div  ng-init="service.services[{{ $counter }}]['id'] = '{{ $salon_service->id }}'" > </div>
        <div  ng-init="service.services[{{ $counter }}]['name'] = '{{ $salon_service->service->name }}'" > </div>
        <?php $counter++; ?>
    @empty
    @endforelse

    <?php $counter = 0; ?>
    @forelse($user->salons->first()->clients as $client)
        <div  ng-init="service.clients[{{ $counter }}]['id'] = '{{ $client->id }}'" > </div>
        <div  ng-init="service.clients[{{ $counter }}]['name'] = '{{ $client->name }}'" > </div>
        <?php $counter++; ?>
    @empty
    @endforelse

    {{-- get all masters  --}}
    <?php $counter = 0; ?>
    @forelse($user->salons->first()->masters as $master)
        <div  ng-init="service.master[{{ $counter }}]['id'] = '{{ $master->id }}'" > </div>
        <div  ng-init="service.master[{{ $counter }}]['name'] = '{{ $master->name }}'" > </div>
        <?php $counter++; ?>
    @empty
    @endforelse

    {{-- <div ng-init="service.start_date = '{{ Carbon\Carbon::now() }}'"></div> --}}
    <div ng-init="service.start_date = '{{ $current_date }}'"></div>


    <style media="screen">
      .table {
        /*border-collapse: separate !important;*/
        /*border-spacing: 10px 0px !important;*/
      }
      .table thead {

      }
      .table tbody {

      }
      .table tbody tr {

      }
      .table tbody td:hover {
        background: rgba(0, 0, 0, 0.05);
      }
      .table > thead > tr > th {
        border: none;
      }
      .table tr th {
          text-align: center;
          border: none;
          vertical-align: middle !important;
      }
      .table tr td {
        text-align: center;
      }
      .table tr td:first-child  {
        border: none;
        width: 50px;
      }
      .table tr td:first-child:hover {
        background: none !important;
      }
      .table tr th:first-child {
        width: 50px;
      }
      .avatar-table-wrap {
        width: 50px;
        margin: 0 auto;
      }
      #table_modal {
        min-width: 50%;
      }
    </style>
    <section class="content" ng-controller="TableController">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
                <h3 class="box-title">
                    Журнал записей
                </h3>
            </div>

            <div class="box-body">
                @if($clientsCount <= 0 || $mastersCount->count() <= 0 )
                    <h6 style="background-color: #2595FF;color: white;padding: 1em;text-align: center;font-weight: bold;">Перед тем, как работать над журналом записей необходимо добавить мастеров и клиентов.</h6>
                    <div class="row">
                      <div class="col-md-6 cleafix  noFloat centerIt">
                         @if($mastersCount <= 0)
                            <md-button class="col-md-5 md-raised md-primary md-button md-default-theme" onclick="window.location = '/panel/masters/add';" >Добавить Мастера</md-button>
                         @endif

                         @if($clientsCount->count() <= 0)
                            <md-button class="col-md-5 md-raised md-primary md-button md-default-theme" onclick="window.location = '/panel/clients/add';">Добавить клиента</md-button>
                         @endif
                      </div>
                    </div>
                @else
                  <div class="row">
                    <div class="clearfix col-xs-12 ">
                      <md-button class="col-md-3 md-raised md-primary md-button md-default-theme col-md-3" >Вчера</md-button>
                      <div class="col-md-5 ">
                          <h2 class="col-xs-12  oswaldStyle text-center noMargin" ng-bind="currentDay">


                         </h2>
                         <br>
                         <small>Сегодня</small></span>

                      </div>

                      <md-button class="col-md-3 md-raised md-primary md-button md-default-theme col-md-3"  ng-click="nextDay()">Завтра</md-button>
                    </div>
                  </div>
                  {{-- Calendar Material  --}}
                    <div class="row">
                        <!--<div class="col-md-3 col-md-push-9">
                            {!! Form::open(['method'=>'POST', 'url'=>'/panel/table/add']) !!}
                                <div class="form-group">
                                    {!! Form::label('service_id', 'Выберите сервис: ') !!}
                                    <select class="form-control" name="service_id" id="service_id" ng-model="select1">
                                        @forelse($user->salons->first()->services as $salon_service)
                                            <option value="{{ $salon_service->id }}">
                                                {{ $salon_service->service->name }}
                                            </option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('client_id', 'Выберите клиента: ') !!}
                                      <select class="form-control" name="client_id" id="client_id" ng-model="select2">
                                        @forelse($user->salons->first()->clients as $client)
                                            <option value="{{ $client->id }}" aria-label="опция">
                                                {{ $client->name }}
                                            </option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('master_id', 'Выберите мастера: ') !!}
                                    <select class="form-control" name="master_id" id="master_id" ng-model="select3">
                                        @forelse($user->salons->first()->masters as $master)
                                            <option value="{{ $master->id }}">
                                                {{ $master->name }}
                                            </option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>

                            {{-- Скрываем до тех пор , пока не будет переключения на след. день --}}
                                <div class="form-group hidden">
                                    {!! Form::label('start_date', 'Дата начала: ') !!}
                                    {!! Form::text('start_date', $current_date, ["class"=>"form-control"])!!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('start_h', 'Время начала: ') !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" name="start_h" id="start_h" ng-model="select4">
                                                @for($hours = 08; $hours < 24; $hours++)
                                                    <option value="{{ $hours }}">
                                                        {{ $hours }}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control" name="start_m" id="start_m" ng-model="select5">
                                                <option value="00">00</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('duration', 'Продолжительность: ') !!}
                                    <select class="form-control" name="duration" id="duration" ng-model="select6">
                                        <option value="15">15 минут</option>
                                        <option value="30">пол часа</option>
                                        <option value="45">45 минут</option>
                                        <option value="60">час</option>
                                        <option value="75">час 15 минут</option>
                                        <option value="90">полтора часа</option>
                                        <option value="105">час 45 минут</option>
                                        <option value="120">2 часа</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    {{-- {!! Form::submit('Зарезервировать', ['class' => 'md-button md-raised']) !!} --}}
                                    <md-button class="md-button md-raised md-primary-btn">Зарезервировать</md-button>
                                </div>

                            {!! Form::close() !!}
                        </div> -->
                        <div class="col-md-5 col-md-push-7">
                          <calendar-md flex layout layout-fill
                            calendar-direction="direction"
                            on-prev-month="prevMonth"
                            on-next-month="nextMonth"
                            on-day-click="dayClick"
                            title-format="'MMMM y'"
                            ng-model='selectedDate'
                            week-starts-on="firstDayOfWeek"
                            tooltips="tooltips"
                            <--Set the initial month here. "8" is September. Defaults to current-->
                            data-start-month="8"
                            <--Set the initial year here. Defaults to current.-->
                            data-start-year="2015"
                            tooltips="tooltips"
                            day-format="dayFormat"
                            day-label-format="'EEE'"
                            day-label-tooltip-format="'EEEE'"
                            day-tooltip-format="'fullDate'"
                            day-content="setDayContent"></calendar-md>
                        </div>
                        <div class="col-md-7 col-md-pull-5">
                            <div class="table-container">
                              <table class="table">
                                  <thead>
                                      <tr>

                                          <th>Время</th>

                                          @if(isset($masters))
                                          @if(count($masters))
                                          @foreach($masters as $master)
                                              <th>
                                                  <div class="avatar-table-wrap">
                                                    <img src="{{$master->avatar}}" class="img-responsive img-circle" alt="avatar" />
                                                  </div>
                                                  {{$master->surname}} <br> {{$master->name}}
                                              </th>
                                          @endforeach
                                          @endif
                                          @endif

                                      </tr>
                                  </thead>
                                  <tbody class="table-striped">
                                      @for($i=1; $i <= 64; $i++)

                                          <?php
                                              $tdNum = (31 + $i) / 4;
                                              $tdHour = floor($tdNum);
                                              switch ($tdHour) {
                                                  case 8:
                                                      $tdHour = '08';
                                                      break;
                                                  case 9:
                                                      $tdHour = '09';
                                                      break;
                                              }
                                              $tdUnfloor = $tdNum - $tdHour;
                                              switch ($tdUnfloor) {
                                                  case 0:
                                                      $tdMinute = '00';
                                                      break;
                                                  case 0.25:
                                                      $tdMinute = '15';
                                                      break;
                                                  case 0.5:
                                                      $tdMinute = '30';
                                                      break;
                                                  case 0.75:
                                                      $tdMinute = '45';
                                                      break;
                                                  default:
                                                      $tdMinute = '00';
                                                      break;
                                              }
                                          ?>

                                          <tr id="{{$tdHour}}:{{$tdMinute}}" >
                                              <td>
                                                  {{$tdHour}}:{{$tdMinute}}
                                              </td>
                                              @foreach($mastersCount as $masterTd)
                                                  <td id="{{$tdHour}}:{{$tdMinute}}-{{$masterTd}}" ng-click="createFromStart({{$tdHour}}, {{$tdMinute}}, {{$masterTd}}, $event);">
                                                    <md-tooltip ng-bind="tooltip_text" class="tooltip_table" md-auto-hide></md-tooltip>
                                                    {{-- @if(count($masterReservations))
                                                    @foreach($masterReservations as $reservation)
                                                      <a style='color:white;display: none;' href='{{url("/panel/clients/profile/".$reservation->client_id)}}'>{{$reservation->client->name}}</a>
                                                    @endforeach --}}
                                                  </td>
                                              @endforeach

                                          </tr>

                                      @endfor
                                  </tbody>

                              </table>
                            </div>
                          </div>
                    </div>
                 @endif
            </div>

        </div>

    </section>

    <script>
        var colours = ['rgba(255, 0, 86, 0.6)', 'green', 'orange', 'blue', 'red', 'green', 'orange', 'blue', 'red', 'green', 'orange', 'blue'];
        var createFromStart = function(aHour, aMinute, aMasterId, $event) {

          // document.getElementById('master_id').value = aMasterId;
          // document.getElementById('start_h').value = aHour;
          // if (aMinute === 0) {
          //   document.getElementById('start_m').value = '00';
          // } else {
          //   document.getElementById('start_m').value = aMinute;
          // }
          // document.getElementById('start_date').value = '{{ Carbon\Carbon::now()->format("Y-m-d") }}';

        };
    </script>

    @if(isset($masters))
    @if(count($masters))
    <script>
        var colourIndex = 0;
    </script>
    @foreach($masters as $master)
        <script>
            var backColor = colours[colourIndex];
            colourIndex += 1;
        </script>
        <?php
            $masterReservations = App\Reservation::where('master_id', '=', $master->id)
                ->where('start', '>', Carbon\Carbon::now()->startOfDay())
                ->where('start', '<', Carbon\Carbon::now()->endOfDay())
                ->get();
        ?>
        @if(count($masterReservations))
        @foreach($masterReservations as $reservation)
        <?php
            $start  = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->start);
            $hour   = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->start)->format('H');
            $minute = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->start)->format('i');

            $end       = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->end);
            $hourEnd   = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->end)->format('H');
            $minuteEnd = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->end)->format('i');

            $id = $master->id;
        ?>
            <script>

                document.getElementById("{{$hour}}:{{$minute}}-{{$id}}").style.backgroundColor = backColor;
                document.getElementById("{{$hour}}:{{$minute}}-{{$id}}").style.boxShadow = "0px 2px 5px rgba(0, 0, 0, 0.3)";
                document.getElementById("{{$hour}}:{{$minute}}-{{$id}}").style.border = "1px solid rgba(130, 100, 100, 0.1)";
              //  document.getElementById("{{$hour}}:{{$minute}}-{{$id}}").innerHTML = "<a style='color:white' href='{{url("/panel/clients/profile/".$reservation->client_id)}}'>{{$reservation->client->name}}</a>";-->
                document.getElementById("{{$hour}}:{{$minute}}-{{$id}}").removeEventListener('click', createFromStart, false);
                @while($start < $end)
                    <?php
                        $start->addMinutes(15);
                        if($start >= $end){
                            break;
                        }
                    ?>
                    document.getElementById("{{$start->format('H:i')}}-{{$id}}").style.backgroundColor = backColor;
                    document.getElementById("{{$start->format('H:i')}}-{{$id}}").style.border = "none";
                    document.getElementById("{{$start->format('H:i')}}-{{$id}}").removeEventListener('click', createFromStart, false);
                @endwhile
            </script>
        @endforeach
        @endif
    @endforeach
    @endif
    @endif

@stop
