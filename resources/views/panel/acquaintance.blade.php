<!DOCTYPE html>
<html ng-app="Application">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('page_title', 'Добавление салона') | FeedBack</title>

    <!-- Стили -->
    <link rel="stylesheet" href="/css/app.css" charset="utf-8">

    <link rel="shortcut icon" href="/favicon.ico">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

</head>

<body class="hold-transition skin-blue sidebar-mini">
@if(app()->environment() == 'production')
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-49907441-11', 'auto');
        ga('send', 'pageview');

    </script>
@endif

<div class="wrapper">

    <!-- Подключение шапки сайта -->
    @include('panel.partials.header-ac')


            <!-- Содержимое страницы -->
    <div class="noMargin clearfix content-wrapper congrats-bg" style="padding-top: 5em !important;">

        @yield('content')

    </div>

    <!--   Floating action button -->
    @if($user->salons->count())
        @if($user->salons->first()->isApproved)
            <nav mfb-menu position="br" effect="slidein-spring"
                 label="Добавить"
                 active-icon="ion-close" resting-icon="ion-plus-round"
                 toggling-method="click">

                <a data-toggle="modal" data-target="#newService" mfb-button icon="ion-social-usd"
                   label="Оказанная услуга"></a>
                <a onclick="window.location = '/panel/clients/add';" mfb-button icon="ion-person-add"
                   label="Добавить клиента"></a>
                <a onclick="window.location = '/panel/masters/add';" mfb-button icon="ion-scissors" label="Добавить мастера"></a>
                {{--<a onclick="window.location = '/panel/salons/add';" mfb-button icon="ion-paintbrush" label="Салон"></a>--}}
            </nav>
            @endif
            @endif
                    <!--   Floating action button -->

            <!-- Подвал сайта -->
            @include('panel.partials.footer-ac')

                    <!-- Control Sidebar -->
            {{--@include('panel.partials.right_menu')--}}

</div>
<!-- ./wrapper -->

<!-- Подключение скрипта -->
<script src="/js/all.js"></script>


<!-- Модальные окна, которые нужны на каждой странице -->
@if(Auth::director()->check())
@if($user->salons->count())
@include('panel.partials.modals.transaction')
@endif
@else
@if($user->salon != null)
        <!-- @include('panel.partials.modals.transaction') -->
@endif
@endif
        <!-- Модальные окна, которые нужны на каждой странице -->

<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

@if(app()->environment() == 'production')
    {{--<!-- BEGIN JIVOSITE CODE {literal} -->--}}
    {{--<script type='text/javascript'>--}}
    {{--(function(){ var widget_id = 'nujvaymsS3';--}}
    {{--var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>--}}
    {{--<!-- {/literal} END JIVOSITE CODE -->--}}
@endif

</body>
</html>
