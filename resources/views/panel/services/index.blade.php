@extends('panel.layout')

@section('page_title')
    Услуги салона
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Услуги',
            'dash_caption_small' => 'Добавление и редактирование услуг'
        ]
    )

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <div class="box-title">
                                Выберите услуги салона
                            </div>
                        </div>
                    </div>

                    <div>
                        @include('flash::message')
                    </div>

                    <hr>

                    <div class="box-body" ng-controller="serviceController">
                        <div class="col-md-offset-1 col-md-10">
                            {{--{!! Form::open(['url' => ['panel/salons/add-services', $salon_id], 'class' => 'form-horizontal', 'files' => true]) !!}--}}
                            {!! Form::open(['class' => 'form-horizontal']) !!}
                            <div>
                                <!-- Angular tab -->
                                <md-tabs md-dynamic-height>
                                  {{-- @foreach($categories as $category) --}}
                                    <md-tab ng-repeat="serviceGroup in serviceList" label="[[ serviceGroup.category_name ]]">
                                      <h2 class="text-center">[[ serviceGroup.category_name]]</h2>
                                      {{-- @foreach($category->subcategories as $subcategory) --}}
                                          <div ng-repeat="subservice in serviceGroup.subCategories" class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                              <md-card>
                                                  <md-toolbar class="checkbox_toolbar">
                                                    [[ subservice.subCategory_name ]]
                                                  </md-toolbar>
                                                  <md-card-content>
                                                      <div class="md-padding">

                                                      {{-- @foreach($subcategory->services as $service) --}}
                                                          {{-- <div class="col-md-4">
                                                              {!! Form::checkbox('service_ids[]', $service->id, in_array($service->id, $salon_services) ? true : false, ['id' => 'service-' . $service->id]) !!}
                                                              {!! Form::label('service-' . $service->id, $service->name, ['class' => 'control-label']) !!}
                                                          </div> --}}
                                                          <div flex="50" ng-repeat="lastService in subservice.services">
                                                            <md-checkbox class="md-primary"  ng-model="lastService.id" aria-label="[[ lastService.name ]]">
                                                              [[ lastService.name ]]
                                                            </md-checkbox>
                                                          </div>

                                                      {{-- @endforeach --}}
                                                      </div>
                                                  </md-card-content>
                                              </md-card>
                                          </div>
                                      {{-- @endforeach --}}
                                    </md-tab>
                                  {{-- @endforeach --}}
                                </md-tabs>
                            </div>

                            <div class="form-group text-center">
                                <div class="col-md-offset-1 col-sm-10">
                                    <md-button flex type="submit" class="btn btn-lg btn-clock block-btn md-raised md-primary-btn">
                                        Все услуги выбраны
                                    </md-button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
