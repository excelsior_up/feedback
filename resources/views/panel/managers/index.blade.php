@extends('panel.layout')

@section('page_title')
    Список менеджеров
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Менеджеры',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
                <h3 class="box-title">
                    Список всех менеджеров
                </h3>
            </div>

            <div class="box-body">
                <table id="clients-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Аватар</th>
                        <th>Email</th>
                        {{--<th>Телефон</th>--}}
                        {{-- <th>День рождения</th> --}}
                        <th>Салон</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salons as $salon)
                        @foreach($salon->managers as $manager)
                            <tr>
                                <td>
                                    {{-- Ссылка на редактирование --}}
                                    <a href="/panel/managers/edit/{{ $manager->id }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    &nbsp;
                                    {{$manager->surname}}
                                </td>
                                <td>{{$manager->name }}</td>
                                <td>
                                    {{-- Avatar helper --}}
                                    @include('helpers.avatar',['avatar'=>$manager->avatar,'caption'=>$manager->name])
                                </td>
                                <td>{{ $manager->email }}</td>
                                {{--<td>{{ $manager->phone }}</td>--}}
                                {{-- <td>{{ $master->birthday }}</td> --}}
                                <td>{{ $manager->salon->name }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                    {{--<tfoot>
                        <tr>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>День рождения</th>
                            <th>Салон</th>
                        </tr>
                    </tfoot>--}}
                </table>
            </div>

        </div>

    </section>

@stop
