@extends('panel.layout')

@section('page_title')
    Добавить менеджера
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Менеджеры',
            'dash_caption_small' => 'Добавить менеджера'
        ]
    )

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <div class="box-title">
                                Добавить менеджера
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="box-body">
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::open(['class' => 'form-horizontal', 'files'=>true]) !!}
                                @include('panel.managers._form', ['button_text' => 'Добавить'])
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
