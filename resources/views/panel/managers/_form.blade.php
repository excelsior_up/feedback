@include('flash::message')

<div class="form-group custom-file-upload" flex>
    <img src="/img/upload-area.jpg" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
    <!--<label for="file">File: </label>-->
    <input type="file" id="image_changer_input" name="avatar" multiple  />
</div>

<md-input-container flex>
    <label for="surname">Фамилия</label>
    {!! Form::text('surname', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="name">Имя</label>
    {!! Form::text('name', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="email">Email</label>
    {!! Form::email('email', null, ['required']) !!}
</md-input-container>

@if(! isset($hide_salon))
    {!! Form::input('hidden', 'salon_id', '[[ testing ]]', ['ng-model' => 'testing']) !!}
    <md-input-container>
        {!! Form::label('salon_id', 'Выберите салон: ') !!}
        <md-select name="salon_ids" id="salon_id" ng-model="testing" {{ old('salon_id') ? 'ng-init=testing=' . old('salon_id') . '' : '' }}>
            @foreach($salons as $salon)
                <md-option value="{{ $salon->id }}" aria-label="{{ $salon->id }}">
                    {{ $salon->name }}
                </md-option>
            @endforeach
        </md-select>
    </md-input-container>
@endif


@if(! isset($hide_password))
    <md-input-container flex>
        <label for="password">
            Пароль
        </label>
        {!! Form::text('password_first', null, ['required', 'id' => 'password-field']) !!}
    </md-input-container>

    <div class="btn btn-primary" id="generate-password-button">
        Сгенерировать пароль?
    </div>

    <md-input-container flex>
        <label for="password_confirmation">
            Повторите пароль
        </label>
        {!! Form::text('password_confirmation', null, ['required']) !!}
    </md-input-container>
@endif

<div class="box-footer">
    <md-button class="md-raised md-primary-btn pull-right">
        @if(isset($button_caption))
            {{$button_caption}}
        @else
            Добавить
        @endif
    </md-button>
</div>
