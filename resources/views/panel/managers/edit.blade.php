@extends('panel.layout')

@section('page_title')
    Редактировать данные менеджера
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Менеджеры',
            'dash_caption_small' => 'Редактировать менеджера'
        ]
    )

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <div class="box-title">
                                Редактировать менеджера
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="box-body">
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::model($manager, ['class' => 'form-horizontal', 'files' => true]) !!}
                                @include('panel.managers._form', ['button_caption' => 'Сохранить', 'hide_salon' => true])
                            {!! Form::close()!!}
                        </div>
                        <div class="col-md-offset-1 col-md-9">
                            {!! Form::open(['url' => '/panel/managers/delete/'. $manager->id])!!}
                                {!! Form::submit('Удалить?',['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop
