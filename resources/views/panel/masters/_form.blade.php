@include('flash::message')

<div class="form-group custom-file-upload" flex>
    <img src="{{ isset($master) ? ($master->avatar ? $master->avatar : '/img/user.png') : '/img/user.png' }}" id="prev_image" class="preview_image" alt="file_preview" style="max-width: 500px;">
    <input type="file" id="image_changer_input" name="avatar" accept="image/*"  />
</div>

<md-input-container flex>
    <label for="surname">Фамилия</label>
    {!! Form::text('surname', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="name">Имя</label>
    {!! Form::text('name', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="email">Email</label>
    {!! Form::email('email', null, ['required']) !!}
</md-input-container>

<md-input-container flex>
    <label for="phone">Телефон</label>
    {!! Form::text('phone', null, ['required', 'class' => 'phone-mask']) !!}
</md-input-container>

{{--@if(! isset($hide_salon))
    <md-input-container  class="md-select-wrapper" flex>
        <label>Салон</label>
        --}}{{-- <md-select ng-model="ctrl.userState" name="salon_id" id="salon_id"> --}}{{--

        <select name="salon_id" id="salon_id" class="form-control" onchange="masterSalonChanged(this)">

            @foreach($salons as $salon)
                <option value="{{ $salon->id }}" aria-label="{{ $salon->id }}">
                    {{ $salon->name }}
                </option>
            @endforeach
        </select>
    </md-input-container>
@endif--}}

<fieldset>
    <div id="master-services">

        @if(isset($master))
            @foreach($salons->first()->services as $salon_service)
                <div class="col-md-4">
                    {!! Form::checkbox('master_services[]', $salon_service->id, false, ['id' => 'service-' . $salon_service->id,
                        $master->services->where('service_id', $salon_service->id)->count() ? 'checked' : ''
                    ]) !!}
                    {!! Form::label('service-' . $salon_service->id, $salon_service->service->name) !!}
                </div>
            @endforeach
        @else
            @foreach($salons->first()->services as $salon_service)
                <div class="col-md-4">
                        {!! Form::checkbox('master_services[]', $salon_service->id, false, ['id' => 'service-' . $salon_service->id]) !!}
                        {!! Form::label('service-' . $salon_service->id, $salon_service->service->name) !!}
                </div>
            @endforeach
        @endif


    </div>
</fieldset>

<div class="box-footer">
    <md-button class="md-raised md-primary-btn pull-right">
        @if(isset($button_caption))
            {{$button_caption}}
        @else
            Добавить
        @endif
    </md-button>
</div>

<script>
    function masterSalonChanged(salon)
    {
        var services_content = '';

        @foreach($salons as $salon)
            if ( {{ $salon->id }} == $(salon).val() )
            {
                @forelse($salon->services as $salon_service)
                    services_content += '<div class="col-md-4">' +
                        '{!! Form::checkbox("master_services[]", $salon_service->id, false, ["id" => "service-" . $salon_service->id]) !!}' +
                        '{!! Form::label("service-" . $salon_service->id, $salon_service->service->name) !!}' +
                    '</div>';
                @empty
                @endforelse
            }
        @endforeach

        $("#master-services").html(services_content);
    }
</script>
