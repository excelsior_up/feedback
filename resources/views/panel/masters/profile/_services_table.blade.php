<table id="timelinedata" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
    <thead>
      <tr role="row">
          <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 205px;">
              Название услуги
          </th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 177px;">
              Клиент
          </th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 253px;">
              День
          </th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 223px;">
              Цена
          </th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 223px;">
              Оценка
          </th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 223px;">
              Комментарий
          </th>
      </tr>
    </thead>
    <tbody>

        @foreach($transactions as $transaction)
            <tr role="row" class="odd">
                <td class="sorting_1">
                    {{-- Услуга --}}
                    {{$transaction->service->name}}
                </td>
                <td>
                    {{-- Клиент и ссылка на его профиль --}}
                    <a href="/panel/clients/profile/{{$transaction->client->id}}">
                        {{$transaction->client->surname}}
                        {{$transaction->client->name}}
                    </a>
                </td>
                <td>{{$transaction->created_at}}</td>
                <td>{{$transaction->price}}</td>
                <td>{{$transaction->rate}}</td>
                <td>{{$transaction->comment}}</td>
            </tr>
        @endforeach

    </tbody>
    <tfoot>
      {{-- <tr>
        <th rowspan="1" colspan="1">Название услуги</th>
        <th rowspan="1" colspan="1">День</th>
        <th rowspan="1" colspan="1">Цена</th>
        <th rowspan="1" colspan="1">Мастер</th>
      </tr> --}}
    </tfoot>
</table>
