@extends('panel.layout')

@section('page_title')
    Мастер {{ $master->name }} {{ $master->surname }}
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Профиль мастера',
            'dash_caption_small' => ''
        ]
    )

    <section class="content" ng-controller="MainController">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">

                        <div class="text-center">
                            <a href="#">
                                {{-- Avatar helper --}}
                                @include('helpers.avatar', ['avatar' => $master->avatar ? $master->avatar : NULL, 'caption' => $master->name, 'size'=>150])
                            </a>
                        </div>

                        <h3 class="profile-username text-center">
                            {{ $master->name ." " . $master->surname }}
                        </h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Кол-во услуг</b> <a class="pull-right">{{ $profileInfo->services }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Объем дохода</b> <a class="pull-right">{{ $profileInfo->amount }} тг.</a>
                            </li>
                            {{--<li class="list-group-item">
                                <b>Любимая услуга</b> <a class="pull-right">{{ $profileInfo->favoriteService }}</a>
                            </li>--}}
                        </ul>

                        <a href="#" class="btn btn-primary btn-block"
                           ng-click="showAdvanced($event)"><b>Написать</b></a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Информация</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong>
                          <span class="myIcons">
                                  <img src="/img/icons/18.png">
                          </span>

                            Ф.И.О
                        </strong>

                        <p class="text-muted">
                            {{ $master->surname . ' ' . $master->name }}
                        </p>

                        <hr>

                        <strong>
                          <span class="myIcons">
                                  <img src="/img/icons/19.png">
                          </span>
                            Почта
                        </strong>

                        <p class="text-muted">
                            {{ $master->email }}
                        </p>

                        <hr>
                        <strong>
                          <span class="myIcons">
                                  <img src="/img/icons/20.png">
                          </span>
                            День рождения
                        </strong>

                        <p class="text-muted">
                            {{ $master->birthday }}
                        </p>

                        <hr>

                        <strong>
                          <span class="myIcons">
                                  <img src="/img/icons/21.png">
                          </span>
                           Номер телефона</strong>

                        <p>

                        <p class="text-muted">{{ $master->phone }}</p>
                        </p>

                        {{--<hr>

                        <strong><i class="fa fa-mobile margin-r-5"></i> Значки</strong>

                        <p>
                            <span class="label label-danger">бронза</span>
                            <span class="label label-success">Серебро</span>
                            <span class="label label-info">Золота</span>
                            <span class="label label-warning">Платина</span>
                            <span class="label label-primary">Алмаз</span>
                        </p>

                        <hr>

                        <strong><i class="fa fa-file-text-o margin-r-5"></i>
                            Комменты
                        </strong>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.
                        </p>--}}

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box timeline_tab">
                  <md-tabs md-dynamic-height md-border-bottom>
                    <md-tab label="Таймлайн" class="md-padding">
                      @include('panel.masters.profile._timeline')
                    </md-tab>
                    <md-tab label="Таблица" class="md-padding">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Таблица услуг</h3>
                                    </div>
                                    <div class="box-body">
                                        <div id="example1_wrapper"
                                             class="dataTables_wrapper form-inline dt-bootstrap">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    @include('panel.masters.profile._services_table')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </md-tab>
                    <md-tab label="Настройка" class="md-padding">
                        @include('panel.masters.profile._settings')
                    </md-tab>
                  </md-tabs>
                </div>
            </div>
        </div>
    </section>

@stop
