@if(isset($salons))
    @if($salons->count())
        {{-- Основная форма редактирования --}}
        {!! Form::model($master, ['method' => 'post', 'url' => '/panel/masters/edit/'.$master->id, 'class' => 'form-horizontal', 'files' => true]) !!}
            @include('panel.masters._form', ['button_caption' => 'Сохранить', 'hide_salon' => true])
        {!! Form::close()!!}

        {{-- Кнопка удаления клиента --}}
        {!! Form::open(['url' => '/panel/masters/delete/'. $master->id])!!}
            {!! Form::submit('Удалить?',['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    @endif
@endif
