<ul class="timeline timeline-inverse">

    @foreach($transactions as $transaction)

        {{-- Метка даты --}}
        <li class="time-label">
            <span class="timeLineDate">
                {{$transaction->created_at->format('d-m-Y')}}
            </span>
        </li>

        {{-- Основной контейнер --}}
        <li>
            {{-- Иконка --}}
            <?php $randomIcon = rand(0,5);?>
            @if($randomIcon == 1) <i class="fa fa-scissors bg-blue"></i> @endif
            @if($randomIcon == 2) <i class="fa fa-hand-peace-o bg-blue"></i> @endif
            @if($randomIcon == 3) <i class="fa fa-hand-scissors-o bg-blue"></i> @endif
            @if($randomIcon == 4) <i class="fa fa-heart bg-blue"></i> @endif


              <md-card  class="timeline-item">
                  {{-- Метка времени --}}
                  <span class="time">
                      <i class="fa fa-clock-o"></i>
                      {{$transaction->created_at->format('H:i')}}
                  </span>

                  <h3 class="timeline-header">
                      <a href="#">
                          {{-- Услуга --}}
                          {{$transaction->service->name}}
                      </a>
                  </h3>

                  <div class="timeline-body">
                      <span class="raspberryColor stars">
                          @for($i=1; $i <= $transaction->rate; $i++)
                              <i class="fa fa-star"></i>
                          @endfor
                          @if(5 - $transaction->rate)
                              @foreach(range(1, 5 - $transaction->rate) as $key => $value)
                                  <i class="fa fa-star-o"></i>
                              @endforeach
                          @endif
                      </span>
                      <br>
                      {{$transaction->comment}}
                  </div>

                  <div class="timeline-footer">
                      {{-- Клиент и ссылка на его профиль --}}
                      Клиент:
                      <a href="/panel/clients/profile/{{$transaction->client->id}}">
                          {{$transaction->client->surname}}
                          {{$transaction->client->name}}
                      </a>
                  </div>
              </md-card>
        </li>

        {{-- <tr role="row" class="odd">


            <td>{{$transaction->created_at}}</td>
            <td>{{$transaction->price}}</td>
            <td>{{$transaction->rate}}</td>
            <td>{{$transaction->comment}}</td>
        </tr> --}}
    @endforeach

    <li>
        <i class="fa fa-clock-o bg-gray"></i>
    </li>
</ul>
