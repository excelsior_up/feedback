@extends('panel.layout')

@section('page_title')
    Список мастеров
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Мастера',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
                <h3 class="box-title">
                    Список всех мастеров
                </h3>
            </div>

            <div class="box-body">
                <table id="clients-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Аватар</th>
                        <th>Профиль</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Email</th>
                        <th>Телефон</th>
                        {{--<th>Салон</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salons as $salon)
                        @foreach($salon->masters as $master)
                            <tr>
                                <td class="text-center">
                                    {{-- Avatar helper --}}
                                    @include('helpers.avatar_small',['avatar' => $master->avatar ? $master->avatar : null, 'caption' => $master->name])
                                </td>
                                <td>
                                    {{-- Ссылка на профиль --}}
                                    <a style="font-size:30px;" href="/panel/masters/profile/{{$master->id}}">
                                        &nbsp;
                                        <i class="fa fa-list-alt"></i>
                                        &nbsp;
                                    </a>
                                </td>
                                <td>{{ $master->surname}}</td>
                                <td>{{ $master->name }}</td>
                                <td>{{ $master->email }}</td>
                                <td>{{ $master->phone }}</td>
                                {{--<td>{{ $master->salon->name }}</td>--}}
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </section>

@stop
