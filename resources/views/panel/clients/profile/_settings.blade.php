@if(isset($salons))
    @if($salons->count())
        {{-- Основная форма редактирования --}}
        {!! Form::model($client, ['method' => 'post', 'url' => '/panel/clients/edit/'.$client->id, 'class' => 'form-horizontal', 'files' => true]) !!}
            @include('panel.clients._form', ['button_caption' => 'Сохранить', 'hide_salon' => true])
        {!! Form::close()!!}

        <div class="pull-left">
          {{-- Кнопка удаления клиента --}}
          {!! Form::open(['url' => '/panel/clients/delete/'. $client->id])!!}
              {!! Form::submit('Удалить?',['class' => 'btn btn-danger']) !!}
          {!! Form::close() !!}
        </div>
        <div style="clear:both;"></div>

    @endif
@endif
