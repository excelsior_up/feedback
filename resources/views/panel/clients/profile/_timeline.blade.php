<ul class="timeline timeline-inverse">

    @foreach($transactions as $transaction)

        {{-- Метка даты --}}
        <li class="time-label">
            <span class="timeLineDate">
                {{$transaction->created_at->format('d-m-Y')}}
            </span>
        </li>

        {{-- Основной контейнер --}}
        <li>
            {{-- Иконка --}}
            <?php $randomIcon = rand(0,5);?>
            @if($randomIcon == 1) <i class="fa fa-scissors bg-blue"></i> @endif
            @if($randomIcon == 2) <i class="fa fa-hand-peace-o bg-blue"></i> @endif
            @if($randomIcon == 3) <i class="fa fa-hand-scissors-o bg-blue"></i> @endif
            @if($randomIcon == 4) <i class="fa fa-heart bg-blue"></i> @endif

            <md-card class="timeline-item">

                {{-- Метка времени --}}
                <span class="time">
                    <i class="fa fa-clock-o"></i>
                    {{$transaction->created_at->format('H:i')}}
                </span>

                <h3 class="timeline-header">
                    <a href="#">
                        {{-- Услуга --}}
                        {{$transaction->service->name}}
                    </a>
                </h3>

                <div class="timeline-body">
                    <span class="raspberryColor stars">
                        @for($i=1; $i <= $transaction->rate; $i++)
                            <i class="fa fa-star"></i>
                        @endfor
                        @if(5 - $transaction->rate)
                            @foreach(range(1, 5 - $transaction->rate) as $key => $value)
                                <i class="fa fa-star-o"></i>
                            @endforeach
                        @endif
                    </span>
                    <br>
                    {{$transaction->comment}}
                </div>

                <div class="timeline-footer">
                    {{-- Мастер и ссылка на его профиль --}}
                    Мастер:
                    <a href="/panel/masters/profile/{{$transaction->master->id}}">
                        {{$transaction->master->surname}}
                        {{$transaction->master->name}}
                    </a>
                </div>
            </md-card>
        </li>

        {{-- <tr role="row" class="odd">


            <td>{{$transaction->created_at}}</td>
            <td>{{$transaction->price}}</td>
            <td>{{$transaction->rate}}</td>
            <td>{{$transaction->comment}}</td>
        </tr> --}}
    @endforeach
{{--
  <!-- timeline time label -->
  <li class="time-label">
    <span class="bg-red">
      10 Фев. 2014
    </span>
  </li>
  <!-- /.timeline-label -->
  <!-- timeline item -->
  <li>
    <i class="fa fa-envelope bg-blue"></i>
    <div class="timeline-item">
      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
      <h3 class="timeline-header"><a href="#">Стрижка</a> {{ $client->name }}</h3>
      <div class="timeline-body">
        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
        weebly ning heekya handango imeem plugg dopplr jibjab, movity
        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
        quora plaxo ideeli hulu weebly balihoo...
      </div>
      <div class="timeline-footer">
        <a class="btn btn-primary btn-xs">Подробное</a>
        <a class="btn btn-danger btn-xs">Мастер Дариус</a>
      </div>
    </div>
  </li>
  <!-- END timeline item -->

  <!-- timeline item -->
  <li>
    <i class="fa fa-user bg-aqua"></i>
    <div class="timeline-item">
      <span class="time"><i class="fa fa-clock-o"></i> 5 минут назад</span>
      <h3 class="timeline-header no-border"><a href="#">{{ $client->name }}</a> сделала себе педикюр Мастер</h3>
    </div>
  </li>
  <!-- END timeline item -->
  <!-- timeline item -->
  <li>
    <i class="fa fa-comments bg-yellow"></i>
    <div class="timeline-item">
      <span class="time"><i class="fa fa-clock-o"></i> 27 минут назад</span>
      <h3 class="timeline-header"><a href="#">{{ $client->name }}</a> сделала себе что то</h3>
      <div class="timeline-body">
        Накрасило волосы
        Макияж
        Массаж лица
      </div>
      <div class="timeline-footer">
        <a class="btn btn-warning btn-flat btn-xs">Подробное</a>
      </div>
    </div>
  </li>
  <!-- END timeline item -->
  <!-- timeline time label -->
  <li class="time-label">
    <span class="bg-green">
      3 Янв. 2014
    </span>
  </li>
  <!-- /.timeline-label -->
  <!-- timeline item -->
  <li>
    <i class="fa fa-camera bg-purple"></i>
    <div class="timeline-item">
      <span class="time"><i class="fa fa-clock-o"></i> 2 дня назад</span>
      <h3 class="timeline-header"><a href="#">{{ $client->name }}</a> сделала депилацию</h3>
      <div class="timeline-body">
        <img src="http://placehold.it/150x100" alt="..." class="margin">
        <img src="http://placehold.it/150x100" alt="..." class="margin">
        <img src="http://placehold.it/150x100" alt="..." class="margin">
        <img src="http://placehold.it/150x100" alt="..." class="margin">
      </div>
    </div>
  </li>
  <!-- END timeline item -->
  --}}
  <li>
    <i class="fa fa-clock-o bg-gray"></i>
  </li>
</ul>
