@extends('panel.layout')

@section('page_title')
    Редактировать данные клиента
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Клиенты',
            'dash_caption_small' => 'Редактирование'
        ]
    )

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>

                    <!-- Сообщение об ошибке -->
                    @include('flash::message')

                    <div class="box-body">
                        @if(isset($salons))
                            @if($salons->count())
                                <div class="col-md-offset-1 col-md-9">
                                    {!! Form::model($client, ['class' => 'form-horizontal', 'files' => true]) !!}
                                        @include('panel.clients._form', ['button_caption' => 'Сохранить', 'hide_salon' => true])
                                    {!! Form::close()!!}
                                </div>
                                <div class="col-md-offset-1 col-md-9">
                                    {!! Form::open(['url' => '/panel/clients/delete/'. $client->id])!!}
                                        {!! Form::submit('Удалить?',['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
