@extends('panel.layout')

@section('page_title')
    Добавить клиента
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Клиенты',
            'dash_caption_small' => 'Добавление'
        ]
    )

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>

                    @include('flash::message')

                    <div class="box-body">
                        @if(isset($salons))
                            @if($salons->count())
                                <div class="col-md-offset-1 col-md-9">
                                    {!! Form::open(['class' => "form-horizontal", 'files' => true]) !!}
                                        @include('panel.clients._form', ['button_caption' => 'Добавить'])
                                    {!! Form::close()!!}
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
