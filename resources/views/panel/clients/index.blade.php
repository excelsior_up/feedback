@extends('panel.layout')

@section('page_title')
    Список клиентов
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Клиенты',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
              <h3 class="box-title">
                  Список всех клиентов
              </h3>
            </div>

            <div class="box-body">
                <table id="clients-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Аватар</th>
                            <th>Профиль</th>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>День рождения</th>
                            {{--<th>Салон</th>--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($salons as $salon)
                            @foreach($salon->clients as $client)
                                <tr>
                                    <td>
                                        {{-- Avatar helper --}}
                                        @include('helpers.avatar_small',['avatar' => $client->avatar ? $client->avatar : null, 'caption' => $client->name])
                                    </td>
                                    <td>
                                        {{-- Ссылка на профиль --}}
                                        <a style="font-size:30px;" href="/panel/clients/profile/{{$client->id}}">
                                            &nbsp;
                                            <i class="fa fa-list-alt"></i>
                                            &nbsp;
                                        </a>
                                    </td>
                                    <td>{{$client->surname}}</td>
                                    <td>{{$client->name}}</td>
                                    <td>{{$client->email}}</td>
                                    <td>{{$client->phone}}</td>
                                    <td>{{ date('d.m.Y', strtotime($client->birthday)) }}</td>
                                    {{--<td>{{$client->salon->name}}</td>--}}
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                    {{--<tfoot>
                        <tr>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>День рождения</th>
                            <th>Салон</th>
                        </tr>
                    </tfoot>--}}
                </table>
            </div>

        </div>

    </section>

@stop
