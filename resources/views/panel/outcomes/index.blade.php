@extends('panel.layout')

@section('page_title')
    Затраты салона
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => 'Затраты',
            'dash_caption_small' => ''
        ]
    )

    <section class="content">

        <!-- Сообщение о добавлении -->
        @include('flash::message')

        <div class="box">

            <div class="box-header">
                <h3 class="box-title">
                    Список всех затрат
                </h3>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <th>Предмет затраты</th>
                        <th>Категория</th>
                        <th>Количество</th>
                        <th>Цена</th>
                    </thead>
                    <tbody>
                        <tr>
                            {!! Form::open(['method' => 'POST', 'action' => 'OutcomesController@postAdd']) !!}
                                <td>
                                    {!! Form::text('name', null, ['class'=>'form-control', 'required', 'placeholder' => 'Название услуги/товара']) !!}
                                </td>
                                <td>
                                    {!! Form::text('category', null, ['class'=>'form-control', 'placeholder' => 'Товар / Услуга']) !!}
                                </td>
                                <td>
                                    {!! Form::text('quantity', null, ['class'=>'form-control', 'required', 'placeholder' => 1]) !!}
                                </td>
                                <td>
                                    {!! Form::text('price', null, ['class'=>'form-control', 'required', 'placeholder' => '1000']) !!}
                                </td>
                                <td>
                                    {!! Form::submit('Добавить', ['class' => 'btn btn-success']) !!}
                                </td>
                            {!! Form::close() !!}
                        </tr>
                    </tbody>
                </table>
                <br>
                <table id="clients-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Предмет затраты</th>
                        <th>Категория</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Общая стоимость</th>
                        <th>Дата добавления</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach($outcomes as $outcome)
                            <tr>
                                <td>{{$outcome->name}}</td>
                                <td>{{$outcome->category}}</td>
                                <td>{{$outcome->quantity}}</td>
                                <td>{{$outcome->price}}</td>
                                <td>{{$outcome->sum}}</td>
                                <td>{{$outcome->created_at->format('Y-m-d')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </section>

@stop
