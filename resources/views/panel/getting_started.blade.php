@extends('panel.acquaintance')

@section('page_title')
    Начало работы
@stop

@section('content')

    @include('panel.partials.dashboard_navigator-aq',
        [
            'dash_caption' => 'Начальная страница',
            'dash_caption_small' => ''
        ]
    )
        <canvas id="confeti" class="active"></canvas>
        <div class="col-xs-12 noMargin congrats-wrap" layout="row" layout-sm="column" layout-align="center center">
            <md-card class="col-md-7 md-default-theme">

            <!-- Сообщение о добавлении -->
            @include('flash::message')

            <div class="box"  layout="column" layout-align="center center">

                <div class="box-body" layout="row" layout-sm="column" layout-wrap="nowrap" layout-align="center center">

                    <h2 class="text-center noTopMargin" flex="100" >
                        Приветствуем, {{ $user->name }} {{ $user->surname }}!
                    </h2>
                    <hr class="divider">
                    <p class="subhead" flex=100 style="line-height: 2;text-align: center;font-size: 1.2em;">
                        Поздравляем Вас с успешной регистрацией! Просим пройти и завершить процесс. 
                    </p>
                    <div layout flex="100" layout-align="center center">
                      <md-button class="md-raised raspberryStyle" style="font-size: 2rem;"><a  class="whiteIt" href="/panel/salons/add">Далее</a> </md-button>
                    </div>
                </div>

                <!--  -->
            </md-card>
        </div>

    </div>

@stop
