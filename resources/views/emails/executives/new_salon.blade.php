<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>
<body style="background-color: #e8ecec; padding: 96px 0; color: #32323a; font-family: Tahoma, Geneva, sans-serif">
<table align="center" style="min-width: 445px; max-width: 825px; width: 90%; border-collapse: collapse; border: none;"
       cellspacing="0" cellpadding="0">

    <tr style="background:#222d32;">
        <td align="center"
            style="padding-top:25px; -moz-border-radius-topright:10px; -moz-border-top-left-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; -webkit-border-top-left-radius:10px; -webkit-border-top-right-radius:10px;">
            <img src="http://i.imgur.com/J4En58y.png" style="align: center;" width="250"/>
        </td>
    </tr>
    <!-- End Header with logo -->

    <!-- Header -->
    <tr style="background:#222d32">
        <td align="center" style="width: 100p; padding-top: 30px; padding-bottom: 40px; ">
            <p style="color: #fefeff; font-size: 26pt; font-weight: 500; line-height: 45px; padding: 0px 20px;">
                Клиент добавлен
            </p>
        </td>
    </tr>
    <!-- End Header -->

    <tr style="background:#ffffff;">
        <td colspan="2" style="padding: 30px 10px; text-align: center;">
            <p style="line-height: 25px"><span style="font-weight: 600; font-size: 20pt;">Уважаемые Executives!</span>
            </p>

            <p style="padding-top: 25px; line-height: 25px; font-size: 16pt;">
                Добавлен новый салон!
            </p>

            <p style="padding-top: 25px; line-height: 25px; font-size: 16pt;">
                Примите необходимые действия!
            </p>

            <ul>
                <li>
                    Салон:
                    <ul>
                        <li>{{ $salon->name }}</li>
                        <li>{{ $salon->address }}</li>
                        <li>{{ $salon->phone1 }}</li>
                    </ul>
                </li>
                <li>
                    Директор:
                    <ul>
                        <li>{{ $salon->director->name }} {{ $salon->director->surname }}</li>
                        <li>{{ $salon->director->phone }}</li>
                        <li>{{ $salon->director->email }}</li>
                    </ul>
                </li>
            </ul>

            <p>
                <a href="fb.excelsior.su/exe">Управление доступом</a>
            </p>

        </td>
    </tr>

    <tr style="background: #ffffff;">
        <td colspan="2"
            style="padding: 20px 10px; text-align: center; border-bottom-left-radius:10px; border-bottom-right-radius:10px; -webkit-border-bottom-left-radius:10px; -webkit-border-bottom-right-radius:10px; -moz-border-bottom-left-radius:10px; -moz-border-bottom-right-radius:10px;">
            <p style="font-size: 16pt;">С уважением,<br>Команда feedback</p>

            <p><a href="fb.excelsior.su" style="font-size: 16pt; color: #32323a;">fb.excelsior.su</a></p>
        </td>
    </tr>


    <tr style="background:#e8ecec; height:40px;">
        <td colspan="2"></td>
    </tr>
    <tr style="background:#ffffff; height:1px;">
        <td colspan="2" style="background:#d0d0d0;"></td>
    </tr>

    <tr style="background:#e8ecec;">
        <td colspan="2" style="padding-top:30px; text-align: center;">
            <p><a href="www.excelsior.su" style="font-size: 10pt; color: #32323a;">www.excelsior.su</a></p>
        </td>
    </tr>
</table>
</body>
</html>
