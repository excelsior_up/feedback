@extends('panel.layout')

@section('page_title')
    Страница не найдена
@stop

@section('content')

    @include('panel.partials.dashboard_navigator',
        [
            'dash_caption' => '404',
            'dash_caption_small' => 'Страница не найдена'
        ]
    )

    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="error-page">
            <h2 class="headline text-yellow"> &nbsp;</h2>
            <div class="error-content" style="margin-left:0;">
                <h3><i class="fa fa-warning text-yellow"></i> Упс! Страница не найдена.</h3>
                <p>
                    Мы не смогли найти запрашиваемую страницу.
                    Может быть, вы захотите вернуться в <a href="/panel">панель управления</a>.
                </p>
            </div><!-- /.error-content -->
            <div>
                <img src="/img/404.jpg" alt="404" class="img img-responsive">
            </div>
        </div><!-- /.error-page -->
    </section><!-- /.content -->

@stop
