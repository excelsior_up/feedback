<!DOCTYPE html>
<html >
<head >
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <title >Регистрация | Feedback</title >
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" >
    <link rel="stylesheet" href="/css/landing.css" >

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js" ></script >
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js" ></script >
    <![endif]-->

</head >

<body class="hold-transition register-page" >

<div class="register-box" >

    <div class="register-box-body" >
        <div class="register-logo" >
            <img src="/img/nozh/logo.png" alt="logotype" width="100px" >
        </div >
        <h5 class="login-box-msg" >Регистрация</h5 >

        <ul class="registerType" >
            <li class="active" type="salon">Салон красоты</li >
            <li type="master" >Мастер</li >
        </ul>

        <div >

            {!! Form::open() !!}

            <div >
                @include('flash::message')
            </div >
            {!! Form::input('hidden', 'salon','salon') !!}
            {!! Form::input('hidden', 'reg_type','0') !!}
            <div class="input-field " >
                <label for="surname" id="nameOrSurname">Название</label >
                {!! Form::text('surname', null, ['class' => 'form-control', 'required']) !!}
                <span class="glyphicon glyphicon-user form-control-feedback" ></span >
            </div >


            <!-- Имя -->
            <div class="input-field " >
                <label for="surname" >Имя</label >
                {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}

                <span class="glyphicon glyphicon-user form-control-feedback" ></span >
            </div >

            <!-- Почта  -->

            <div class="input-field " >
                <label data-error="wrong" for="email" data-success="right" >E-mail</label >
                {!! Form::email('email', null, ['class' => 'validate', 'required']) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback" ></span >
            </div >


            <!-- Номер  -->
            <div class="input-field " >
                <label for="phone" class="active" >Номер телефона</label >
                {!! Form::text('phone', null, ['class' => 'form-control phone-mask', 'required','placeholder' => '+7-7__-_______']) !!}

                <span class="glyphicon glyphicon-phone form-control-feedback" ></span >
            </div >


            <!-- Пароль  -->

            <div class="input-field " >
                <label for="surname" >Пароль</label >
                {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback" ></span >
            </div >

            <!-- Пароль еще раз  -->
            <div class="input-field " >
                <label for="surname" >Пароль</label >
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
                <span class="glyphicon glyphicon-log-in form-control-feedback" ></span >
            </div >


            <!-- Номер  -->
            <div class="input-field " >
                <label for="phone" class="active" >Вас пригласил</label >
                {!! Form::text('follow', null, ['class' => 'form-control phone-mask', 'required','placeholder' => '+7-7__-_______']) !!}

                <span class="glyphicon glyphicon-phone form-control-feedback" ></span >
            </div >

            <!-- Пароль еще раз  -->



            <div class="row" >
               <div class="col-xs-12">
                        <input type="checkbox" name="agree" id="agree" checked />
                        <label for="agree">
                             Я согласен с
                             <a href="#">пользовательским соглашением</a>
                        </label>
                </div>
                <div class="col-xs-12" style="margin-top: 1em" >
                    <button type="submit" class="col-xs-12 waves-effect waves-light btn" >
                        Зарегистрироваться
                    </button >
                </div ><!-- /.col -->
            </div >

            {!! Form::close() !!}
        </div >
        <br >
        <a href="/auth/login" class="another text-center" >Уже есть учетная запись?</a >
    </div ><!-- /.form-box -->
</div ><!-- /.register-box -->

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="/js/landing.js" ></script >


</body >
</html >
