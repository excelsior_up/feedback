<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Восстановление пароля | Feedback</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/landing.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition register-page">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49907441-11', 'auto');
    ga('send', 'pageview');

</script>


<div class="register-box">

    <div class="register-box-body">
        <div class="register-logo">
            <img src="/img/logo.png" alt="logotype" width="358px">
        </div>
        <h5 class="login-box-msg">Восстановление пароля для {{$email}}</h5>


            {!! Form::open(['method' => 'POST', 'url' => '/auth/refresh/'.$salt,'class' => 'clearfix']) !!}

                <div>
                    @include('flash::message')
                </div>

                <div class="input-field ">
                    <label data-error="wrong" for="email"  data-success="right">Ваш новый пароль</label>
                    {!! Form::password('password_1', null, ['class' => 'validate', 'required']) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="input-field ">
                    <label data-error="wrong" for="email"  data-success="right">Пароль еще раз</label>
                    {!! Form::password('password_2', null, ['class' => 'validate', 'required']) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>




                <div class="row col-xs-12">
                    <button type="submit" class="col-xs-12 waves-effect waves-light btn">Подтвердить</button>
                </div><!-- /.col -->

                <div class="row col-xs-12 " >

                    <a href="/auth/register" class="text-center another">Нет учетной записи?</a>
                    <br>
                    <a href="/auth/login" class="text-center another">Войти</a>

                </div>

                </div>



            {!! Form::close() !!}


        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <script src="/js/landing.js"></script>

</body>
</html>
