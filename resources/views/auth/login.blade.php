<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Вход | Feedback</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/landing.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition register-page">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49907441-11', 'auto');
    ga('send', 'pageview');

</script>

<div class="register-box">

    <div class="register-box-body">
        <div class="register-logo">
            <img src="/img/logo.png" alt="logotype" width="358px">
        </div>
        <h5 class="login-box-msg">Авторизация</h5>


                    {!! Form::open(['autocomplete' => 'off']) !!}

                        <div>
                            @include('flash::message')
                        </div>

                        <!-- Почта  -->

                        <div class="input-field ">
                            <label data-error="wrong" for="email"  data-success="right">Телефон</label>
                            {!! Form::text('phone', null, ['class' => 'phone-mask validate', 'required']) !!}
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <!-- Пароль  -->


                        <div class="input-field ">
                            <label for="surname">Пароль</label>
                            {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>

                        {{--<hr>--}}

                        <!-- Роль  -->
                        {{--<div class="has-feedback">
                            <div>
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group" role="group">
                                        <input type="radio" name="role" id="role_director" value="director" required>
                                        <label for="role_director">Директор</label>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <input type="radio" name="role" id="role_admin" value="admin" required>
                                        <label for="role_admin">Администратор</label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group" role="group">
                                        <input type="radio" name="role" id="role_manager" value="manager" required>
                                        <label for="role_manager">Менеджер</label>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <input type="radio" name="role" id="role_master" value="master" required>
                                        <label for="role_master">Мастер</label>
                                    </div>
                                </div>
                            </div>

                        </div>--}}

                        <div class="row">

                            <div class="col-xs-8">
                                <input type="checkbox" id="remember_me" name="remember_me" checked>
                                <label for="remember_me"> Запомнить меня?</label>

                            </div><!-- /.col -->

                            <div class="col-xs-4">
                                <button type="submit" class="waves-effect waves-light btn">Войти</button>
                            </div><!-- /.col -->
                        </div>

                    {!! Form::close() !!}

                <br>
                <a href="/auth/register" class="text-center another">Нет учетной записи?</a>
                <br>
                <a href="/auth/resetpassword" class="text-center another">Забыли пароль?</a>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <script src="/js/landing.js"></script>

</body>
</html>
