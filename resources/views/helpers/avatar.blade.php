{{-- Круглый аватар --}}
<div
    alt="{{ isset($caption) ? $caption : '?' }}"
    style="background-image:url({{ isset($avatar) ? $avatar  : '/img/user.png' }})"
    class="avatarRound"
> </div>
