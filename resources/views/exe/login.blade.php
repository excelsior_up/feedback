@extends('exe.layout')

@section('content')
    {!! Form::open(['class' => 'form-horizontal']) !!}

    <div class="form-group">
        {!! Form::label('email', 'Введите e-mail: ', ['class' => 'control-label col-sm-3']) !!}
        <div class="col-sm-9">
            {!! Form::text('email', null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Пароль: ', ['class' => 'control-label col-sm-3']) !!}
        <div class="col-sm-9">
            {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9 text-right">
            <button type="submit" class="btn btn-lg btn-primary btn-block">
                Войти
            </button>
        </div>
    </div>

    {!! Form::close() !!}
@stop