@extends('exe.layout')

@section('content')


    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($salons as $salon)

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{ $salon->id }}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $salon->id }}" aria-expanded="true" aria-controls="collapse{{ $salon->id }}">
                            {{ $salon->name }}, {{ date('d.m.Y, H:i', strtotime($salon->created_at)) }}
                        </a>
                    </h4>
                </div>
                <div id="collapse{{ $salon->id }}" class="panel-collapse collapse {{ $salon->isApproved ? '' : 'in' }}" role="tabpanel" aria-labelledby="heading{{ $salon->id }}">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <fieldset>
                                <legend>Директор: </legend>
                                <ul class="list-unstyled">
                                    <li>
                                        <label for="">ФИО:</label>
                                        <span>
                                            {{ $salon->director ? $salon->director->name : 'Нет имени' }}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Номер:</label>
                                        <span>
<!--                                            --><?php //var_dump($salon->director->phone); die(); ?>
                                            {{ $salon->director ? $salon->director->phone : 'Нет номера' }}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Email:</label>
                                        <span>
                                              {{ $salon->director ? $salon->director->email : 'Нет email' }}
                                        </span>
                                    </li>
                                </ul>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <legend>Салон: </legend>
                                <ul class="list-unstyled">
                                    <li>
                                        <label for="">Имя:</label>
                                        <span>
                                            {{ $salon->name }}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Описание:</label>
                                        <span>
                                             {{ $salon->director ? $salon->director->description : 'Нет описания' }}
                                            {{--{{ $salon->description }}--}}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Телефон:</label>
                                        <span>
                                            {{ $salon->director ? $salon->director->phone1 : 'Нет телефона' }}
                                            {{--{{ $salon->phone1 }}--}}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Адрес:</label>
                                        <span>
                                            {{ $salon->address }}
                                        </span>
                                    </li>
                                    <li>
                                        <label for="">Сайт:</label>
                                        <span>
                                            {{ $salon->director ? $salon->director->site : 'Нет сайта' }}
                                            {{--{{ $salon->site }}--}}
                                        </span>
                                    </li>
                                    <li>
                                        @if($salon->isApproved)
                                            <label for="">Подтвержден: </label>
                                            <span>
                                                {{ date('d.m.Y, H:i', strtotime($salon->approved_at)) }}
                                            </span>
                                            <a href="/exe/remove/{{ $salon->id }}" class="btn btn-danger pull-right">
                                                <i class="glyphicon glyphicon-remove"></i>
                                                Снять
                                            </a>
                                        @else
                                            <label for="">Подтвердить: </label>
                                            <a href="/exe/approve/{{ $salon->id }}" class="btn btn-primary">
                                                <i class="glyphicon glyphicon-ok"></i>
                                                OK
                                            </a>
                                        @endif
                                    </li>
                                </ul>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach

    </div>

@stop