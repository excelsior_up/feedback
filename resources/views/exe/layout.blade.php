<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Управление доступом</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <style>
        .btnExcel{
            -webkit-transition: 400ms;
            -moz-transition: 400ms;
            -ms-transition: 400ms;
            -o-transition: 400ms;
            transition: 400ms;
            margin: 0 auto;
            display: block;
            float: none;
        }
        .btnExcel:hover{
            -webkit-transform: scale(0.9);
            -moz-transform: scale(0.9);
            -ms-transform: scale(0.9);
            -o-transform: scale(0.9);
            transform: scale(0.9);
            width: 200px;
            margin: 0 auto;
            box-shadow: 0px 0px 10px rgba(0,0,0,0.5);
        }
    </style>

</head>

<body>

<div class="container">
    @if(Auth::exe()->check())
        <div class="col-xs-12" style="margin: 1em 0">
            <a role="button"   style="background: #3452ff; color:white" class="col-xs-12  btnExcel btn " href="/excel">
                Экспорт в Excel
            </a>
        </div>
    @endif
    <div class="header clearfix" style="margin-bottom: 1em">
        <nav>
            @if(Auth::exe()->check())
                <ul class="nav nav-pills pull-right">
                    <li role="presentation" class="active"><a href="#">{{ $exe->name }}</a></li>
                    <li role="presentation"><a href="/exe/logout">Выход</a></li>
                </ul>
            @endif
        </nav>
        <h3 class="text-muted">Feedback Executives</h3>
    </div>

    <div class="row marketing">
        <div class="col-lg-12">

            @yield('content')

        </div>
    </div>

    <footer class="footer">
        <p>&copy; Excelsior 2015</p>
    </footer>

</div> <!-- /container -->

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
