<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterService extends Model
{
    protected $table = 'master_services';

    protected $fillable = [
        'master_id',
        'service_id',
    ];

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
}
