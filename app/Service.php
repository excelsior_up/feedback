<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function salons()
    {
        return $this->belongsToMany('App\SalonService');
    }

    public function masters()
    {
        return $this->belongsToMany('App\MasterService');
    }
}
