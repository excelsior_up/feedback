<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    // Таблица базы данных для товаров
    protected $table = 'goods';

    // Поля для заполнения
    protected $fillable = [
        'salon_id',
        'brand',
        'name',
        'category',
        'options',
        'amount',
        'price',
        'capacity'
    ];

    // Связь 1/1 с Салоном
    public function salon()
    {
        return $this->belongsTo('App\Salon', 'salon_id');
    }
}
