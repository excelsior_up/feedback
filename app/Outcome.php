<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Outcome extends Model
{
    // Таблица базы данных
    protected $table = 'outcomes';

    // Поля заполнения
    protected $fillable = [
        'salon_id',
        'name',
        'category',
        'quantity',
        'price',
        'sum',
    ];

    // Связь 1/1 с салоном
    public function salon()
    {
        return $this->belongsTo('App\Salon');
    }

    public function scopeForSalon($query, $salon_id)
    {
        return $query->where('salon_id', $salon_id);
    }

    public function scopeForWeek($query)
    {
        return $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    }
}
