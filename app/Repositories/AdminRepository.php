<?php


namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminsController;

use Storage;

use App\Helpers\ImageHelper;

use App\Admin;
use App\Salon;

class AdminRepository
{
    private $adminsController;

    public function __construct(AdminsController $adminsController)
    {
        $this->adminsController = $adminsController;
    }

    public function create(Request $request, $salon_id)
    {
        $salon = Salon::findOrFail($request->input('salon_id'));
        if(
            $salon->admins
                ->where('name',    $request->input('name'))
                ->where('surname', $request->input('surname'))
                ->where('email',   $request->input('email'))
                ->count()
        ){
            // Администратор уже числится в базе салона - вывести ошибку
            $errorMessage = 'Администратор уже числится в базе салона!';
            return $this->adminsController->returnFail($errorMessage, $request);
        }

        if($request->input('password_first') != $request->input('password_confirmation'))
        {
            $errorMessage = 'Пароли не совпадают!';
            return $this->adminsController->returnFail($errorMessage, $request);
        }

        // $requestData = $request->all();
        // unset($requestData['_token']);

        $admin = Admin::create(
            array_merge($request->all(), ['salon_id' => $request->input('salon_id')])
        );

        // Пароли
        $admin->password = bcrypt($request->input('password_first'));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $admin ) return $this->adminsController->returnFail($errorMessage, $request);

        // Аватар
        if($request->hasFile('avatar'))
        {
            $admin->avatar = ImageHelper::save($request->file('avatar'), 'admins');
        } else {
            $admin->avatar = NULL;
        }

        $admin->save();

        $successMessage = 'Вы успешно добавили нового администратора! Теперь он появится в списке.';

        return $this->adminsController->returnSuccess($successMessage);
    }

    public function edit($id, Request $request)
    {
        $admin = Admin::find($id);

        // Имеется ли еще один администратор с данными (введенными с сайта)
        $salon = Salon::findOrFail($admin->salon_id);
        $count = $salon->admins
            ->where('name', $request->input('name'))
            ->where('surname', $request->input('surname'))
            ->where('email', $request->input('email'));
        if($count->count()){
            if($count->first()->id != $admin->id)
            {
                // Администратор уже числится в базе салона - вывести ошибку
                $errorMessage = 'Администратор уже числится в базе салона!';
                return $this->adminsController->returnFail($errorMessage, $request);
            }
        }

        if ( ! $admin ) return $this->adminsController->returnFail('Такого администратора не существует!');


        // Аватар
        if($request->hasFile('avatar'))
        {
            if ($admin->avatar)
            {
                Storage::disk('local_public')->delete($admin->avatar);
            }
            $admin->avatar = ImageHelper::save($request->file('avatar'), 'admins');
        }
        else
        {
            //$manager->avatar = "no";
        }
        $admin->update($request->all());

        $admin->save();

        $successMessage = 'Вы успешно отредактировали администратора!';

        return $this->adminsController->returnSuccess($successMessage);
    }

    public function change_password($id)
    {
        $admin = Admin::find($id);
        // TODO changePassword
    }

    public function delete($id)
    {
        $admin = Admin::find($id);

        if ( ! $admin ) return $this->adminsController->returnFail('Такого администратора не существует!');

        if ($admin->avatar)
        {
            Storage::disk('local_public')->delete($admin->avatar);
        }

        $admin->delete();

        return $this->adminsController->returnSuccess('Администратор успешно удален!');
    }
}
