<?php


namespace App\Repositories;

use App\Executive;
use Illuminate\Http\Request;
use App\Http\Controllers\SalonsController;

use App\Helpers\ImageHelper;
use App\SalonService;

use Mail;

use App\Salon;

class SalonRepository
{
    private $salonsController;

    public function __construct(SalonsController $salonsController)
    {
        $this->salonsController = $salonsController;
    }

    /**
     * Перевод на страницу добавления услуг при неудачной предыдущей попытке
     *
     * @param Request $request
     * @param $director_id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function addServicesRedirect(Request $request, $director_id)
    {
        $user = Auth::director()->get();

        $latest_added_salon = $user->salons[sizeof($user->salons)-1];



        $successMessage = 'Салон успешно добавлен. Для завершения процесса добавления, выберите услуги, которые предоставляет ваш салон.';

        return $this->salonsController->getAddServices($latest_added_salon->id, $successMessage, $from = 'repo');
    }



    /**
     * Создание нового салона
     *
     * @param Request $request
     * @param $director_id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create(Request $request, $director_id)
    {
        $salon = Salon::create(array_merge($request->all(), ['director_id' => $director_id]));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $salon ) return $this->salonsController->returnFail($errorMessage, $request);

        if ($request->hasFile('image'))
        {
            $salon->image = ImageHelper::save($request->file('image'), 'salons');
        }

        $salon->save();

        $successMessage = 'Салон успешно добавлен. Для завершения процесса добавления, выберите услуги, которые предоставляет ваш салон.';

        return $this->salonsController->getAddServices($salon->id, $successMessage, $from = 'repo');
    }

    /**
     * Добавления услуг салона
     *
     * @param $salon_id
     * @param $services
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addServices($salon_id, $services)
    {
        $salon = Salon::find($salon_id);

        foreach($services as $service)
        {
            $salon->services()->create(['service_id' => $service]);
        }

        /*foreach ($request->input('service_ids') as $service)
        {
            $s_service = new SalonService;
            $s_service->salon_id = $salon->id;
            $s_service->service_id = $service;
            $s_service->save();
        }*/

        $user = $salon->director;
        $email = $user->email;

        Mail::send('emails.add_salon', compact('user', 'salon'), function($message) use ($email) {
            $message->to($email)->subject('Салон добавлен');
        });

        $exec_emails = Executive::lists('email')->toArray();

        Mail::send('emails.executives.new_salon', compact('salon'), function($message) use ($exec_emails) {
            $message->to($exec_emails)->subject('Салон добавлен');
        });

        $message = 'Поздравляем! Процесс добавления салона успешно завершен!';

        return $this->salonsController->returnSuccess($message);
    }

    /**
     * Редактирование салона
     *
     * @param $id
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function edit($id, Request $request)
    {
        $salon = Salon::find($id);

        if ( ! $salon ) return $this->salonsController->returnFail('Такого салона не существует!');

        $salon->update($request->all());

        if ($request->hasFile('image'))
        {
            $salon->image = ImageHelper::save($request->file('image'), 'salons');
        }

        $salon->save();

        //$salon->services()->delete();

        $successMessage = 'Вы успешно отредактировали информацию салона!';

        return $this->salonsController->returnSuccess($successMessage);
    }

    /**
     * Удаление салона
     *
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $salon = Salon::find($id);

        if ( ! $salon ) return $this->salonsController->returnFail('Такого салона не существует!');

        /*$salon->masters->delete();
        $salon->clients->delete();*/
        $salon->services()->delete();
        $salon->delete();

        return $this->salonsController->returnSuccess('Салон успешно удален!');
    }
}
