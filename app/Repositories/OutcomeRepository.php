<?php


namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\OutcomesController;

use Storage;
use App\Helpers\ImageHelper;

use Mail;

use App\Outcome;
use App\Salon;

class OutcomeRepository
{
    private $outcomesController;

    public function __construct(OutcomesController $outcomesController)
    {
        $this->outcomesController = $outcomesController;
    }

    public function create(Request $request, $user)
    {
        $salon_id = $user->salons->first()->id;

        $outcome = Outcome::create(array_merge($request->all(), ['salon_id' => $salon_id]));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $outcome ) return $this->outcomesController->returnFail($errorMessage, $request);

        $outcome->sum = $outcome->price * $outcome->quantity;

        $outcome->save();

        $email = $user->email;

        if( !($user->settings))
        {
            Mail::send('emails.add_outcome', compact('user', 'outcome'), function($message) use ($email) {
                $message->to($email)->subject('Добавлены данные о затратах');
            });
        } else {
            if($user->settings->add_outcome_mail == 1)
            {
                Mail::send('emails.add_outcome', compact('user', 'outcome'), function($message) use ($email) {
                    $message->to($email)->subject('Добавлены данные о затратах');
                });
            }
        }

        $successMessage = 'Данные о затратах добавлены в список.';

        return $this->outcomesController->returnSuccess($successMessage, '/panel/outcomes');
    }

    public function delete($id)
    {
        // $client = Client::find($id);
        //
        // if ( ! $client ) return $this->clientsController->returnFail('Такого клиента не существует!');
        //
        // if ($client->avatar)
        // {
        //     Storage::disk('local_public')->delete($client->avatar);
        // }
        //
        // $client->delete();
        //
        // $message = 'Клиент успешно удален!';
        // return $this->clientsController->returnSuccess($message);
    }
}
