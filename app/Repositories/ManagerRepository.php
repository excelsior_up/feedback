<?php


namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\ManagersController;

use Storage;

use App\Helpers\ImageHelper;

use App\Manager;
use App\Salon;

class ManagerRepository
{
    private $managersController;

    public function __construct(ManagersController $managersController)
    {
        $this->managersController = $managersController;
    }

    public function create(Request $request, $salon_id)
    {
        $salon = Salon::findOrFail($request->input('salon_id'));
        if(
            $salon->managers
                ->where('name',    $request->input('name'))
                ->where('surname', $request->input('surname'))
                ->where('email',   $request->input('email'))
                ->count()
        ){
            // Менеджер уже числится в базе салона - вывести ошибку
            $errorMessage = 'Менеджер уже числится в базе салона!';
            return $this->managersController->returnFail($errorMessage, $request);
        }

        if($request->input('password_first') != $request->input('password_confirmation'))
        {
            $errorMessage = 'Пароли не совпадают!';
            return $this->managersController->returnFail($errorMessage, $request);
        }

        $manager = Manager::create(
            array_merge($request->all(), ['salon_id' => $request->input('salon_id')])
        );

        // Пароли
        $manager->password = bcrypt($request->input('password_first'));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $manager ) return $this->managersController->returnFail($errorMessage, $request);

        // Аватар
        if($request->hasFile('avatar'))
        {
            $manager->avatar = ImageHelper::save($request->file('avatar'), 'managers');
        } else {
            $manager->avatar = NULL;
        }

        $manager->save();

        $successMessage = 'Вы успешно добавили нового менеджера! Теперь он появится в списке.';

        return $this->managersController->returnSuccess($successMessage);
    }

    public function edit($id, Request $request)
    {
        $manager = Manager::find($id);

        // Имеется ли еще один мастер с данными (введенными с сайта)
        $salon = Salon::findOrFail($manager->salon_id);

        $count = $salon->managers
            ->where('name', $request->input('name'))
            ->where('surname', $request->input('surname'))
            ->where('email', $request->input('email'));
        if($count->count()){
            if($count->first()->id != $manager->id)
            {
                // Клиент уже числится в базе салона - вывести ошибку
                $errorMessage = 'Менеджер уже числится в базе салона!';
                return $this->managersController->returnFail($errorMessage, $request);
            }
        }

        if ( ! $manager ) return $this->managersController->returnFail('Такого менеджера не существует!');

        // Аватар
        if($request->hasFile('avatar'))
        {
            if ($manager->avatar)
            {
                Storage::disk('local_public')->delete($manager->avatar);
            }

            $manager->avatar = ImageHelper::save($request->file('avatar'), 'managers');
        }

        $manager->update($request->all());

        $manager->save();

        $successMessage = 'Вы успешно отредактировали менеджера!';

        return $this->managersController->returnSuccess($successMessage);
    }

    public function change_password($id)
    {
        $manager = Manager::find($id);
        // TODO changePassword
    }

    public function delete($id)
    {
        $manager = Manager::find($id);

        if ( ! $manager ) return $this->managersController->returnFail('Такого менеджера не существует!');

        if ($manager->avatar)
        {
            Storage::disk('local_public')->delete($manager->avatar);
        }

        $manager->delete();

        return $this->managersController->returnSuccess('Менеджер успешно удален!');
    }
}
