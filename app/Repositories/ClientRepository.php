<?php


namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\ClientsController;

use Storage;
use App\Helpers\ImageHelper;

use Mail;

use App\Client;
use App\Salon;

class ClientRepository
{
    private $clientsController;

    public function __construct(ClientsController $clientsController)
    {
        $this->clientsController = $clientsController;
    }

    public function create(Request $request, $salon_id)
    {
        $salon = Salon::findOrFail($salon_id);

        if(
            $salon->clients
                ->where('name', $request->input('name'))
                ->where('surname', $request->input('surname'))
                ->where('email', $request->input('email'))
                ->count()
        ){
            // Клиент уже числится в базе салона - вывести ошибку
            $errorMessage = 'Клиент уже числится в базе салона!';
            return $this->clientsController->returnFail($errorMessage, $request);
        }

        $client = Client::create(array_merge($request->all(), ['salon_id' => $salon_id]));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $client ) return $this->clientsController->returnFail($errorMessage, $request);

        // Аватар
        if($request->hasFile('avatar'))
        {
            $client->avatar = ImageHelper::save($request->file('avatar'), 'clients');
        } else {
            $client->avatar = NULL;
        }

        $client->save();

        $user = $client->salon->director;
        $email = $user->email;

        if( !($user->settings))
        {
            $clientLink = url('/panel/clients/profile/' . $client->id);
            Mail::send('emails.add_client', compact('user', 'client', 'clientLink'), function($message) use ($email) {
                $message->to($email)->subject('Клиент добавлен');
            });
        } else {
            if($user->settings->add_client_mail == 1)
            {
                $clientLink = url('/panel/clients/profile/' . $client->id);
                Mail::send('emails.add_client', compact('user', 'client', 'clientLink'), function($message) use ($email) {
                    $message->to($email)->subject('Клиент добавлен');
                });
            }
        }

        $successMessage = 'Вы успешно добавили нового клиента! Теперь он появится в списке.';

        return $this->clientsController->returnSuccess($successMessage, '/panel/clients/profile/' . $client->id);
    }

    public function edit($id, Request $request)
    {
        $client = Client::find($id);

        // Имеется ли еще один клиент с данными (введенными с сайта)
        $salon = Salon::findOrFail($client->salon_id);
        $count = $salon->clients
            ->where('name', $request->input('name'))
            ->where('surname', $request->input('surname'))
            ->where('email', $request->input('email'));
        if($count->count()){
            if ($count->first()->id != $client->id)
            {
                // Клиент уже числится в базе салона - вывести ошибку
                $errorMessage = 'Клиент уже числится в базе салона!';
                return $this->clientsController->returnFail($errorMessage, $request);
            }
        }

        if ( ! $client ) return $this->clientsController->returnFail('Такого клиента не существует!');

        // Аватар
        if($request->hasFile('avatar'))
        {
            if ($client->avatar)
            {
                Storage::disk('local_public')->delete($client->avatar);
            }

            $client->avatar = ImageHelper::save($request->file('avatar'), 'clients');
        }
        else
        {
            //$client->avatar = "no";
        }

        $client->update($request->all());

        $client->save();

        $successMessage = 'Вы успешно отредактировали клиента!';

        return $this->clientsController->returnSuccess($successMessage, '/panel/clients/profile/' . $client->id);
    }

    public function delete($id)
    {
        $client = Client::find($id);

        if ( ! $client ) return $this->clientsController->returnFail('Такого клиента не существует!');

        if ($client->avatar)
        {
            Storage::disk('local_public')->delete($client->avatar);
        }

        $client->delete();

        $message = 'Клиент успешно удален!';
        return $this->clientsController->returnSuccess($message);
    }
}
