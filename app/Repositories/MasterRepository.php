<?php


namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\MastersController;

use Storage;
use Mail;

use App\Helpers\ImageHelper;
use App\SalonService;

use App\Master;
use App\Salon;

class MasterRepository
{
    private $mastersController;

    public function __construct(MastersController $mastersController)
    {
        $this->mastersController = $mastersController;
    }

    public function create(Request $request, $salon_id)
    {
        $salon = Salon::findOrFail($salon_id);

        if(
            $salon->masters
                ->where('name', $request->input('name'))
                ->where('surname', $request->input('surname'))
                ->where('email', $request->input('email'))
                ->count()
        ){
            // Клиент уже числится в базе салона - вывести ошибку
            $errorMessage = 'Мастер уже числится в базе салона!';
            return $this->mastersController->returnFail($errorMessage, $request);
        }

        $master = Master::create(array_merge($request->all(), ['salon_id' => $salon_id]));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $master ) return $this->mastersController->returnFail($errorMessage, $request);

        // Аватар
        if($request->hasFile('avatar'))
        {
            $master->avatar = ImageHelper::save($request->file('avatar'), 'masters');
        } else {
            $master->avatar = NULL;
        }

        $master->save();

        // master services
        if ($request->input('master_services'))
        {
            foreach($request->input('master_services') as $service)
            {
                $master->services()->create(['service_id' => $service]);
            }
        }

        $user = $master->salon->director;

        if( !($user->settings))
        {
            $email = $user->email;
            $clientLink = url('/panel/masters/profile/' . $master->id);
            Mail::send('emails.add_master', compact('user', 'master', 'clientLink'), function($message) use ($email) {
                $message->to($email)->subject('Мастер добавлен');
            });
        } else {
            if($user->settings->add_master_mail == 1)
            {
                $email = $user->email;
                $clientLink = url('/panel/masters/profile/' . $master->id);
                Mail::send('emails.add_master', compact('user', 'master', 'clientLink'), function($message) use ($email) {
                    $message->to($email)->subject('Мастер добавлен');
                });
            }
        }

        $successMessage = 'Вы успешно добавили нового мастера! Теперь он появится в списке.';

        return $this->mastersController->returnSuccess($successMessage, '/panel/masters/profile/' . $master->id);
    }

    public function edit($id, Request $request)
    {
        $master = Master::find($id);

        // Имеется ли еще один клиент с данными (введенными с сайта)
        $salon = Salon::findOrFail($master->salon_id);
        $count = $salon->masters
            ->where('name', $request->input('name'))
            ->where('surname', $request->input('surname'))
            ->where('email', $request->input('email'));
        if($count->count()){
            if ($count->first()->id != $master->id)
            {
                // Клиент уже числится в базе салона - вывести ошибку
                $errorMessage = 'Мастер уже числится в базе салона!';
                return $this->mastersController->returnFail($errorMessage, $request);
            }
        }

        if ( ! $master ) return $this->mastersController->returnFail('Такого мастера не существует!');

        $master->update($request->all());

        // Аватар
        if($request->hasFile('avatar'))
        {
            if ($master->avatar)
            {
                Storage::disk('local_public')->delete($master->avatar);
            }
            $master->avatar = ImageHelper::save($request->file('avatar'), 'masters');
        }

        // master services
        $master->services()->delete();
        // if ($request->input('master_services'))
        // {
            foreach($request->input('master_services') as $service)
            {
                $master->services()->create(['service_id' => $service]);
            }
        // }

        $master->save();

        $successMessage = 'Вы успешно отредактировали мастера!';

        return $this->mastersController->returnSuccess($successMessage, '/panel/masters/profile/' . $master->id);
    }

    public function delete($id)
    {
        $master = Master::find($id);

        if ( ! $master ) return $this->mastersController->returnFail('Такого мастера не существует!');

        if ($master->avatar)
        {
            Storage::disk('local_public')->delete($master->avatar);
        }
        $master->delete();

        return $this->mastersController->returnSuccess('Мастер успешно удален!');
    }
}
