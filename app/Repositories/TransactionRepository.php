<?php


namespace app\Repositories;

use App\Http\Controllers\TransactionsController;
use App\Transaction;
use Illuminate\Http\Request;
use Mail;

class TransactionRepository
{
    private $transactionsController;

    public function __construct(TransactionsController $transactionsController)
    {
        $this->transactionsController = $transactionsController;
    }

    public function create(Request $request, $salon_id)
    {
        if ( ! $request->input('service_id') ||
             ! $request->input('master_id') ||
             ! $request->input('client_id'))
        {
            $message = 'Проверьте наличие мастера или клиента!';

            return $this->transactionsController->returnFail($message);
        }

        $transaction = Transaction::create(array_merge($request->all(), ['salon_id' => $salon_id]));

        $message = 'Транзакция успешно добавлена!';

        if ($transaction)
        {

            $user  = $transaction->master->salon->director;
            $email = $user->email;
            $serviceS = \App\SalonService::where('id', $transaction->service_id)->first()->service->name;

            if( !($user->settings))
            {
                Mail::send('emails.add_transaction', compact('user', 'transaction', 'serviceS'), function($message) use ($email) {
                    $message->to($email)->subject('Добавлена оказанная услуга');
                });
            } else {
                if($user->settings->add_transacton_mail == 1)
                {
                    Mail::send('emails.add_transaction', compact('user', 'transaction', 'serviceS'), function($message) use ($email) {
                        $message->to($email)->subject('Добавлена оказанная услуга');
                    });
                }
            }

            return $this->transactionsController->returnSuccess($message);
        }

    }
}
