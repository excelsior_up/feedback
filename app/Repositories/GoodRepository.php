<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Controllers\GoodsController;

use Storage;
use App\Helpers\ImageHelper;

use Mail;

use App\Good;
use App\Salon;

class GoodRepository
{
    private $goodsController;

    public function __construct(GoodsController $goodsController)
    {
        $this->goodsController = $goodsController;
    }

    public function create(Request $request, $user)
    {
        $salon_id = $user->salons->first()->id;

        $good = Good::create(array_merge($request->all(), ['salon_id' => $salon_id]));

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $good ) return $this->goodsController->returnFail($errorMessage, $request);

        $good->save();

        // Отправка почты
        // $email = $user->email;
        //
        // Mail::send('emails.add_outcome', compact('user', 'outcome'), function($message) use ($email) {
        //     $message->to($email)->subject('Добавлены данные о затратах');
        // });

        $successMessage = 'Новый расходный материал добавлен в список!';

        return $this->goodsController->returnSuccess($successMessage, '/panel/goods');
    }

    public function edit($id, Request $request)
    {
        $good = Good::findOrFail($id);

        $errorMessage = 'Проверьте поля на правильность заполнения!';

        if ( ! $good ) return $this->goodsController->returnFail($errorMessage, $request);

        $good->update($request->all());
        $good->save();

        $successMessage = 'Расходный материал отредактирован!';

        return $this->goodsController->returnSuccess($successMessage, '/panel/goods');
    }

    public function delete($id, $user)
    {
        // Салон пользователя
        $salon = $user->salons->first();

        // Выбор расходного материала
        $good = Good::find($id);

        // Расходный материал не найден
        if ( ! $good ) return $this->goodsController->returnFail('Такого расходного материала не существует!');

        // Расходный материал записан в салоне пользователя, удаляющего его
        if ($good->salon->id != $salon->id){
            return $this->goodsController->returnFail('Ошибка!');
        }

        // Удаление изображения расходного материала
        if ($good->photo)
        {
            Storage::disk('local_public')->delete($good->photo);
        }

        // Удаление
        $good->delete();

        // Успешное завершение
        $message = 'Расходный материал успешно удален!';
        return $this->goodsController->returnSuccess($message);
    }
}
