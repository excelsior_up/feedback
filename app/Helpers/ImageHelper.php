<?php


namespace app\Helpers;

use Image;

class ImageHelper
{

    /**
     * @param null $file
     * @param string $folder
     * @return bool|string
     * @internal param string $prefix
     */
    public static function save($file, $folder = '')
    {
        $dir = '/uploads'. '/' . $folder . '/';

        $destinationPath = public_path() . $dir;

        $extension = $file->getClientOriginalExtension();

        $filename = $folder . '_' . str_random(6) . '.' . $extension;

        while (file_exists($destinationPath . $filename))
            $filename = $folder . '_' . str_random(6) . '.' . $extension;

        $upload_success = $file->move($destinationPath, $filename);

        if ($upload_success) {
            /*try
            {
                $img = Image::make($destinationPath . $filename);
                $img->resize(475, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $img->save($destinationPath . $filename);

                $project->preview = $dir . $filename;
            }
            catch (Exception $e) {}*/

            return $dir . $filename;
        }
        //
        //return $dir . $filename;
    }

}
