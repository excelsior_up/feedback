<?php


namespace app\Helpers;
use Validator;

class ValidationHelper
{
    /**
     * Validation helper make function
     *
     * @param array $data
     * @param array $rules
     * @return \stdClass
     */
    public static function make(Array $data, Array $rules)
    {
        $validation = Validator::make($data, $rules);

        $result = new \stdClass();
        $result->failed = false;

        if ($validation->fails())
        {
            $messages = '<ul>';
            foreach($validation->messages()->all() as $message)
            {
                $messages .= '<li>' . $message . '</li>';
            }
            $messages .= '</ul>';

            $result->failed = true;
            $result->error = $messages;
        }

        return $result;
    }
}