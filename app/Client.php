<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'salon_id',
        'name',
        'surname',
        'phone',
        'birthday',
        'email',

    ];

    // Таблица базы данных
    protected $table = 'clients';

    // Связь 1/1 с Салоном
    protected function salon()
    {
        return $this->belongsTo('App\Salon');
    }

    // Связь 1/n с Резервациями
    protected function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function scopeFindByEmail($query, $email)
    {
        return $query->where('email', $email)->first();
    }

    public function scopeForSalon($query, $salon_id)
    {
        return $query->where('salon_id', $salon_id);
    }
}
