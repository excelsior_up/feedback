<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    // Таблица базы данных
    protected $table = 'salons';

    protected $fillable = [
        'subscription_id',
        'director_id',
        'name',
        'email',
        'description',
        'address',
        'phone1',
        'phone2'
    ];

    // Связь 1/1 с Директором
    protected function director()
    {
        return $this->belongsTo('App\Director');
    }

    // Связь 1/n с Администраторами
    protected function admins()
    {
        return $this->hasMany('App\Admin');
    }

    // Связь 1/n с Клиентами
    protected function clients()
    {
        return $this->hasMany('App\Client');
    }

    // Связь 1/n с Резервациями
    protected function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    // Связь 1/n с Затратами
    protected function outcomes()
    {
        return $this->hasMany('App\Outcome');
    }

    // Связь 1/n с Товарами
    protected function goods()
    {
        return $this->hasMany('App\Good');
    }

    // Связь 1/n с Мастерами
    public function masters()
    {
        return $this->hasMany('App\Master');
    }

    // Связь 1/n с Меджерами
    public function managers()
    {
        return $this->hasMany('App\Manager');
    }

    // Связь 1/n с Услугами
    public function services()
    {
        return $this->hasMany('App\SalonService');
    }

    // Связь 1/1 с Подпиской
    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
}
