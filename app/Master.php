<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $fillable = [
        'salon_id',
        'name',
        'surname',
        'email',
        'phone',
    ];

    public function salon()
    {
        return $this->belongsTo('App\Salon');
    }

    public function services()
    {
        return $this->hasMany('App\MasterService');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }
}
