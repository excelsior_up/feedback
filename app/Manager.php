<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    // Таблица базы данных
    protected $table = 'managers';

    // Заполняемые поля
    protected $fillable = [
        'salon_id',
        'name',
        'surname',
        'email'
    ];

    // Связь 1/1 с Салоном
    public function salon()
    {
        return $this->belongsTo('App\Salon');
    }
}
