<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'director_id',
    ];

    // Таблица базы данных
    protected $table = 'settings';

    public function director()
    {
        return $this->belongsTo('App\Director', 'director_id');
    }
}
