<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Flash;
use Storage;

use App\Salon;
use App\Client;
use App\Transaction;

use App\Repositories\ClientRepository;

class ClientsController extends Controller
{
    protected $clientRepository;

    /**
     * Конструкор класса клиентов
     *
     */
    public function __construct()
    {
        $this->clientRepository = new ClientRepository($this);
    }

    /**
     * Страница клиентов
     *
     */
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.clients.index", compact('user', 'salons'));
    }

    /**
     * Добавление клиента
     *
     */
    public function getAdd()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.clients.add", compact('user', 'salons'));
    }

    /**
     * Добавление клиента
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request)
    {
        // Проверка на существование
        // добавляемого пользователя в салоне
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        return $this->clientRepository->create($request, $salon_id);
    }

    /**
     * Редактирование клиента
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
/*
    public function getEdit($id)
    {
        $user = $this->getCurrentUser();

        if(count($user->salons))
        {
            $salons = $user->salons;

            $client = Client::findOrFail($id);
            foreach ($salons as $salon)
            {
                if($salon->id == $client->salon_id)
                {
                    return view("panel.clients.edit",
                        ['user' => $user, 'salons' => $salons, 'client' => $client]
                    );
                }
            }
            return redirect()->to('/panel/clients');
        }
        else
        {
            return "нет салона - нет клиентов";
        }
    }
*/
    /**
     * Редактирование клиента
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit($id, Request $request)
    {
        $user = $this->getCurrentUser();

        if(count($user->salons))
        {
            $salons = $user->salons;

            $client = Client::findOrFail($id);
            foreach ($salons as $salon)
            {
                if($salon->id == $client->salon_id)
                {
                    return $this->clientRepository->edit($id, $request);
                }
            }
            return redirect()->to('/panel/clients');
        }
        else
        {
            return redirect()->to('/panel/clients');
        }
    }

    /****
     * Удаление клиента
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($id)
    {
        if(Auth::admin()->check() or Auth::director()->check())
        {
            $user = $this->getCurrentUser();

            if(count($user->salons))
            {
                $salons = $user->salons;

                $client = Client::findOrFail($id);
                foreach ($salons as $salon)
                {
                    if($salon->id == $client->salon_id)
                    {
                        // Здесь проходит удаление
                        return $this->clientRepository->delete($id);
                    }
                }
                return redirect()->to('/panel/clients');
            }
            else
            {
                return redirect()->to('/panel/clients');
            }
        }
        else
        {
            return redirect()->to('/panel/clients');
        }
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     * @param $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnSuccess($message, $url = '/panel/clients')
    {
        Flash::success($message);

        return redirect()->to($url);
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     *
     * @param $message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }

    /**
     * Профиль клиента
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getProfile($id)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        $client = Client::findOrFail($id);

        $salons = $user->salons;

        $clientTransactions = Transaction::forSalon($salon_id)->where('client_id', $client->id);

        $transactions = $clientTransactions->get();

        $profileInfo = new \stdClass();
        $profileInfo->services = $clientTransactions->count();
        $profileInfo->amount = $clientTransactions->sum('price');
        $profileInfo->favoriteService = 0;//$clientTransactions->sum('price');

        return view("panel.clients.profile.index", compact('user', 'client', 'salons', 'transactions', 'profileInfo'));
    }
}
