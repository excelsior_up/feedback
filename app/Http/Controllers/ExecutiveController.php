<?php

namespace App\Http\Controllers;

use App\Executive;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salon;
use Mail;
use Auth;

class ExecutiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.exe', ['except' => ['getLogin', 'postLogin']]);
    }

    public function getIndex()
    {
        $exe = Auth::exe()->get();

        $salons = Salon::latest()->orderBy('isApproved', 'DESC')->get();

        return view('exe.index', compact('exe', 'salons'));
    }

    public function getApprove($id)
    {
        $salon = Salon::findOrFail($id);

        $salon->isApproved = 1;
        $salon->approved_at = \Carbon\Carbon::now();
        $salon->save();

        Mail::send('emails.salon_approved', ['receiver' => $salon->director, 'salon' => $salon], function($message) use ($salon) {
            $message->to($salon->director->email)->subject('Салон подтвержден!');
        });

        return redirect()->back();
    }

    public function getRemove($id)
    {
        $salon = Salon::findOrFail($id);

        $salon->isApproved = NULL;
        $salon->save();

        Mail::send('emails.salon_unapproved', ['receiver' => $salon->director, 'salon' => $salon], function($message) use ($salon) {
            $message->to($salon->director->email)->subject('Салон отключен!');
        });

        return redirect()->back();
    }

    public function getLogin()
    {
        return view('exe.login');
    }


    public function tryLogin($role, Request $request)
    {
        // Попытка авторизации с помощью данных формы
        $remember = $request->input('remember_me') == 'on' ? true : false;

        return Auth::$role()->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ], $remember);
    }


    public function postLogin(Request $request)
    {
        if (Auth::exe()->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]))
        {
            return redirect()->to('/exe');
        }
       return redirect()->back()->withInput();
    }

    public function getLogout()
    {
        Auth::exe()->logout();

        return redirect('/');
    }
}
