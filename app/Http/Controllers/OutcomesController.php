<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salon, App\Outcome;
use Flash;

use App\Transaction;
use App\Repositories\OutcomeRepository;

class OutcomesController extends Controller
{
    protected $outcomeRepository;

    public function __construct()
    {
        $this->outcomeRepository = new OutcomeRepository($this);
    }

    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $outcomes = $user->salons->first()->outcomes->take(100);

        return view('panel.outcomes.index', compact('user', 'outcomes'));
    }


    /**
     * Добавление затраты
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();

        return $this->outcomeRepository->create($request, $user);
    }

    /**
     * Удаление (отсутствует в представлениях)
     *
     */
    public function postDelete($id)
    {
        //return $this->outcomeRepository->delete($id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     */
    public function returnSuccess($message, $url = '/panel/outcomes')
    {
        Flash::success($message);

        return redirect()->to($url);
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     *
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }
}
