<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salon;

use Flash;

use App\Repositories\AdminRepository;

class AdminsController extends Controller
{
    protected $adminRepository;

    public function __construct()
    {
        $this->adminRepository = new AdminRepository($this);
    }

    /****
     * Список всех администраторов
     *
     */
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.admins.index", compact('user', 'salons'));
    }

    /****
     * Страница добавления администратора
     *
     */
    public function getAdd()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.admins.add", compact('user', 'salons'));
    }

    /****
     * Функция добавления администратора
     *
     */
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();

        return $this->adminRepository->create($request, $user->id);
    }

    /****
     * Страница редактирования администратора
     *
     */
    public function getEdit($id)
    {
        $user = $this->getCurrentUser();

        if(count($user->salons))
        {
            $salons = $user->salons;

            $admin = Admin::findOrFail($id);
            foreach ($salons as $salon)
            {
                if($salon->id == $admin->salon_id)
                {
                    return view("panel.admins.edit",
                        ['user' => $user,
                        'salons' => $salons,
                        'admin' => $admin,
                        'hide_password' => true,
                        'hide_salon' => true,]
                    );
                }
            }
            return redirect()->to('/panel/admins');
        }
        else
        {
            return redirect()->to('/panel/admins');
        }
    }

    /****
     * Функция редактирования администратора
     *
     */
     public function postEdit($id, Request $request)
     {
         $user = $this->getCurrentUser();

         if(count($user->salons))
         {
             $salons = $user->salons;

             $admin = Admin::findOrFail($id);
             foreach ($salons as $salon)
             {
                 if($salon->id == $admin->salon_id)
                 {
                     return $this->adminRepository->edit($id, $request);
                 }
             }
             return redirect()->to('/panel/admins');
         }
         else
         {
             return redirect()->to('/panel/admins');
         }
     }


    /****
     * Функция удаления администратора
     *
     */
    public function postDelete($id)
    {
        return $this->adminRepository->delete($id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     */
    public function returnSuccess($message)
    {
        Flash::success($message);
        return redirect()->to('/panel/admins');
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     *
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }
}
