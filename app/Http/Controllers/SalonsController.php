<?php

namespace App\Http\Controllers;

use App\Salon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Subscription;
use Flash;

use App\Repositories\SalonRepository;

class SalonsController extends Controller
{
    protected $salonRepository;

    public function __construct()
    {
        $this->salonRepository = new SalonRepository($this);
    }

    /**
     * Страница салонов
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.salons.index", compact('user', 'salons'));

    }

    /**
     * Функция для создания салона - вид
     *
     * @return \Illuminate\View\View
     */
    public function getAdd()
    {
        $user = $this->getCurrentUser();

        if ($user->salons->count()) return redirect()->to('/panel/salons/edit/' . $user->salons->first()->id);

        $subscriptions = Subscription::lists('name', 'id');

        return view('panel.salons.add', compact('user', 'subscriptions', 'categories'));
    }

    /**
     * Создание салона с помощью репозитория
     *
     * @param Request $request
     * @internal param SalonRepository $salon
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();



        if ($user->salons->count()) return redirect()->to('/panel');

        return $this->salonRepository->create($request, $user->id);
    }

    public function getAddServices($salon_id = null,$message="Салон успешно добавлен. Для завершения процесса добавления, выберите услуги, которые предоставляет ваш салон.")
    {

        $user = $this->getCurrentUser();


        $user_salon = $user->salons->first();

        if($salon_id)
        {

            // some action in case of adding > 1 salon
        }
        else {
            $salon_id = $user_salon->id;
        }

        // if ((!$from)) return redirect('panel/salons');

        Flash::success($message);

        $categories = Category::all();



        return view('panel.salons.add_services', compact('user', 'categories', 'salon_id'));
    }

    public function postAddServices(Request $request)
    {
        $user = $this->getCurrentUser();

        $user_salons = $user->salons;

        return $this->salonRepository->addServices($request->input('salon_id'), $request->input('service_ids'));
    }

    public function getEdit($id)
    {
        $user = $this->getCurrentUser();

        $salon = Salon::find($id);

        $salon_services = $salon->services->lists('service_id')->toArray();

        $subscriptions = Subscription::lists('name', 'id');

        $categories = Category::with('subcategories')->get();

        return view('panel.salons.edit', compact('user', 'salon', 'salon_services', 'subscriptions', 'categories'));
    }

    public function postEdit($id, Request $request)
    {
        $user = $this->getCurrentUser();

        return $this->salonRepository->edit($id, $request);
    }

    /**
     * Удаление салона
     *
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        return $this->salonRepository->delete($id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     * @param $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnSuccess($message)
    {
        $user = $this->getCurrentUser();

        Flash::success($message);

        return redirect()->to('/panel/salons/edit/' . $user->salons->first()->id);
    }

    /**
     * Используем эту функцию для возвращения неуспешного сообщения
     *
     * @param $message
     * @param Request|NULL $request
     * @return $this
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect ()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }
}
