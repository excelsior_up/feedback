<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salon, App\Master;
use Flash;

use App\Transaction;
use App\Repositories\MasterRepository;

class MastersController extends Controller
{
    protected $masterRepository;

    public function __construct()
    {
        $this->masterRepository = new MasterRepository($this);
    }

    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view('panel.masters.index', compact('user', 'salons'));
    }

    /**
     * Добавление мастера
     *
     */
    public function getAdd()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.masters.add", compact('user', 'salons'));
    }

    /**
     * Добавление мастера
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        return $this->masterRepository->create($request, $salon_id);
    }

    public function postEdit($id, Request $request)
    {
        $user = $this->getCurrentUser();

        if(count($user->salons))
        {
            $salons = $user->salons;

            $master = Master::findOrFail($id);
            foreach ($salons as $salon)
            {
                if($salon->id == $master->salon_id)
                {
                    return $this->masterRepository->edit($id, $request);
                }
            }
            return redirect()->to('/panel/masters');
        }
        else
        {
            return redirect()->to('/panel/masters');
        }
    }

    public function postDelete($id)
    {
        return $this->masterRepository->delete($id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     * @param $message
     * @param string $url
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnSuccess($message, $url = '/panel/masters')
    {
        Flash::success($message);

        return redirect()->to($url);
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     * @param $message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }

    /****
     * Профиль Клиента
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getProfile($id)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        $master =  Master::findOrFail($id);

        $salons = $user->salons;

        $masterTransactions = Transaction::forSalon($salon_id)->where('master_id', $master->id);

        $transactions = $masterTransactions->get();

        $profileInfo = new \stdClass();
        $profileInfo->services = $masterTransactions->count();
        $profileInfo->amount = $masterTransactions->sum('price');
        $profileInfo->favoriteService = 0;//$masterTransactions->sum('price');

        return view("panel.masters.profile.index", compact('user','master','salons', 'transactions', 'profileInfo'));
    }
}
