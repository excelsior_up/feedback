<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\ImageHelper;

use Storage;
use Auth;
use Session;
use Flash;
use Mail;

use App\Director;

class DirectorController extends Controller
{
    /**
     * Проверка на авторизацию
     *
     */
    public function __construct()
	{
		$this->middleware('auth.director', [
			'except' => [ /* 'getLogin', */ ]
		]);
	}

    /**
     * Изменение пароля Владельца (Директора)
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPassword($id, Request $request)
    {
        $director = Director::findOrFail($id);

        $old  = $request->input("oldpassword");
        $new1 = $request->input("password");
        $new2 = $request->input("repassword");

        if( ! \Hash::check($old, $director->password)){
            Session::flash('err_msg', 'Неверный пароль!');
            Session::flash('err_dsc', 'Убедитесь, что вы ввели верный старый пароль!');
            return redirect()->back();
        }

        if($new1 != $new2){
            Session::flash('err_msg', 'Пароли не совпадают!');
            Session::flash('err_dsc', 'Убедитесь, что новый пароль и подтверждение совпадают!');
            return redirect()->back();
        }

        $director->password = bcrypt($new1);
        $director->save();

        $user = $director;
        $email = $director->email;

        if( !($user->settings))
        {
            Mail::send('emails.profile_change_password', compact('user'), function($message) use ($email) {
                $message->to($email)->subject('Пароль изменен');
            });
        } else {
            if($user->settings->add_profile_password_mail == 1)
            {
                Mail::send('emails.profile_change_password', compact('user'), function($message) use ($email) {
                    $message->to($email)->subject('Пароль изменен');
                });
            }
        }

        Session::flash('suc_msg', 'Пароль успешно изменен!');
        Session::flash('suc_dsc', 'Теперь входите в систему с новым паролем!');
        return redirect()->back();
    }

    /**
     * Изменение данных профиля
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request)
    {
        $user = $this->getCurrentUser();

        $user->surname = $request->input('surname');
        $user->name    = $request->input('name');
        $user->email   = $request->input('email');

        // Аватар
        if($request->hasFile('avatar'))
        {
            if ($user->avatar)
            {
                Storage::disk('local_public')->delete($user->avatar);
            }

            $user->avatar = ImageHelper::save($request->file('avatar'), 'directors');
        }

        $user->save();

        return redirect()->back();
    }

    /**
     * Изменение Настроек пользователя
     *
     */
    public function postSettings(Request $request)
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        if (! $user->settings){
            $settings = new \App\Settings;
            $settings->director_id = $user->id;
            $settings->save();
        }

        $settings = $user->settings;

        if($request->input('add_client_mail'))
            {$settings->add_client_mail = 1;} else {$settings->add_client_mail = 0;}
        if($request->input('add_outcome_mail'))
            {$settings->add_outcome_mail = 1;} else {$settings->add_outcome_mail = 0;}
        if($request->input('add_transaction_mail'))
            {$settings->add_transaction_mail = 1;} else {$settings->add_transaction_mail = 0;}
        if($request->input('add_profile_password_mail'))
            {$settings->add_profile_password_mail = 1;} else {$settings->add_profile_password_mail = 0;}
        if($request->input('add_master_mail'))
            {$settings->add_master_mail = 1;} else {$settings->add_master_mail = 0;}

        $settings->save();

        return redirect()->back();
    }

}
