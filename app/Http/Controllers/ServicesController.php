<?php

namespace App\Http\Controllers;

use App\Service;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use Flash;

class ServicesController extends Controller
{
    public function __construct()
    {

    }

    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salon = $user->salons->first();

        $salon_services = [];

        foreach($salon->services as $salon_service)
        {
            $salon_services[] = $salon_service->service->id;
        }

        $categories = Category::all();

        return view('panel.services.index', compact('user', 'salon', 'categories', 'salon_services'));
    }

    public function postIndex(Request $request)
    {
        $user = $this->getCurrentUser();

        $salon = $user->salons->first();

        $salon->services()->delete();

        foreach($request->input('service_ids') as $service_id)
        {
            $salon->services()->create(['service_id' => $service_id]);
        }

        Flash::success('Услуги успешно выбраны!');

        return redirect('/panel/services');
    }

    // For angular request

    public  function getSubcategories()
    {

        $categories = Category::all();
        $subCategories = Subcategory::all();
        $services  = Service::all();

        $categoryEntities = array();

        foreach($categories as $category)
        {
            $categoryEntity['category_id'] = $category->id;
            $categoryEntity['category_name'] = $category->name;

            $subCategoryEntities = array();

            foreach($category->subcategories as $subCategory)
            {
                $subCategoryEntity['subCategory_id'] = $subCategory->id;
                $subCategoryEntity['subCategory_name'] = $subCategory->name;

                $serviceEntities = array();

                foreach($subCategory->services as $service)
                {
                    $serviceEntity['id'] = $service->id;
                    $serviceEntity['name'] = $service->name;

                    array_push($serviceEntities,$serviceEntity);
                }

                $subCategoryEntity['services'] = $serviceEntities;

                array_push($subCategoryEntities,$subCategoryEntity);
            }

            $categoryEntity['subCategories'] = $subCategoryEntities;


            array_push($categoryEntities,$categoryEntity);



        }

        return response()->json($categoryEntities);
    }

}
