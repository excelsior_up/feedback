<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cache;
use Hash;
class MainController extends Controller
{
    public function __construct()
    {

    }

    public function getIndex()
    {
        return view('layouts.landing');
    }

    public function khan() {

    	$user = $this->getCurrentUser();

    	return view('panel.khan.index', compact('user'));
    }

    public function test() {
    	return 'this works';
    }

    public function getHash(Request $request)
    {
       dd ( bcrypt($request->value));
    }


}
