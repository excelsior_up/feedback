<?php

namespace App\Http\Controllers;

use App\Manager;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salon;

use Flash;

use App\Repositories\ManagerRepository;

class ManagersController extends Controller
{
    protected $managerRepository;

    public function __construct()
    {
        $this->managerRepository = new ManagerRepository($this);
    }

    /****
     * Список всех менеджеров
     *
     */
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.managers.index", compact('user', 'salons'));
    }

    /****
     * Страница добавления менеджера
     *
     */
    public function getAdd()
    {
        $user = $this->getCurrentUser();

        $salons = $user->salons;

        return view("panel.managers.add", compact('user', 'salons'));
    }

    /****
     * Функция редактирования менеджера
     *
     */
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();

        return $this->managerRepository->create($request, $user->id);
    }

    /****
     * Страница редактирования менеджера
     *
     */
    public function getEdit($id)
    {
        $user = $this->getCurrentUser();

        if(count($user->salons))
        {
            $salons = $user->salons;

            $manager = Manager::findOrFail($id);
            foreach ($salons as $salon)
            {
                if($salon->id == $manager->salon_id)
                {
                    return view("panel.managers.edit",
                        ['user' => $user,
                        'salons' => $salons,
                        'manager' => $manager,
                        'hide_password' => true,
                        'hide_salon' => true,]
                    );
                }
            }
            return redirect()->to('/panel/managers');
        }
        else
        {
            return redirect()->to('/panel/managers');
        }
    }

    /****
     * Функция редактирования менеджера
     *
     */
     public function postEdit($id, Request $request)
     {
         $user = $this->getCurrentUser();

         if(count($user->salons))
         {
             $salons = $user->salons;

             $manager = Manager::findOrFail($id);
             foreach ($salons as $salon)
             {
                 if($salon->id == $manager->salon_id)
                 {
                     return $this->managerRepository->edit($id, $request);
                 }
             }
             return redirect()->to('/panel/managers');
         }
         else
         {
             return redirect()->to('/panel/managers');
         }
     }


    /****
     * Функция удаления менеджера
     *
     */
    public function postDelete($id)
    {
        return $this->managerRepository->delete($id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     */
    public function returnSuccess($message)
    {
        Flash::success($message);
        return redirect()->to('/panel/managers');
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     *
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }
}
