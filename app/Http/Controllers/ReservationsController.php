<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reservation;
use Flash;

class ReservationsController extends Controller
{
    //
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $masters = $user->salons->first()->masters;
        $clientsCount = $user->salons->first()->clients->count();
        $mastersCount = $masters->lists('id');


        $current_date = Carbon::now()->format('Y-m-d');

        return view('panel.table.day_masters', compact('user', 'masters', 'mastersCount','current_date','clientsCount'));
    }

    public function postAdd(Request $request)
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        // echo '<pre>', var_dump($request->all()), '</pre>'; die;

        // Проверка на ввод данных в полях
        if ( ! $request->input('service_id') ||
             ! $request->input('client_id')  ||
             ! $request->input('master_id'))
        {
            Flash::error('Недостаточно данных!');
            return redirect()->back()->withInput();
        }

        $reservation = new Reservation;
        // Информация о контенте
        $reservation->salon_id   = $user->salons->first()->id;
        $reservation->service_id = $request->input('service_id');
        $reservation->client_id  = $request->input('client_id');
        $reservation->master_id  = $request->input('master_id');

        // Информация о времени
        $startHour    = str_pad($request->input('start_h'), 2, 0, STR_PAD_LEFT);
        $startMinute  = str_pad($request->input('start_m'), 2, 0, STR_PAD_LEFT);
        $duration     = (int)$request->input('duration');

        //$start  = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reservation->start);
        $date  = $request->input('start_date');

        // Определение стартового времени резервации
        $outDate = $date . ' ' . $startHour . ':' . $startMinute . ':00';
        $reservation->start = $outDate;

        // Определение конечного времени резервации
        // посредством добавления длительности к стартовому времени
        $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $reservation->start);
        $end->addMinutes($duration);

        $reservation->end = $end;

        // Возможные помехи - (сегодня)
        $troubles = Reservation::where('master_id', '=', $reservation->master_id)
            ->where('start', '>', \Carbon\Carbon::now()->startOfDay())
            ->where('start', '<', \Carbon\Carbon::now()->endOfDay());

        // Предрасположенное вхождение в создание
        $tr1 = $troubles->where('start', '>=', $reservation->start)->where('start', '<=', $reservation->end)->get();
        if(count($tr1)){
            Flash::success('Промежуток занят! вход');
            return redirect()->back();
        }
        // var_dump($tr1); die;
        // Предрасположенный выход из создания
        $tr1 = $troubles->where('end', '>=', $reservation->start)->where('end', '<=', $reservation->end)->get();
        if(count($tr1)){
            Flash::success('Промежуток занят! выход');
            return redirect()->back();
        }

        // Поглощение создания
        $tr1 = $troubles->where('start', '<=', $reservation->start)->where('end', '>=', $reservation->end)->get();
        if(count($tr1)){
            Flash::success('Промежуток занят!');
            return redirect()->back();
        }

        // Замещение создания
        $tr1 = $troubles->where('start', '>=', $reservation->start)->where('end', '<=', $reservation->end)->get();
        if(count($tr1)){
            Flash::success('Промежуток занят!');
            return redirect()->back();
        }

        $reservation->save();

        return redirect()->back();
    }

}
