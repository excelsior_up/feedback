<?php

namespace App\Http\Controllers;

use Flash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\TransactionRepository;

class TransactionsController extends Controller
{
    private $transactionRepository;

    public function __construct()
    {
        $this->transactionRepository = new TransactionRepository($this);
    }

    public function getIndex()
    {

    }

    /**
     * Сохраняем новую транзакцию
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postNew(Request $request)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        return $this->transactionRepository->create($request, $salon_id);
    }

    /**
     * Используем эту функцию для возвращения успешного сообщения
     *
     * @param $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnSuccess($message)
    {
        Flash::success($message);

        return redirect()->to('/panel');
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     *
     * @param $message
     * @param Request|NULL $request
     * @return $this
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }
}
