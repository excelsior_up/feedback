<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Flash;
use Auth;
use Mail;
use App\Director;
use App\PasswordReset;
use Twilio\Rest\Client;

use App\Helpers\ValidationHelper;

class AuthCommonController extends Controller
{
    public function __construct()
    {

    }

    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Авторизация
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->kickAllUsers();

        $isLoginSuccess = $this->tryLogin('director', $request);

//        switch($request->input('role'))
//        {
//            case 'director':
//                $isLoginSuccess = $this->tryLogin('director', $request);
//                break;
//
//            case 'executive':
//                $isLoginSuccess = $this->tryLogin('executive', $request);
//                break;
//
//            case 'admin':
//                $isLoginSuccess = $this->tryLogin('admin', $request);
//                break;
//
//            case 'manager':
//                $isLoginSuccess = $this->tryLogin('manager', $request);
//                break;
//
//            case 'master':
//                $isLoginSuccess = $this->tryLogin('master', $request);
//                break;
//
//            default:
//                return redirect()->back()->withInput();
//                break;
//        }

        if ($isLoginSuccess)
        {
            // Успешная авторизация
            return redirect()->to("/panel");
        }
        else
        {
            // Ошибка - вовращение на предыдущую страницу
            Flash::error('Неправильные данные для входа');

            return redirect()->back()->withInput();
        }
    }

    public function tryLogin($role, Request $request)
    {
        // Попытка авторизации с помощью данных формы
        $remember = $request->input('remember_me') == 'on' ? true : false;


        return Auth::$role()->attempt([
            'phone' => $request->input('phone'),
            'password' => $request->input('password')
        ], $remember);
    }

    /**
     * Страница восстановления пароля - отправка инструкции на почту
     *
     */
    public function getResetpassword()
    {
        return view('auth.password_reset');
    }

    /**
     * Функция восстановления пароля - отправка инструкции на почту
     *
     */
    public function postReset(Request $request)
    {
        // Соль
        $salt = str_random(24);

        // Внесенные данные о почте в формы
        $email = $request->input('email');

        // Пользователь с введенным email
        $user = Director::where('email', $email)->first();

        // Существует ли пользователь с таким email?
        if(! $user)
        {
            Flash::error('Пользователя с таким email не существует!');
            return redirect()->back()->withInput();
        }

        // Создание объекта восстановления пароля
        $passwordReset = new PasswordReset;
        $passwordReset->email    = $email;
        $passwordReset->role     = 'director';

        // Уникальная соль для объекта восстановления
        while(true)
        {
            $salt = str_random(24);
            $passwordReset->salt = $salt;
            $resetModel = PasswordReset::where('salt', $passwordReset->salt)->first();
            if (! $resetModel) {
                break;
            }
        }

        // Неиспользованная ссылка
        $passwordReset->used = 0;
        // Сохранение объекта восстановления
        $passwordReset->save();

        // Генерация ссылки подтверждения восстановления
        $resetLink = url('/auth/refresh/' . $salt);

        // Отправка почты
        Mail::send('emails.password_reset', compact('user', 'resetLink'), function($message) use ($email) {
            $message->to($email)->subject('Восстановление пароля');
        });

        // Успешная отправка инструкции
        Flash::success('Инструкция по восстанолению пароля успешно отправлена на '.$email);
        return redirect()->to('/auth/login');
    }

    /**
     * Страница восстановления пароля - создание нового пароля
     *
     */
    public function getRefresh($unique)
    {
        // Выбор неиспользованной модели восстановления пароля с уникальной солью
        $resetModel = PasswordReset::where('used', 0)->where('salt', $unique)->first();

        // Существует ли выбранная модель?
        if(! $resetModel){
            Flash::error('Неправильная ссылка или ссылка уже была использована');
            return redirect()->to('/auth/login');
        }

        // Представление с формой ввода пароля
        return view('auth.password_refresh', ['salt' => $unique, 'email' => $resetModel->email]);
    }

    /**
     * Функция восстановления пароля - создание нового пароля
     *
     */
    public function postRefresh($unique, Request $request)
    {
        // Введенные в форме пароли
        $password_1 = $request->input('password_1');
        $password_2 = $request->input('password_2');

        // Заполнены все поля?
        if ($password_1 == "" or $password_2 == "")
        {
            Flash::error('Заполните все поля!');
            return redirect()->back()->withInput();
        }

        // Введенные пароли совпадают?
        if ($password_1 != $password_2)
        {
            Flash::error('Пароли не совпадают!');
            return redirect()->back()->withInput();
        }

        // Введенный пароль имеет длину >= 6 символов?
        if ( strlen($password_1) < 6 or strlen($password_2) < 6 )
        {
            Flash::error('Длина пароля - не менее 6 символов!');
            return redirect()->back()->withInput();
        }

        // Выбор неиспользованной модели восстановления пароля с уникальной солью
        $resetModel = PasswordReset::where('used', 0)->where('salt', $unique)->first();

        // Существует ли модель?
        if($resetModel){
            // Использовано
            $resetModel->used = 1;
            // Выбор пользователя по почте в модели восстановления
            $user = Director::where('email', $resetModel->email)->first();
            // Обновление пароля в модели и у пользователя
            $user->password       = bcrypt($password_1);
            $resetModel->password = bcrypt($password_1);
            // Сохранение модели и пользователя
            $user->save();
            $resetModel->save();
        } else {
            // Ошибка - использовано или неправильная ссылка
            Flash::error('Неправильная ссылка или ссылка уже была использована');
            return redirect()->to('/auth/login');
        }

        // Успешная смена пароля
        Flash::success('Пароль успешно восстановлен!');
        return redirect()->to('/auth/login');
    }

    /**
     * Регистрация нового владельца
     *
     */
    public function getRegister()
    {
        return view('auth.registration');
    }

    /*
     * Регистрация нового владельца
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postRegister(Request $request)
    {
        $validation = ValidationHelper::make($request->all(), Director::$rules);

        if ($validation->failed)
        {
            Flash::error($validation->error);
            return redirect()->back()->withInput();
        }

        // Добавление владельца в базу
        $user = Director::create($request->all());
        $user->password = \Hash::make($request->input('password'));
        $user->save();

        $email = $user->email;
        $loginLink = url('/auth/login');

        // Your Account SID and Auth Token from twilio.com/console
        $account_sid = 'AC66811a44fa4ade05ab39d75880b44fb9';
        $auth_token = 'bd01a8d36aaef812452fac5849dc19ed';
        // In production, these should be environment variables. E.g.:
        // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

        // A Twilio number you own with SMS capabilities
        $twilio_number = "+17208970920";

        $phone = str_replace(array('-'), '', $user->phone);



        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
        // Where to send a text message (your cell phone?)
            $phone,
            array(
                'from' => $twilio_number,
                'body' => 'Dobro pozhalovat v klub NOZH! Vash login - '. $request->input('phone') .' parol - '. $request->input('password') .' Po vsem voprosam - +77088888877 Zhanna'
            )
        );

        Mail::send('emails.register', compact('user', 'loginLink'), function($message) use ($email) {
            $message->to($email)->subject('Регистрация на сайте');
        });

        return $this->postLogin($request);

    }

    public function getLogout()
    {
        $this->kickAllUsers();

        return redirect('/');
    }
}
