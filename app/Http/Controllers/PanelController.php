<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Client;
use App\SalonService;
use App\Outcome;
use Carbon\Carbon;
use JavaScript;
use Mail;
use Flash;
use App\Settings;

class PanelController extends Controller
{
    public function __construct()
	{
        return $this->middleware('hasSalon', ['only' => 'getIndex']);
	}

    /**
     * Профиль пользователя
     *
     */
    public function getProfile()
    {
        $user = $this->getCurrentUser();

        $settings = $user->settings;

        if ( ! $settings)
        {
            Settings::create([
                'director_id' => $user->id
            ]);

            $settings = $user->settings;
        }

        return view('panel.personal_info', compact('user', 'settings'));
    }

    /**
     * Главная страница - Панель управления
     *
     */
    public function getIndex()
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        $salonTransactions = Transaction::forSalon($salon_id);

        $todayTransactions = $salonTransactions->forToday();

        $latestTransactions = $salonTransactions->latest()->take(8);

        $transactions = $latestTransactions->get();

        $dashboard['today']['amountOfMoney']      = $todayTransactions->sum('price');
        $dashboard['today']['numberOfServices']   = $todayTransactions->count();
        $dashboard['today']['numberOfClients']    = $todayTransactions->groupBy('client_id')->count();

        $latestVisitors = $latestTransactions->groupBy('client_id')->get();

        // Выбор 8 клиентов с ближайшими днями рождения
        $upcomingBirthdays = $this->setBirthdays($salon_id, 8);

        // Выбор первых 6 оказанных услуг по популярности
        $popularServices   = ($this->mostPopularServices(6));

        // Мастеры, и прибыль, полученная от услуг, оказанных ими
        $mastersProfit     = $this->mastersProfit();

        // Прибыль на сегодня
        $todayProfit = $this->profitBlock($salon_id);

        // Последние 8 затрат
        $latestOutcomes = Outcome::forSalon($salon_id)->latest()->take(8)->get();

        $weekTransactions = $this->getWeekTransactions($salon_id);

        $weekIncome = $weekTransactions->incomes;
        $weekOutcome = $weekTransactions->outcomes;

        $weekProfit = [
            'income' => array_sum($weekIncome),
            'outcome' => array_sum($weekOutcome),
            'profit' => array_sum($weekIncome) - array_sum($weekOutcome)
        ];

        JavaScript::put(compact('weekIncome', 'weekOutcome', 'popularServices'));

        return view("panel.dashboard.index",
            compact(
                'user', 'dashboard', 'transactions',
                'latestVisitors', 'upcomingBirthdays',
                'popularServices', 'mastersProfit',
                'latestOutcomes', 'todayProfit', 'weekProfit'
            )
        );
    }

    /**
     * Отправка быстрого еmail с панели управления
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postSendEmail(Request $request)
    {
        $user = $this->getCurrentUser();

        $salon = $user->salons->first();

        $email = $request->input('email');

        if ( ! $email )
        {
            Flash::error('Получатель не определен!');

            return redirect()->back()->withInput();
        }

        $receiver = Client::findByEmail($email);

        if ( ! $receiver )
        {
            Flash::error('Получатель не определен!');

            return redirect()->back()->withInput();
        }

        $mailData = new \stdClass();
        $mailData->subject = $request->input('subject');
        $mailData->text = $request->input('text');

        Mail::send('emails.quick', compact('salon', 'receiver', 'mailData'), function($message) use ($request, $salon) {

            $message->to($request->email)->subject('Письмо от салона ' . $salon->name);

        });

        Flash::success('E-mail успешно отправлен!');

        return redirect()->to('/panel');
    }

    /**
     * Предстоящие $counter дней рождения клиентов
     * int $counter
     * @param $salon_id
     * @param $counter
     * @return array
     */
    private function setBirthdays($salon_id, $counter)
    {

        // Месяц и день текущей даты
        $todayMonth   = (int)Carbon::now()->format('m');
        $todayDay     = (int)Carbon::now()->format('d');

        // Массив возвращаемых клиентов
        $latestClients = [];

        // Сортировка по дню рождения
        // TODO выбрать не всех клиентов,
        // а только первых $counter, причем
        // отсортированных по дате (ТОЛЬКО месяц и день, год исключить)
        $clients = Client::forSalon($salon_id)->get();

        // Проход по клиентам
        foreach ($clients as $client)
        {

            // Прерывание при $counter клиентах
            if($counter == 0)
            {
                break;
            }

            // Месяц и день рождения текущего клиента
            $month = Carbon::createFromFormat('Y-m-d',$client->birthday)->format('m');
            $day   = Carbon::createFromFormat('Y-m-d',$client->birthday)->format('d');

            // Проверка - месяц рождения клиента не январь
            if($month != 1)
            {
                // Если месяц рождения следующий после текущего
                if($todayMonth == $month - 1)
                {
                    // Добавление клиента в массив
                    $latestClients[] = $client;
                    // Уменьшение счетчика
                    // (для ограничения количества отобранных клиентов)
                    $counter = $counter - 1;
                }
                // Если месяц рождения совпадает
                elseif($todayMonth == $month)
                {
                    // Проверка, не прошел ли этот день
                    if($day >= $todayDay)
                    {
                        // Добавление клиента в массив
                        $latestClients[] = $client;
                        // Уменьшение счетчика
                        // (для ограничения количества отобранных клиентов)
                        $counter = $counter - 1;
                    }
                }
            }
            else // Месяц - январь
            {
                // Для января другой алгоритм
                // День рождения в январе (следующий год) - сейчас декабрь
                if ($todayMonth == 12)
                {
                    // Добавление клиента в массив
                    $latestClients[] = $client;
                    // Уменьшение счетчика
                    // (для ограничения количества отобранных клиентов)
                    $counter = $counter - 1;
                }
                // День рождения в этом же месяце (январь)
                if ($todayMonth == 1)
                {
                    // Проверка, не прошел ли этот день
                    if($day >= $todayDay)
                    {
                        // Добавление клиента в массив
                        $latestClients[] = $client;
                        // Уменьшение счетчика
                        // (для ограничения количества отобранных клиентов)
                        $counter = $counter - 1;
                    }
                }
            }
        }

        // Возвращение массива клиентов с выбранными днями рождения
        return $latestClients;
    }

    /**
     * Самые популярные $counter услуг(и)
     * int $counter
     * @return array
     * @internal param $counter
     */
    private function mostPopularServices($counter)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        // Выбор всех оказанных услуг
        $transactions = Transaction::forSalon($salon_id)->forMonth()->get();

        // Объявление массива популярности оказанных услуг
        $popularArray = [];

        // Проверка на наличие транзацкций --> Sanzhar

        if(SalonService::all()->count() > 0 ) {

            // Проход по оказанным услугам
            foreach ($transactions as $transaction) {

                // Определение ключа - названия услуги
                $caption = SalonService::where('id', $transaction->service_id)->first()->service->name;

                // Проверка - есть ли уже элемент в массиве
                // по индексации id услуги
                if (!isset($popularArray[$caption])) {
                    // Ели нет, то добавить новый элемент:
                    // массив[ название_услуги ] = количество_выполнений
                    $popularArray[$caption] = 1;
                } else {
                    // если уже имеется, то увеличить
                    // количество выполнений на 1
                    $popularArray[$caption] += 1;
                }
            }

            // Объявляется массив возвращения
            $reverseArray = [];

            // Массив возвращения заполняется путем добавления
            // элементов, обратных по ключу и значению массиву популярности
            foreach ($popularArray as $key => $value) {
                $reverseArray[] = [$value, $key];
            }

            // Сортировка массива по возрастанию
            // и его реверс (по убыванию)
            sort($reverseArray);
            $reverseArray = array_reverse($reverseArray);

            // Обнуление массива популярности для
            // его заполнения только $counter элементами
            $popularArray = [];
            for ($i = 0; $i < count($reverseArray); $i++) {
                // Имеется ли $i элемент в массиве?
                if (isset($reverseArray[$i])) {
                    // Превышен ли лимит в 6 услуг?
                    if ($i > 5) {
                        // Если да, то последний элемент
                        // будет называться "Другие" и будет
                        // содержать сумму популярности всех
                        // оставшихся услуг
                        $popularArray[5][0] += $reverseArray[$i][0];
                        $popularArray[5][1] = "Другие";
                    } else {
                        // А если не превышен, то
                        // просто дописать
                        $popularArray[] = $reverseArray[$i];
                    }
                } else {
                    // Завершить
                    break;
                }
            }

        }

        // Возвращение массива популярности оказанных услуг
        return $popularArray;
    }

    /**
     * Доходы по мастерам
     *
     */
    private function mastersProfit()
    {
        // Выбор теукщего пользователя
        $user = $this->getCurrentUser();

        // Текущий месяц и год
        $todayMonth = Carbon::now()->format('m');
        $todayYear  = Carbon::now()->format('Y');

        // Оказанные услуги
        $transactions = Transaction::all();

        // Массив мастеров и доходов
        $profitMasterArray = [];

        // Проход по мастерам каждого салона
        foreach ($user->salons->first()->masters as $master)
        {

            // Прибыль, полученная от работы текущего мастера
            $profit = 0;

            // Проход по оказанным услугам
            foreach ($transactions as $transaction)
            {

                // Если услуга оказана текущим мастером
                if($transaction->master_id == $master->id)
                {
                    // Месяц и год текущей оказанной услуги
                    $month = $transaction->created_at->format('m');
                    $year  = $transaction->created_at->format('Y');

                    // Проверка - услуга оказана в этом месяце этого года
                    if ($todayMonth == $month and $todayYear == $year)
                    {
                        // Увеличение суммы на величину стоимости услуги
                        $profit += $transaction->price;
                    }

                }

            }

            // Запись мастера в массив
            $profitMasterArray[] = [
                $profit, $master
            ];

        }

        // Сортировка по убыванию
        sort($profitMasterArray);
        $profitMasterArray = array_reverse($profitMasterArray);

        // Возвращение массива
        return $profitMasterArray;
     }

    /**
     * Прибыль
     * @param $salon_id
     * @return array
     */
    private function profitBlock($salon_id)
    {
        // Текущая дата
        $todayDay   = Carbon::now()->format('d');
        $todayMonth = Carbon::now()->format('m');
        $todayYear  = Carbon::now()->format('Y');

        // Оказанные услуги и затраты
        $transactions = Transaction::forSalon($salon_id)->get();
        $outcomes     = Outcome::forSalon($salon_id)->get();

        // Общий плюс доходов
        $allPlus = 0;

        foreach ($transactions as $transaction)
        {
            // Дата оказания текущей услуги
            $day   = $transaction->created_at->format('d');
            $month = $transaction->created_at->format('m');
            $year  = $transaction->created_at->format('Y');

            // Если услуга оказана в этот день (сегодня)
            if ($todayDay == $day and $todayMonth == $month and $todayYear == $year)
            {
                // Добавление к сумме доходов
                $allPlus += $transaction->price;
            }
        }

        // Общий минус расходов
        $allMinus = 0;

        foreach ($outcomes as $outcome)
        {
            // Дата фиксирования текущей затраты
            $day   = $outcome->created_at->format('d');
            $month = $outcome->created_at->format('m');
            $year  = $outcome->created_at->format('Y');

            // Если затрата совершена в этот день (сегодня)
            if ($todayDay == $day and $todayMonth == $month and $todayYear == $year)
            {
                // Добавление к минусу расходов
                $allMinus += $outcome->price;
            }
        }

        return [$allPlus, $allMinus, $allPlus - $allMinus];
    }

    /**
     * Страница "Давайте добавим салон"
     * @return \Illuminate\View\View
     */
    public function getGettingStarted()
    {
        $user = $this->getCurrentUser();

        return view('panel.getting_started', compact('user'));
    }

    /**
     * Страница "Подождите, пока одобрим"
     *
     * @return \Illuminate\View\View
     */
    public function getLastStep()
    {
        $user = $this->getCurrentUser();

        return view('panel.last_step', compact('user'));
    }

    /**
     * Функция для высчитывания дохода и расхода на месяц
     * @param $salon_id
     * @return \stdClass
     */
    public function getWeekTransactions($salon_id)
    {
        $allIncomes = Transaction::forSalon($salon_id)->forWeek()->get();
        $allOutcomes = Outcome::forSalon($salon_id)->forWeek()->get();

        $result = new \stdClass();
        $result->incomes = [];
        $result->outcomes = [];

        foreach(range(0, 6) as $index)
        {
            $result->incomes[$index] = 0;
            $result->outcomes[$index] = 0;
        }

        $weekDays = ['isMonday', 'isTuesday', 'isWednesday', 'isThursday', 'isFriday', 'isSaturday', 'isSunday'];

        foreach($allIncomes as $income)
        {
            foreach($weekDays as $dayNum => $day)
            {
                if($income->created_at->$day())
                {
                    $result->incomes[$dayNum] += $income->price;
                }
            }
        }

        foreach($allOutcomes as $outcome)
        {
            foreach($weekDays as $dayNum => $day)
            {
                if($outcome->created_at->$day())
                {
                    $result->outcomes[$dayNum] += $outcome->sum;
                }
            }
        }

        return $result;
    }
}
