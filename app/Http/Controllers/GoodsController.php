<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\GoodRepository;
use Flash;

use App\Good;

class GoodsController extends Controller
{
    protected $goodRepository;

    public function __construct()
    {
        $this->goodRepository = new GoodRepository($this);
    }
    /**
     * Список - Мой склад
     *
     */
    public function getIndex()
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        // Салон пользователя
        $salon = $user->salons->first();

        // Товары (расходный материал)
        $goods = $salon->goods;

        return view('panel.goods.index',compact('goods', 'user', 'salon'));
    }

    /**
     * Добавление Расходного материала
     *
     */
    public function getAdd()
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        // Салон пользователя
        $salon = $user->salons->first();

        return view('panel.goods.add',compact('user', 'salon'));
    }
    public function postAdd(Request $request)
    {
        $user = $this->getCurrentUser();

        $salon_id = $user->salons->first()->id;

        return $this->goodRepository->create($request, $user);
    }

    /**
     * Просмотр / редактирование / удаление расходного материала
     *
     */
    public function getInfo($id, Request $request)
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        // Салон пользователя
        $salon = $user->salons->first();

        $good = Good::findOrFail($id);
        if ($good->salon->id != $salon->id){
            return $this->returnFail('Ошибка!', $request);
        }

        return view('panel.goods.edit',compact('good', 'user', 'salon'));
    }
    public function postInfo($id, Request $request)
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        // Салон пользователя
        $salon = $user->salons->first();

        $good = Good::findOrFail($id);
        if ($good->salon->id != $salon->id){
            return $this->returnFail('Ошибка!', $request);
        }

        return $this->goodRepository->edit($id, $request);
    }

    /**
     * Удаление расходного материала
     *
     */
    public function postDelete($id)
    {
        // Текущий пользователь
        $user = $this->getCurrentUser();

        return $this->goodRepository->delete($id, $user);
    }



    /**
     * Используем эту функцию для возвращения успешного сообщения
     * @param $message
     * @param string $url
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnSuccess($message, $url = '/panel/goods')
    {
        Flash::success($message);

        return redirect()->to($url);
    }

    /**
     * Используем эту функцию для возвращения неуспшеного сообщения
     * @param $message
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnFail($message, Request $request = NULL)
    {
        Flash::error($message);

        $redirect = redirect()->back();
        $request ? $redirect->withInput($request->all()) : '';

        return $redirect;
    }

}
