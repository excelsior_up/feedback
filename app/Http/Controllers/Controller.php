<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Auth;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function kickAllUsers()
    {
        Auth::director()->logout();
        /*Auth::admin()->logout();
        Auth::manager()->logout();
        Auth::master()->logout();*/
    }

    public static function getCurrentUser()
    {
        if (Auth::director()->check())
            return Auth::director()->get();
        /*elseif(Auth::admin()->check())
            return Auth::admin()->get();
        elseif(Auth::manager()->check())
            return Auth::manager()->get();
        elseif(Auth::master()->check())
            return Auth::master()->get();*/
    }
}
