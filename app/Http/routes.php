<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('PHPExcel.php');
require_once('PHPExcel/Writer/Excel5.php');

use Twilio\Rest\Client;

use Maatwebsite\Excel\Collections\Collec;
use App\Exports\UsersExport;

Route::get('/excel', function () {

    $conn = mysqli_connect("localhost", "root", "", "feedback");
    $conn->set_charset("utf8");

    $query = "SELECT d.name as directorName, d.surname, d.phone, d.email, s.name, s.address FROM directors  as d JOIN salons as s ON d.id = s.director_id";

    $objPHPExcel = new PHPExcel();
//    PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
    $result = $conn->query($query);

    $fields = array('Имя', 'Фамилия', 'Телефон', 'Email', 'Наименование салона', 'Адрес салона');

    $rowCount = 3;
    $col = 1;
    foreach ($fields as $field) {

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $field);

        $styleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'size' => 11,
                'color' => array('rgb' => 'ffffff')
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3452ff')
            ),
        );

        $objPHPExcel->getActiveSheet()->getStyle('B3:G3')->applyFromArray($styleArray);

        $col++;


    }
    foreach (range('B', 'G') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }
    $objPHPExcel->getActiveSheet()->setTitle("База салонов");
    $rowCount = 4;
    foreach ($result as $key => $values) {
        $column = 'B';
        foreach ($values as $value) {
            $objPHPExcel->getActiveSheet()->setCellValue($column . $rowCount, $value);
            $column++;
        }
        $rowCount++;
    }


    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="База салонов.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
});

Route::get('/sms', function () {
    // Your Account SID and Auth Token from twilio.com/console
    $account_sid = 'AC66811a44fa4ade05ab39d75880b44fb9';
    $auth_token = 'bd01a8d36aaef812452fac5849dc19ed';
    // In production, these should be environment variables. E.g.:
    // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

    // A Twilio number you own with SMS capabilities
    $twilio_number = "+17208970920";

    $phone = str_replace(array('-'), '', ' +7-708-8888877');


    $client = new Client($account_sid, $auth_token);
    $client->messages->create(
    // Where to send a text message (your cell phone?)
        $phone,
        array(
            'from' => $twilio_number,
            'body' => 'Dobro pozhalovat v klub NOZH! Vash login - yourmail@mail.com parol - qwerty Po vsem voprosam - +77088888877 Zhanna'
        )
    );

});

Route::get('/mail', function () {
    $email = 'dsanjar@rocketmail.com';

    Mail::send('emails.register', [], function ($message) use ($email) {
        $message->to($email)->subject('Регистрация на сайте');
    });
    return view('emails.template');
});

// Владельцы бизнеса (Директоры)
Route::group(['prefix' => 'director'], function () {
    Route::controller('/', 'DirectorController');
});

Route::group(['prefix' => 'exe'], function () {

    Route::controller('/', 'ExecutiveController');

});


Route::group(['prefix' => 'panel'], function () {

    // Проверка на авторизацию в панель ( director | admin | manager | master )
    Route::group(['middleware' => 'auth.administration'], function () {

        Route::controller('/director', 'DirectorController');

        Route::get('/getting-started', 'PanelController@getGettingStarted');

        Route::get('/salons/add', 'SalonsController@getAdd');
        Route::post('/salons/add', 'SalonsController@postAdd');

        Route::group(['middleware' => 'hasSalon'], function () {

            Route::post('/salon/add-services/', 'SalonsController@postAddServices');
            Route::get('/salon/add-services/', 'SalonsController@getAddServices');

            /**** If director added services ****/

            Route::get('/last-step', 'PanelController@getLastStep');

            Route::group(['middleware' => ['addedServices', 'salonApproved']], function () {


                // Контроллер для салонов
                Route::controller('/salons', 'SalonsController');

                // Контроллер для мастеров
                Route::controller('/masters', 'MastersController');

                /*
                // Контроллер для менеджеров
                Route::controller('/managers', 'ManagersController');

                // Контроллер для администраторов
                Route::controller('/admins', 'AdminsController');
                */

                // Контроллер для затрат
                Route::controller('/outcomes', 'OutcomesController');

                // Контроллер для резерваций и журнала
                Route::controller('/table', 'ReservationsController');

                // Контроллер для клиентов
                Route::controller('/clients', 'ClientsController');

                // Контроллер для товаров (мой склад)
                Route::controller('/goods', 'GoodsController');

                // Контроллер для услуг салона
                Route::controller('/services', 'ServicesController');

                // Контроллер для транзакций салона
                Route::controller('/transactions', 'TransactionsController');


                // Основной контроллер панели
                Route::controller('/', 'PanelController');

            });

        });
    });
});

Route::group(['prefix' => 'auth'], function () {
    // Авторизация директоров (владельцев бизнеса)
    //Route::controller('director', 'Auth\AuthDirectorController');

    // Контроллер для общих методов
    Route::controller('/', 'Auth\AuthCommonController');
});

// khangeldy

Route::get('/hashIt/{value}', function ($value) {
    echo Hash::make($value);
});
// Главный контроллер (статичные страницы)
Route::controller('/', 'MainController');
