<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasSalonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::director()->get();

        if ( ! $user->salons->count())
        {
            return redirect()->to('/panel/getting-started');
        }

        return $next($request);
    }
}
