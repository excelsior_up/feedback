<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateAdministration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            ! ( Auth::director()->check() ||
            Auth::manager()->check() ||
            Auth::admin()->check() )
        )
        {
            return redirect()->intended('/auth/login');
        }

        return $next($request);
    }
}
