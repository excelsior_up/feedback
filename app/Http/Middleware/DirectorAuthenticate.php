<?php

namespace App\Http\Middleware;

use Closure;

class DirectorAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
	{
		if (\Auth::director()->guest())
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->action('Auth\AuthDirectorController@getLogin');
			}
		}

		return $next($request);
	}

}
