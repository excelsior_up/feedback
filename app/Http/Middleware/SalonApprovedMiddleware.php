<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SalonApprovedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::director()->get();
        if ( $user->salons->count() && ! $user->salons->first()->isApproved)
        {
            return redirect()->to('/panel/last-step');
        }

        return $next($request);
    }
}
