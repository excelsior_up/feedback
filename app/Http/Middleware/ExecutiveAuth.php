<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ExecutiveAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if ( !Auth::exe()->check())
       {
            return redirect('/exe/login');
       }
       return $next($request);
    }
}
