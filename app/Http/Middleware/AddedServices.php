<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AddedServices
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::director()->get();
        if ( ! $user->salons->first()->services->count() )
        {
            return redirect()->to('/panel/salon/add-services');
        }


        return $next($request);
    }
}
