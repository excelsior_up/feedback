<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    // Таблица базы данных
    protected $table = 'subscriptions';

    // Связь 1/1 с Салоном
    protected function salon()
    {
        return $this->hasOne('App\Salon');
    }

    // Связь 1/1 с Подробнее о подписке
    protected function subscriptionDetail()
    {
        return $this->hasOne('App\SubscriptionDetail');
    }
}
