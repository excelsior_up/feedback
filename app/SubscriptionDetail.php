<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionDetail extends Model
{
    // Таблица базы данных
    protected $table = 'subscription_details';

    // Связь 1/1 с Подпиской
    protected function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
}
