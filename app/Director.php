<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Director extends Model implements AuthenticatableContract,
                                     AuthorizableContract
{
    use Authenticatable, Authorizable;

    // Таблица базы данных
    protected $table = 'directors';

    public static $rules = [
        'name' => 'required',
        'surname' => 'required',
        'email' => 'required|unique:directors',
        'password' => 'required|min:6|confirmed'
    ];

    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
    ];

    //
    protected $hidden = ['password', 'remember_token'];

    // Связь 1/* с Салоном
    protected function salons()
    {
        return $this->hasMany('App\Salon')->latest();
    }

    protected function settings()
    {
        return $this->hasOne('App\Settings', 'director_id');
    }
}
