<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalonService extends Model
{
    protected $table = 'salon_services';

    protected $fillable = [
        'salon_id',
        'service_id',
    ];

    // Связь 1/n с Резервациями
    protected function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
}
