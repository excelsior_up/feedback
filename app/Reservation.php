<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Reservation extends Model
{
    // Таблица базы данных для Резерваций Журнала
    protected $table = 'reservations';

    // Поля для заполнения
    protected $fillable = [
        'salon_id',
        'master_id',
        'client_id',
        'service_id',
        'start',
        'end',
        'day'
    ];

    // Связь 1/1 с Салоном
    protected function salon()
    {
        return $this->belongsTo('App\Salon', 'salon_id');
    }

    // Связь 1/1 с Мастером
    protected function master()
    {
        return $this->belongsTo('App\Master', 'master_id');
    }

    // Связь 1/1 с Клиентом
    protected function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    // Связь 1/1 с Услугой
    protected function service()
    {
        return $this->belongsTo('App\SalonService', 'service_id');
    }

    // Резервации на сегодня
    public function scopeForToday($query)
    {
        return $query->where('start', '>', Carbon::now()->startOfDay())->where('end', '<', Carbon::now()->startOfDay());
    }
}
