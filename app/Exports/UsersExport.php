<?php
/**
 * Created by PhpStorm.
 * User: Sanzhar Danybayev
 * Date: 6/5/2018
 * Time: 12:51 AM
 */

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Salon;

class UsersExport implements \Maatwebsite\Excel\Concerns\FromQuery
{
    use Exportable;

    public function query()
    {
        return Salon::query();
    }
}