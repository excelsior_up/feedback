<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
    protected $fillable = [
        'salon_id',
        'service_id',
        'master_id',
        'client_id',
        'price',
        'comment',
        'rate',
    ];

    public function master()
    {
        return $this->belongsTo('App\Master', 'master_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function scopeForToday($query)
    {
        return $query->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()]);
    }

    public function scopeForSalon($query, $salon_id)
    {
        return $query->where('salon_id', $salon_id);
    }

    public function scopeForMonth($query)
    {
        return $query->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
    }

    public function scopeForWeek($query)
    {
        return $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    }
}
