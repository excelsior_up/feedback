<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Admin extends Model implements AuthenticatableContract,
                                     AuthorizableContract
{
    use Authenticatable, Authorizable;

    // Таблица базы данных
    protected $table = 'admins';

    protected $fillable = [
        'salon_id',
        'name',
        'surname',
        'email',
    ];
    //
    //protected $hidden = ['password', 'remember_token'];

    // Связь 1/1 с Салоном
    protected function salon()
    {
        // Неизвестно почему не работает
        return $this->belongsTo('App\Salon');
        //return \App\Salon::where('id', '=', $this->salon_id)->first();
    }
}
