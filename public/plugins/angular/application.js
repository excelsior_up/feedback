
var app = angular.module('Application', ['ngAria', 'ngAnimate', 'ngMaterial']);

app.config(['$interpolateProvider','$mdThemingProvider', function($interpolateProvider, $mdThemingProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');

	$mdThemingProvider.theme('default')
	  .primaryPalette('light-blue')
	  .accentPalette('orange');
}]);

app.controller('MainController',['$scope','$mdDialog', function($scope,$mdDialog) {
	console.log('main controller works');

	$scope.showAdvanced = function(ev) {
		console.log('btn');
	    $mdDialog.show({
	      controller: DialogController,
	      template:
	        '<md-dialog aria-label="List dialog">' +
	        '  <md-dialog-content>'+
	        '    <md-list>'+
	        '      <md-list-item ng-repeat="item in items">'+
	        '       <p>Number {{item}}</p>' +
	        '      '+
	        '    </md-list-item></md-list>'+
	        '  </md-dialog-content>' +
	        '  <div class="md-actions">' +
	        '    <md-button ng-click="answer()"  class="md-primary">' +
	        '      Close Dialog' +
	        '    </md-button>' +
	        '  </div>' +
	        '</md-dialog>',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true
	    })
	    .then(function(answer) {
	      $scope.status = 'You said the information was "' + answer + '".';
	    }, function() {
	      $scope.status = 'You cancelled the dialog.';
	    });
	  };

}]);

function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}