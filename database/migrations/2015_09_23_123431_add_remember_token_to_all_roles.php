<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRememberTokenToAllRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->string("remember_token")->after('password');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->string("remember_token")->after('password');
        });

        Schema::table('masters', function (Blueprint $table) {
            $table->string("remember_token")->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });

        Schema::table('masters', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
    }
}
