<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salon_id');
            $table->integer('director_id');
            $table->integer('add_client');
            $table->integer('add_master');
            $table->integer('add_outcome');
            $table->integer('add_salon');
            $table->integer('add_transaction');
            $table->integer('profile_change_password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
