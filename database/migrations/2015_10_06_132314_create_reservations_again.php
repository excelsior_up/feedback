<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('reservations');
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salon_id');
            $table->integer('master_id');
            $table->integer('client_id');
            $table->integer('service_id');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->timestamp('day');
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
