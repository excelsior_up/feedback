<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesForSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules_for_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->double('wasted_from');
            $table->double('visited_from');
            $table->double('has_sale');
            $table->integer('delete_after_days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rules_for_sale');
    }
}
