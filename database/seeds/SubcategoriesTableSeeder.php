<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $subcategories = [

            1 => [
                'Стрижки',
                'Мужской зал',
                'Детский зал',
                'Окрашивание',
                'Уход за волосами',
                'Причёски',
                'Укладки',
                'Завивка',
                'Плетение',
            ],

            2 => [
                'Маникюр',
                'Наращивание ногтей',
                'Педикюр',
            ],

            3 => [
                'Макияж',
                'Уход за ресницами и бровями',
                'Перманентный макияж',
                'Чистка лица',
                'Уход за лицом',
                'Удаление волос',
                'Уход за телом',
                'Массаж',
                'Боди-арт'
            ]

        ];

        foreach($subcategories as $id => $subcategory)
        {
            foreach($subcategory as $cat)
            {
                DB::table('subcategories')->insert([
                    'category_id' => $id,
                    'name' => $cat
                ]);
            }

        }

        Model::reguard();
    }
}
