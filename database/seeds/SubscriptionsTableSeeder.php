<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $subscriptions = [

            'Эконом',
            'Бизнес',
            'Премиум'

        ];

        foreach($subscriptions as $id => $subscription)
        {
            DB::table('subscriptions')->insert([
                'name' => $subscription
            ]);
        }

        Model::reguard();
    }
}
