<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $categories = [

            'Красивые волосы',
            'Уход за ногтями',
            'Лицо и тело'

        ];

        foreach($categories as $category)
        {
            DB::table('categories')->insert([
                'name' => $category
            ]);
        }

        Model::reguard();
    }
}
