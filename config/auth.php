<?php

return [

    'multi' => [
        'director' => [
            'driver' => 'eloquent',
            'model'  => 'App\Director',
            'table'  => 'directors',
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model'  => 'App\Admin',
            'table'  => 'admins',
        ],
        'manager' => [
            'driver' => 'eloquent',
            'model'  => 'App\Manager',
            'table'  => 'managers',
        ],
        'exe' => [
            'driver' => 'eloquent',
            'model'  => 'App\Executive',
            'table'  => 'executives'
        ],
        'master' => [
            'driver' => 'eloquent',
            'model'  => 'App\User',
            'table'  => 'masters',
        ],
    ],

    'password' => [
        'email' => 'emails.password',
        'table' => 'password_resets',
        'expire' => 60,
    ],

];
