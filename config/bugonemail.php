<?php

return array(
    'project_name' => 'Feedback!',
    'notify_emails' => array('excelsior.devs@gmail.com'),
    'email_template' => "bugonemail::email.notifyException",
    'notify_environment' => array('production'),
    'prevent_exception' => array('Symfony\Component\HttpKernel\Exception\NotFoundHttpException'),
);
